<?php

	class agenda{

		private $idAgenda;
		private $tituloAgenda;
		private $tipoAgenda;
		private $privacidadAgenda;
		private $fechaAgenda;
		private $horaAgenda;
		private $descripcionAgenda;
		private $encargadoAgenda;
		private $informacionAgenda;

		public function agenda($idAgenda,$tituloAgenda,$tipoAgenda,$privacidadAgenda,$fechaAgenda,
								$horaAgenda,$descripcionAgenda,$encargadoAgenda,$informacionAgenda){

			$this->idAgenda = $idAgenda;
			$this->tituloAgenda = $tituloAgenda;
			$this->tipoAgenda = $tipoAgenda;
			$this->privacidadAgenda = $privacidadAgenda;
			$this->fechaAgenda = $fechaAgenda;
			$this->horaAgenda = $horaAgenda;
			$this->descripcionAgenda = $descripcionAgenda;
			$this->encargadoAgenda = $encargadoAgenda;
			$this->informacionAgenda = $informacionAgenda;

		}

		public function setIdAgenda($idAgenda){
			$this->idAgenda = $idAgenda;
		}
		public function setTituloAgenda($tituloAgenda){
			$this->tituloAgenda = $tituloAgenda;
		}
		public function setTipoAgenda($tipoAgenda){
			$this->tipoAgenda = $tipoAgenda;
		}
		public function setPrivacidadAgenda($privacidadAgenda){
			$this->privacidadAgenda = $privacidadAgenda;
		}
		public function setFechaAgenda($fechaAgenda){
			$this->fechaAgenda = $fechaAgenda;
		}
		public function setHoraAgenda($horaAgenda){
			$this->horaAgenda = $horaAgenda;
		}
		public function setDescripcionAgenda($descripcionAgenda){
			$this->descripcionAgenda = $descripcionAgenda;
		}
		public function setEncargadoAgenda($encargadoAgenda){
			$this->encargadoAgenda = $encargadoAgenda;
		}
		public function setInformacionAgenda($informacionAgenda){
			$this->informacionAgenda = $informacionAgenda;
		}

		public function getIdAgenda(){
			return $this->idAgenda;
		}
		public function getTituloAgenda(){
			return $this->tituloAgenda;
		}
		public function getTipoAgenda(){
			return $this->tipoAgenda;
		}
		public function getPrivacidadAgenda(){
			return $this->privacidadAgenda;
		}
		public function getFechaAgenda(){
			return $this->fechaAgenda;
		}
		public function getHoraAgenda(){
			return $this->horaAgenda;
		}
		public function getDescripcionAgenda(){
			return $this->descripcionAgenda;
		}
		public function getEncargadoAgenda(){
			return $this->encargadoAgenda;
		}
		public function getInformacionAgenda(){
			return $this->informacionAgenda;
		}


	}

?>