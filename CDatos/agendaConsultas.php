<?php
	include "../CDatos/conexion.php";
	include "../CDominio/agenda.php";

	class agendaConsultas{

		public function agendaConsultas(){}

		function abrir_cerrar_conexion($consulta){
			$conexion = new conexion();
			$resultado = mysqli_query($conexion->abrirConexion(),$consulta);
			$conexion->cerrarConexion();

			return $resultado;
		}

		function buscarRegistros(){

			$resultado = "";

			$buscarRegistros = $this->abrir_cerrar_conexion("call buscar_registroAgenda()");

			if(mysqli_num_rows($buscarRegistros) > 0){
				while($row = mysqli_fetch_row($buscarRegistros)){
					$resultado .= $row[0].",".$row[1].",".$row[2].",".$row[3].",".$row[4].",".$row[5].",".$row[6].",".$row[7].",".$row[8]."*";
				}

			}
			else{
				$resultado = "0";
			}
			return $resultado;			

		}

		function seleccionarActividad($id){
			$resultado = "";

			$seleccionarAgenda = $this->abrir_cerrar_conexion("call seleccionar_actividad_agenda ('".$id."')");

			if(mysqli_num_rows($seleccionarAgenda) > 0){
				while($row = mysqli_fetch_row($seleccionarAgenda)){
					$resultado = $row[0].",".$row[1].",".$row[2].",".$row[3].",".$row[4].",".$row[5].",".$row[6].",".$row[7].",".$row[8];
				}

			}
			else{
				$resultado = "0";
			}
			return $resultado;	
		}

		function actualizarAgenda($agenda){
			$resultado = "";

			$actualizarAgenda = $this->abrir_cerrar_conexion("call actualizar_agenda('".$agenda->getIdAgenda()."','".$agenda->getTituloAgenda()."','".$agenda->getPrivacidadAgenda()."','".$agenda->getFechaAgenda()."','".$agenda->getHoraAgenda()."','".$agenda->getDescripcionAgenda()."','".$agenda->getEncargadoAgenda()."','".$agenda->getInformacionAgenda()."','".$agenda->getTipoAgenda()."')");

			if($actualizarAgenda != 0){
				$resultado = 1;
			}
			else
				$resultado = 0;

			return $resultado;

		}

		function eliminarAgenda($id){
			$resultado = "";

			$eliminarAgenda = $this->abrir_cerrar_conexion("call eliminar_agenda('".$id."')");

			if($eliminarAgenda != 0)
				$resultado = 1;
			else
				$resultado = 0;

			return $resultado;
		}

		function registrarAgenda($agenda){
			$resultado = "";
			$registro = 0;
			$buscarUltimoRegistro = $this->abrir_cerrar_conexion("call buscar_ultimo_registroAgenda()");
			if(mysqli_num_rows($buscarUltimoRegistro) > 0){
				while($row = mysqli_fetch_row($buscarUltimoRegistro))
					$registro = $row[0] + 1;
			}

			$registrarAgenda = $this->abrir_cerrar_conexion("call registrar_agenda('".$registro."','".$agenda->getTituloAgenda()."','".$agenda->getTipoAgenda()."','".$agenda->getPrivacidadAgenda()."','".$agenda->getFechaAgenda()."','".$agenda->getHoraAgenda()."','".$agenda->getDescripcionAgenda()."','".$agenda->getEncargadoAgenda()."','".$agenda->getInformacionAgenda()."')");

			if($registrarAgenda != 0)
				$resultado = 1;
			else
				$resultado = 0;

			return $resultado;
		}
	}
?>