<?php

class VoluntarioTest extends \PHPUnit\Framework\TestCase{

    /** @test */
    public function generica(){
        
        $this->assertTrue(true); 
    }



    /** @test */
    public function asignar_obtener_cedula_de_voluntario(){
        
        $voluntario = new \CDominioT\voluntario;
        

        $voluntario->setId(123456789);
        $this->assertEquals($voluntario->getId(), 123456789);
    }

    
    
    /** @test */
    public function asignar_obtener_nombre_de_voluntario(){
        
        $voluntario = new \CDominioT\voluntario;
            
    
        $voluntario->setNombre('rosa');
        $this->assertEquals($voluntario->getNombre(), 'rosa');
    }
}