<?php
	
	include '../CDatos/conexion.php';
	include  '../CDominio/directorio.php';

	class directorioConsultas{

		public function directorioConsultas(){}


		//REALIZA EL CONTENIDO DE LAS FILAS
		function contarFilas(){
			$conexion = new conexion();
			$respuesta = "";

			$contarFilas = "call contar_filas_directorio()";
			$resultado = mysqli_query($conexion->abrirConexion(),$contarFilas);
			if(mysqli_num_rows($resultado) > 0)
				while($row = mysqli_fetch_row($resultado))
					$respuesta = $row[0];
			$conexion->cerrarConexion();

			return $respuesta;
		}

		//REALIZA LA BUSQUEDA DE LOS COMITES REGISTRADOS PARA EL SELECT
		function buscarComite(){

			$conexion = new conexion();
			$respuesta = "";

			$buscarComite = " call buscar_comite()";
			$resultado = mysqli_query($conexion->abrirConexion(), $buscarComite);

			if(mysqli_num_rows($resultado) > 0){
				while($columna = mysqli_fetch_row($resultado)){
					$respuesta.= $columna[0].",";
				}
			}
			$conexion->cerrarConexion();
			return $respuesta;
		}

		//REALIZA LA BUSQUEDA CON DATOS COMO : NOMBRE,APELLIDO1,APELLIDO2,INSTITUCION
		function buscarIntegranteEspecifico($dato){

			$respuesta = "";
			$conexion = new conexion();

			//SI ES UN DATO COMO: DENNIS,MUNOZ, EL DETECTA QUE SON DOS VARIABLES QUE DEBE BUSCAR PARA FILTRAR
			if(strpos($dato, ",") == true){
				$datoxBuscar = explode(",",$dato);
				$datoxBuscar[1] = trim($datoxBuscar[1],'*');
				$tamaniodebusqueda = count($datoxBuscar);
				
				//REALIZA BUSQUEDAS ENTRE DOS VARIABLES POSIBLES
				if($tamaniodebusqueda == 2){


					$buscarIntegranteEspecifico = "call buscar_intregrante_especifico_2variables('".$dato[0]."','".$dato[1]."');";
					$resultado = mysqli_query($conexion->abrirConexion(),$buscarIntegranteEspecifico);

					if(mysqli_num_rows($resultado) > 0){
						while($row = mysqli_fetch_row($resultado)){
							$respuesta .= $row[0].",".$row[1].",".$row[2].",".$row[3].",".$row[4].",".$row[5].",".$row[6].",".$row[7].",".$row[8]."+";
						}
					}
					$conexion->cerrarConexion();
					
				}
				//REALIZA BUSQUEDAS ENTRE TRES VARIABLES POSIBLES
				else if($tamaniodebusqueda == 3){
					$buscarIntegranteEspecifico = "call buscar_intregrante_especifico_3variables('".$dato[0]."','".$dato[1]."','".$dato[2]."');";
					$resultado = mysqli_query($conexion->abrirConexion(),$buscarIntegranteEspecifico);

					if(mysqli_num_rows($resultado) > 0){
						while($row = mysqli_fetch_row($resultado)){
							$respuesta .= $row[0].",".$row[1].",".$row[2].",".$row[3].",".$row[4].",".$row[5].",".$row[6].",".$row[7].",".$row[8]."+";
						}
					}					
					$conexion->cerrarConexion();
				}
				//REALIZA BUSQUEDAS ENTRE 4 VARIABLES POSIBLES
				else if($tamaniodebusqueda == 4){
					$buscarIntegranteEspecifico = "call buscar_intregrante_especifico_4variables('".$dato[0]."','".$dato[1]."','".$dato[2]."','".$dato[3]."');";
					$resultado = mysqli_query($conexion->abrirConexion(),$buscarIntegranteEspecifico);

					if(mysqli_num_rows($resultado) > 0){
						while($row = mysqli_fetch_row($resultado)){
							$respuesta .= $row[0].",".$row[1].",".$row[2].",".$row[3].",".$row[4].",".$row[5].",".$row[6].",".$row[7].",".$row[8]."+";
						}
					}					
					$conexion->cerrarConexion();

				}
			}
			//SI EL DATO NO CONTIENE LA O LAS COMA(S) REALIZA UNA BUSQUEDA CON UNA UNICA VARIABLE
			else{
				$buscarIntegranteEspecifico = "call buscar_intregrante_especifico('".$dato."');";

				$resultado = mysqli_query($conexion->abrirConexion(), $buscarIntegranteEspecifico);

				if(mysqli_num_rows($resultado) > 0){
					while($row = mysqli_fetch_row($resultado)){
						$respuesta .= $row[0].",".$row[1].",".$row[2].",".$row[3].",".$row[4].",".$row[5].",".$row[6].",".$row[7].",".$row[8].",".$row[9]."+";
					}
				}

				$conexion->cerrarConexion();

			}

			return $respuesta;
		}

		function eliminarIntegrante($id){

			$respuesta = "";
			$conexion = new conexion();

			$actualizarIntegrante = "call eliminar_integrante('".$id."');";

			$resultado = mysqli_query($conexion->abrirConexion(), $actualizarIntegrante);
			if($resultado != 0){
				$respuesta = 1;
			}
			$conexion->cerrarConexion();

			return $respuesta;

		}

		function devolverIntegrante($id){

			$conexion = new conexion();
			$respuesta = "";

			$buscarIntegrante = "call devolver_integrante('".$id."');";

			$resultado = mysqli_query($conexion->abrirConexion(), $buscarIntegrante);
			$conexion->cerrarConexion();

			if(mysqli_num_rows($resultado) > 0){

				while($row = mysqli_fetch_row($resultado)){

					$id = $row[0];
					$cedula = $row[1];
					$nombre = $row[2];
					$apellido1 = $row[3];
					$apellido2 = $row[4];
					$telefono = $row[5];
					$institucion = $row[6];
					$puesto = $row[7];
					$comite = $row[8];
					$correo = $row[9];

					$respuesta .= $id.",".
								  $cedula.",".
								  $nombre.",".
								  $apellido1.",".
								  $apellido2.",".
								  $telefono.",".
								  $institucion.",".
								  $puesto.",".
								  $comite.",".
								  $correo;

				}
			}

			return $respuesta;

		}

		function actualizarIntegrante($id,$directorio){

			$respuesta = "";
			$conexion = new conexion();

			$actualizarIntegrante = "call actualizar_integrante(
																'".$id."',
																'".$directorio->getCedulaIntegranteD()."',
																'".$directorio->getNombreIntegranteD()."',
																'".$directorio->getApellido1IntegranteD()."',
																'".$directorio->getApellido2IntegranteD()."',
																'".$directorio->getTelefonoIntegranteD()."',
																'".$directorio->getInstitucionIntegranteD()."',
																'".$directorio->getPuestoIntegranteD()."',
																'".$directorio->getComiteIntegranteD()."',
																'".$directorio->getCorreoIntegranteD()."'
																);";

			$resultado = mysqli_query($conexion->abrirConexion(), $actualizarIntegrante);
			if($resultado != 0){
				$respuesta = 1;
			}
			$conexion->cerrarConexion();

			return $respuesta;
		}

		function buscarIntegrantes($pagina){

			$conexion = new conexion();	
			$respuesta = "";

			$buscarIntegrante = "call buscar_integrantes('".$pagina."')";

			$resultado = mysqli_query($conexion->abrirConexion(), $buscarIntegrante);
			$conexion->cerrarConexion();

			if(mysqli_num_rows($resultado) > 0){

				while($row = mysqli_fetch_row($resultado)){

					$id = $row[0];
					$cedula = $row[1];
					$nombre = $row[2];
					$apellido1 = $row[3];
					$apellido2 = $row[4];
					$telefono = $row[5];
					$institucion = $row[6];
					$puesto = $row[7];
					$comite = $row[8];
					$correo = $row[9];

					$respuesta .= $id.",".
								  $cedula.",".
								  $nombre.",".
								  $apellido1.",".
								  $apellido2.",".
								  $telefono.",".
								  $institucion.",".
								  $puesto.",".
								  $comite.",".
								  $correo."+";

				}
			}

			return $respuesta;

		}

		function insertarIntegrante($cedula,$nombre,$apellido1,$apellido2,$telefono,$institucion,$puesto, $correo){

			$conexion = new conexion();
			$respuesta = 0;

			$buscarUltimoIntegrante = "call buscar_ultimo_iddirectorio()";

			$resultado = mysqli_query($conexion->abrirConexion(),$buscarUltimoIntegrante);
			$conexion->cerrarConexion();
	       
	        if ($row = mysqli_fetch_row($resultado)) {
	            $respuesta = trim($row[0]) + 1;
	        }   

			$registrarIntegrante = "call insertar_integrante(
															'".$respuesta."',
															'".$cedula."',
															'".$nombre."',
															'".$apellido1."',
															'".$apellido2."',
															'".$telefono."',
															'".$institucion."',
															'".$puesto."',
															'Municipal de Emergencias',
															'".$correo."'
															)";

			$resultado2 = mysqli_query($conexion->abrirConexion(), $registrarIntegrante);	
			if($resultado2 != 0)
				$resultado2 = 1;
			$conexion->cerrarConexion();
			return $resultado2;
		}

	}

?>