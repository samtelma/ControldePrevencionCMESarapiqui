<?php

	include 'conexion.php';

	class consultasBodega{

		public function consultasBodega() {}

		//REALIZA EL CONTENIDO DE LAS FILAS
		function contarFilas() {

			// Conectamos con la DB.
			$conexion = new conexion();
			$respuesta = "";

			// Creamos la consulta SQL.
			$query = "call contar_filas_Bodega();";

			// Ejecutamos la peticion.
			$resultado = mysqli_query($conexion->abrirConexion(), $query);

			if (mysqli_num_rows($resultado) > 0) {

				while ($row = mysqli_fetch_row($resultado)) {

					$respuesta = $row[0];
				}
			}
			//Cerramos la conexion.
			$conexion->cerrarConexion();

			return $respuesta;
		}

		function Query($query) {

			$conexion = new conexion();
			$resultado = mysqli_query($conexion->abrirConexion(), $query);
			$conexion->cerrarConexion();
			
			return $resultado;
		}

		function buscarBodegaEspecifica($dato) {

			// Conectamos con la DB.
			$conexion = new conexion();

			// Creamos la consulta SQL.
			$query = "call buscar_integrante_especifico_Bodega('".$dato."');";

			// Ejecutamos la peticion.
			$resultado = mysqli_query($conexion->abrirConexion(), $query);

			if (mysqli_num_rows($resultado) > 0) {

				$i = 0;
				$array = array();

				while ($row = mysqli_fetch_row($resultado)) {

					$conexion->cerrarConexion();

					$buscar = "call obtener_id_responsable_por_nombre('".$row[4]."');";
					$result = mysqli_query($conexion->abrirConexion(),$buscar);

					if (mysqli_num_rows($result) > 0) {

						$r = mysqli_fetch_row($result);
						$nombreResponsable = $r[0];
					} else {

						$nombreResponsable = "Sin definir";
					}

					$array[$i] = new bodega($row[0], $row[1], $row[2], $row[3], $nombreResponsable);
					$i++;
				}

				$conexion->cerrarConexion();
				return $array;
			}

			return 0;
		}

		function Cargar() {
			
			// Creamos la conuslta SQL.
			$query = "call obtener_nombres_responsables_de_bodega;";

			// Ejecutamos la peticion.
			$resultado = $this->Query($query);

			if (mysqli_num_rows($resultado) > 0) {

				$datos = "";

				while ($row = mysqli_fetch_row($resultado)) {

					$datos.=$row[0]."+";
				}

				return $datos;
			}

			return 0;
		}

		function ObtenerIdResponablePorNombre($nombreResponsable) {

			// Creamos la consulta SQL.
			$query = "call obtener_id_responsable_por_nombre('".$nombreResponsable."');";

			// Ejecutzmos la peticion.
			$resultado = $this->Query($query);
			
			if (mysqli_num_rows($resultado) > 0) {

				$row = mysqli_fetch_row($resultado);

				return  $row[0];
			}

			return 0;
		}

		function Registrar($bodega) {

			// Creamos la consult SQL.
			$query="call insertar_una_Bodega('".$bodega->getNombre()."','".
												$bodega->getDescripcion()."','".
												$bodega->getDireccion()."','".
												$bodega->getResponsable()."')";
																					
			// Ejecutamos la peticion.
			$resultado = $this->Query($query);

			return $resultado;
		}

		function Consultar($pagina) {
			
			// Creamos la consulta SQL.
			$query = "call consultar_tabla_Bodega('".$pagina."');";

			// Ejecutamos la peticion.
			$resultado = $this->Query($query);
			
			if (mysqli_num_rows($resultado) > 0) {

				$i = 0;
				$array = array();

				while ($row = mysqli_fetch_row($resultado)) {

					/*
					$result = $this->Query("call obtener_nombre_responsable_por_id_bodega('".$row[4]."');");

					if (mysqli_num_rows($result) > 0) {

						$r = mysqli_fetch_row($result);
						$nombreResponsable = $r[0];
					} else {

						$nombreResponsable = "Sin definir";
					}
					*/

					// Obtenemos los datos.
					$id = $row[0];
					$nombre = $row[1];
					$descripcion = $row[2];
					$direccion = $row[3];
					$responsable = $row[4];

					// Creamos el objeto bodega.
					$bodega = new bodega($id, 
											$nombre, 
											$descripcion, 
											$direccion, 
											$responsable);

					// Lo añadimos al vector de bodegas. 
					$array[$i] = $bodega;
					$i++;
				}

				return $array;
			}

			return 0;
		}

		function Eliminar($id) {

			// Creamos la consulta SQL.
			$query = "call eliminar_una_Bodega ('".$id."');";

			// Ejecutamos la peticion.
			$resultado = $this->Query($query);

			return $resultado;
		}

		function Seleccionar($id) {

			// Creamos la consulta SQL.
			$query = "call consultar_una_Bodega('".$id."');";

			// Ejecutamos la peticion.
			$resultado = $this->Query($query);

			$datos = "";

			if (mysqli_num_rows($resultado) > 0) {

				$row = mysqli_fetch_row($resultado);

				//$query = "SELECT nombreresponsable FROM tbresponsable WHERE idresponsable='".$row[4]."'";
				$query = "call obtener_nombre_responsable_por_id_bodega('".$row[4]."');";
				$result = $this->Query($query);

				if (mysqli_num_rows($result) > 0) {

					$aux = mysqli_fetch_row($result);
					$nombreResponsable = $aux[0];
				} else {

					$nombreResponsable = "Sin definir";
				}
				$bodega = new bodega($row[0], $row[1], $row[2], $row[3], $nombreResponsable);

				return $bodega;
			} else {

				return 0;
			}
		}

		function Actualizar($bodega) {

			// Creamos la consulta SQL.
			$query = "call actualizar_una_Bodega ('".$bodega->getId()."','"
													.$bodega->getNombre()."','"
													.$bodega->getDescripcion()."','"
													.$bodega->getDireccion()."','"
													.$bodega->getResponsable()."');";

			// Ejecutamos la peticion.
			$resultado = $this->Query($query);

			return $resultado;
		}
	}
?>