<?php
namespace CDominioT;

	class bodega {

		private $id;
		private $nombre;
		private $descripcion;
		private $direccion;
		private $responsable;

		public function bodega($id, $nombre, $descripcion, $direccion, $responsable) {

			$this->id = $id;
			$this->nombre = $nombre;
			$this->descripcion = $descripcion;
			$this->direccion = $direccion;
			$this->responsable = $responsable;
		}

		// Sets...
		public function setId($id) {

			$this->id = $id;
		}

		public function setNombre($nombre) {

			$this->nombre = $nombre;
		}

		public function setDescripcion($descripcion) {

			$this->descripcion = $descripcion;
		}

		public function setdireccion($direccion) {

			$this->direccion = $direccion;
		}

		public function setResponsable($responsable) {

			$this->responsable = $responsable;
		}

		// Gets...
		public function getId() {

			return $this->id;
		}

		public function getNombre() {

			return $this->nombre;
		}

		public function getDescripcion() {

			return $this->descripcion;
		}

		public function getDireccion() {

			return $this->direccion;
		}

		public function getResponsable() {

			return $this->responsable;
		}

		// toString...
		public function toString() {

			return $this->id."-".$this->nombre."-".$this->descripcion."-".$this->direccion."-".$this->responsable;
		}
	}
?>