<?php

	include 'conexion.php';

	class consultasAlbergues {

		public function consultasAlbergues() {}

		//REALIZA EL CONTENIDO DE LAS FILAS
		function contarFilas() {

			$conexion = new conexion(); // Conectamos con la bbase de datos.
			$respuesta = "";
			$query = "call contar_filas_Albergue() "; // Creamos la consulta SQL.
			$resultado = mysqli_query($conexion->abrirConexion(), $query); // Ejecutamos la peticion.

			if(mysqli_num_rows($resultado) > 0) { // Verificamos el resultado.

				while($row = mysqli_fetch_row($resultado)) {

					$respuesta = $row[0];
				}
			}

			$conexion->cerrarConexion(); // Cerramos la conexion.

			return $respuesta;
		}

		function buscarAlbergueEspecifico($dato){

			// Creamos la cnsulta SQL.
			$query = "call buscar_especifico_Albergue('".$dato."')";
			$resultado = $this->Query($query); // Ejecutamos la peticion.

			if(mysqli_num_rows($resultado) > 0){ // Verificamos el resultado.

				$i = 0;
				$array = array(); 

				while($row = mysqli_fetch_row($resultado)) {

					// Obtenemos los datos.
					$id        = $row[0];
					$nombre    = $row[1];
					$capacidad = $row[2];
					$encargado = $row[3];
					$localidad = $row[4];
					$telefono  = $row[5];
					$luz       = $row[6];
					$agua      = $row[7];
					$cuartos   = $row[8];
					$cocina    = $row[9];
					$banos     = $row[10];

					// Creamos el objeto albergue.
					$array[$i]=new albergue($row[0], 
											$row[1], 
											$row[2], 
											$row[3], 
											$row[4],
											$row[5], 
											$row[6], 
											$row[7], 
											$row[8], 
											$row[9], 
											$row[10]);
					$i++;
				}

				return $array;
			}
			
			return 0;
		}

		function Query($query) {

			$conexion = new conexion();
			$resultado = mysqli_query($conexion->abrirConexion(), $query);
			$conexion->cerrarConexion();

			return $resultado;
		}

		function CargarDatosEncargados() {
			
			// Creamos la consulta SQL.
			$query = "call cargar_nombres_responsables_Albergue()";
			$resultado = $this->Query($query); // Ejecutamos la peticion.

			if (mysqli_num_rows($resultado) > 0) { // Verificamos el resultado.

				$datos = "";

				while ($row = mysqli_fetch_row($resultado)) {

					$datos.=$row[0]." ".$row[1]." ".$row[2]."+";
				}

				return $datos;
			}

			return 0;
		}

		function OntenerIdResponsablePorNombre($nombreresponsable,$apellido1,$apellido2) {

			$query = "call obtener_id_responsable_por_nombre_Albergue('".$nombreresponsable."','".$apellido1."','".$apellido2."')"; // Creamos la consulta SQL.
			$resultado = $this->Query($query); // Ejecutamos la peticion.
			
			if (mysqli_num_rows($resultado) > 0) { // Evaluamos el resultado.

				$idresponsable = 0;

				while ($row = mysqli_fetch_row($resultado)) {

					$idresponsable = $row[0];
				}

				return $idresponsable;
			}

			return 0;
		}

		function Registrar($albergue) {

			$responsable = $albergue->getEncargado();
			$responsable = explode(' ',$responsable);
			$idEncargado = $this->OntenerIdResponsablePorNombre($responsable[0],$responsable[1],$responsable[2]);

			if ($albergue->getAgua() != "0" && $albergue->getAgua() != "1") {

				// Creamos la consuta SQL.
			$query = "call insertar_un_Albergue('".$albergue->getNombre()."','"
													.$albergue->getCapacidad()."','"
													.$idEncargado."','"
													.$albergue->getLocalidad()."','"
													.$albergue->getTelefono()."','"
													.$albergue->getLuz()."','"
													.$albergue->getCuartos()."','"
													.$albergue->getCocina()."','"
													.$albergue->getBanos()."', 
													'0');";
			} else{

				// Creamos la consuta SQL.
				$query = "call insertar_un_Albergue('".$albergue->getNombre()."','"
														.$albergue->getCapacidad()."','"
														.$idEncargado."','"
														.$albergue->getLocalidad()."','"
														.$albergue->getTelefono()."','"
														.$albergue->getLuz()."','"
														.$albergue->getAgua()."','"
														.$albergue->getCuartos()."','"
														.$albergue->getCocina()."','"
														.$albergue->getBanos()."',
														'0');";
			}

			$resultado = $this->Query($query);
			
			return $resultado;
		}

		function Consultar($pagina) {
			
			// Obtenemos los datos.
			$query = "call consultar_tabla_Albergue('".$pagina."')";

			$resultado = $this->Query($query); // Ejecutamos la peticion.
			
			if (mysqli_num_rows($resultado) > 0) { // Comprobamos los resultados.

				$i = 0;
				$array = array();

				while ($row = mysqli_fetch_row($resultado)) {

					// Obtenemos los datos.
					$id        = $row[0];
					$nombre    = $row[1];
					$capacidad = $row[2];
					$encargado = $row[3];
					$localidad = $row[4];
					$telefono  = $row[5];
					$luz       = $row[6];
					$agua      = $row[7];
					$cuartos   = $row[8];
					$cocina    = $row[9];
					$banos     = $row[10];

					// Creamos el objeto albergue.
					$array[$i]=new albergue($row[0], 
											$row[1], 
											$row[2], 
											$row[3], 
											$row[4],
											$row[5], 
											$row[6], 
											$row[7], 
											$row[8], 
											$row[9], 
											$row[10]);

					$i++;
				}

				return $array;
			}

			return 0;
		}

		function Eliminar($id) {

			$query = "call eliminar_un_Albergue('".$id."')"; // Creamos la consulta SQL.
			$resultado = $this->Query($query); // Ejecutamos la peticion.

			return $resultado;
		}

		function Seleccionar($id) {

			// Creamos la consulta SQL.
			$query = "call seleccionar_Albergue('".$id."')";

			$resultado = $this->Query($query); // Ejecutmaos la peticion.

			if (mysqli_num_rows($resultado) > 0) { // Evaluamos el resultado.

				$row = mysqli_fetch_row($resultado);

				// Obtenemos los datos.
					$id        = $row[0];
					$nombreAlbergue    = $row[1];
					$nombreResponsable    = $row[2];
					$apellido1    = $row[3];
					$apellido2    = $row[4];					
					$capacidad = $row[5];
					$localidad = $row[6];
					$telefono  = $row[7];
					$luz       = $row[8];
					$agua      = $row[9];
					$cuartos   = $row[10];
					$cocina    = $row[11];
					$banos     = $row[12];

				$datos = $id."_".$nombreAlbergue."_".$nombreResponsable."_".$apellido1."_".$apellido2."_".$capacidad."_".$localidad."_".$telefono."_".$luz."_".$agua."_".$cuartos."_".$cocina."_".$banos;
			}

			return $datos;
		}

		function Actualizar($albergue) {
			$responsable = $albergue->getEncargado();
			$responsable = explode(' ',$responsable);
			$idEncargado = $this->OntenerIdResponsablePorNombre($responsable[0],$responsable[1],$responsable[2]);

			// Creamos la consulta SQL.
			$query ="call actualizar_un_Albergue(
											'".$albergue->getId()."',
											'".$albergue->getNombre()."',
											'".$albergue->getCapacidad()."',
											'".$idEncargado."',
											'".$albergue->getLocalidad()."',
											'".$albergue->getTelefono()."',
											'".$albergue->getLuz()."',
											'".$albergue->getAgua()."',
											'".$albergue->getCuartos()."',
											'".$albergue->getCocina()."',
											'".$albergue->getBanos()."'
											)";

			$resultado = $this->Query($query); // Ejecutamos la peticion.

			return $resultado;
		}

		function TelefonoResponsable($nombre) {
			
			$buscarTelefono = explode(' ',$nombre);

			$query = "SELECT telefonoresponsable FROM tbresponsable WHERE nombreresponsable='".$buscarTelefono[0]."' AND apellido1responsable = '".$buscarTelefono[1]."' AND apellido2responsable = '".$buscarTelefono[2]."'"; // Creamos la consulta SQL.
			$resultado = $this->Query($query); // Ejecutamos la peticion.

			if (mysqli_num_rows($resultado) > 0) { // Evaluamos el resultado.

				$row = mysqli_fetch_row($resultado);

				return $row[0];
			}

			return $resultado;
		}

		function obtenerNombreResponsableporID($id){

			$query = "SELECT nombreresponsable,apellido1responsable,apellido2responsable FROM tbresponsable WHERE idresponsable = '".$id."'";

			$resultado = $this->Query($query);

			if(mysqli_num_rows($resultado) > 0){
				$row = mysqli_fetch_row($resultado);

				$datos = $row[0]." ".$row[1]." ".$row[2];
			}

			return $datos;
		}
	}
?>