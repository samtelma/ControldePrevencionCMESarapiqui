<?php

class InsumoTest extends \PHPUnit\Framework\TestCase{

   
    /** @test */
    public function asignar_obtener_cantidad_de_insumo(){
        
        $insumo = new \CDominioT\insumo;
        

        $insumo->setCantidad(150);
        $this->assertEquals($insumo->getCantidad(), 150);
    }




    /** @test */
    public function asignar_obtener_institucion_de_insumo(){
        
        $insumo = new \CDominioT\insumo;
        

        $insumo->setInstitucion('Cruz Roja');
        $this->assertEquals($insumo->getInstitucion(), 'Cruz Roja');
    }

}