<?php

	include 'conexion.php';

	class consultasInsumo {

		public function consultasInsumo() {}

		//REALIZA EL CONTENIDO DE LAS FILAS
		function contarFilas(){
			$conexion = new conexion();
			$respuesta = "";

			$contarFilas = "call contar_filas_Insumos ()";
			$resultado = mysqli_query($conexion->abrirConexion(),$contarFilas);
			if(mysqli_num_rows($resultado) > 0)
				while($row = mysqli_fetch_row($resultado))
					$respuesta = $row[0];
			$conexion->cerrarConexion();

			return $respuesta;
		}

		function buscarInsumoEspecifico($dato){

			$conexion = new conexion();
			$resultado = $this->Query("call buscar_especifico_Insumo('".$dato."')");
			if (mysqli_num_rows($resultado) > 0) {

				$i = 0;
				$array = array();

				while ($row = mysqli_fetch_row($resultado)) {

					$id          = $row[0];
					$articulo    = $row[1];
					$institucion = $row[2];
					$cantidad    = $row[3];
					$observacion = $row[4];
					$bodega      = $row[5];

					$array[$i] = new insumo($id, $articulo, $institucion, $cantidad, $observacion, $bodega);
					$i++;
				}

				return $array;
			}

			return 0;
		}		

		function Query($query) {

			$conexion = new conexion();
			$resultado = mysqli_query($conexion->abrirConexion(), $query);
			$conexion->cerrarConexion();

			return $resultado;
		}

		function CargarDatosBodega() {
			
			$query = "call cargar_datos_bodega_en_Insumos()";
			$resultado = $this->Query($query);

			if (mysqli_num_rows($resultado) > 0) {

				$datos = "";

				while ($row = mysqli_fetch_row($resultado)) {

					$datos.=$row[0]."+";
				}

				return $datos;
			} else {

				return 0;
			}
		}

		function OntenerIdBodegaPorNombre($nombreBodega) {

			$query = "call obtener_id_bodega_por_nombre_de_Insumos('".$nombreBodega."')";
			$resultado = $this->Query($query);

			if (mysqli_num_rows($resultado) > 0) {

				$row = mysqli_fetch_row($resultado);

				return $row[0];
			} else {

				return 0;
			}
		}

		function Registrar($insumo) {

			$query ="call insertar_un_Insumos('".$insumo->getArticulo()."',
													'".$insumo->getInstitucion()."',
													'".$insumo->getCantidad()."',
													'".$insumo->getObservacion()."',
													'".$insumo->getBodega()."',
													'0')";

			$resultado = $this->Query($query);

			if ($resultado > 0) {

				return 1;
			} else {

				return 0;
			}
		}

		function ConsultarInsumos($pagina) {
			
			$resultado = $this->Query("call consultar_tabla_Insumos('".$pagina."')");

			if (mysqli_num_rows($resultado) > 0) {

				$i = 0;
				$array = array();

				while ($row = mysqli_fetch_row($resultado)) {

					$id          = $row[0];
					$articulo    = $row[1];
					$institucion = $row[2];
					$cantidad    = $row[3];
					$observacion = $row[4];
					$bodega      = $row[5];

					$array[$i] = new insumo($id, $articulo, $institucion, $cantidad, $observacion, $bodega);
					$i++;
				}

				return $array;
			}

			return 0;
		}

		function EliminarInsumo($id) {

			$query = "call  eliminar_un_Insumos('".$id."')";
			$resultado = $this->Query($query);
			
			return $resultado;
		}

		function SeleccionarInsumo($id) {

			$query = "call buscar_por_bodega_un_Insumo('".$id."')";
			
			$resultado = $this->Query($query);

			if (mysqli_num_rows($resultado) > 0) {

				$row = mysqli_fetch_row($resultado);

				return $insumo = new insumo($row[0], $row[1],$row[2], $row[3], $row[4], $row[5]);
			} else {

				return 0;
			}
		}

		function Actualizar($insumo) {

			$query="call actualizar_un_Insumos('".$insumo->getId()."',
												'".$insumo->getArticulo()."',
												'".$insumo->getInstitucion()."',
												'".$insumo->getCantidad()."',
												'".$insumo->getObservacion()."',
												'".$insumo->getBodega()."')";

			$resultado = $this->Query($query);

			return $resultado; 
		}
	}
?>