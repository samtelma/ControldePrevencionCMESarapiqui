window.onload = function (event) {
    LimpiarCampos();
    Consultar(calcularPaginaparaRegistros());
    crearPaginacion();
    verificarPagina();
}

$(document).ready(function() {
    
    $('#btn_registrar').click(function(){
        $("html").css("overflow","hidden");
        document.getElementById('cedulaDetalle').style.margin = "0px -600px 0px 0px";
        document.getElementById('botones').style.margin = "0px 190px";
        document.getElementById("modal").style.top = "0px";
        LimpiarCampos();
        CargarDatosResponsable("Sin definir");
    });
    $('#cancelar').click(function(){
        $("html").css("overflow","scroll");
        document.getElementById("modal").style.top = "-500vh";
        LimpiarCampos();
    });
    $('.btnEliminar').click(function(){
        $("html").css("overflow","hidden");
        document.getElementById("div_modal_confirmacion").style.top = "0px";
        LimpiarCampos();
    });
    $('#btn_cancelar_confirmacion').click(function(){
        $("html").css("overflow","scroll");
        document.getElementById("modal").style.top = "-500vh";
        LimpiarCampos();
    });
    document.getElementsByClassName("close")[0].onclick = function() {
        $("html").css("overflow","scroll");
        document.getElementById('modal').style.top = "-500vh";
    }

    window.onclick = function(event) {

        if (event.target == document.getElementById('modal')) {
            $("html").css("overflow","scroll");
            document.getElementById('modal').style.top = "-500vh";
        }
    }
});


function LimpiarCampos() {

    OcultarMensajes();

    document.getElementById("btn").value = "Registrar";
    document.getElementById("nombre").value = "";
    document.getElementById("telefono").value = "";
    document.getElementById("detalle").value = ""; // Metodo que se encarga de limpiar los campos de textos.
    document.getElementById('buscar').value = "";
}

function OcultarMensajes() {

    $(".mensaje").hide();
    $(".asterisco").hide();
}

function BTN() { // Metodo que se encarga de ver si es registrar o actualizar.

    OcultarMensajes();

    if (document.getElementById("btn").value == "Registrar") {

        Registrar();
    } else {

        Actualizar();
    }
}

function AceptarNumeros(e) {

    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    // tecla = 8 -> es de retroceso.
    // tecla = 0 -> es de tabulador.
    if (tecla == 8 || tecla == 0) {

        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros y letras
    patron = /[0-9]/;
    tecla_final = String.fromCharCode(tecla);

    OcultarMensajes();

    return patron.test(tecla_final);
}

function AceptarLetrasNumeros(e) {

    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    // tecla = 8 -> es de retroceso.
    // tecla = 0 -> es de tabulador.
    // tecla = 32 -> es del espacio.
    if (tecla == 8 || tecla == 0 || tecla == 32) {

        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros y letras
    patron = /[A-Za-z0-9]/;
    tecla_final = String.fromCharCode(tecla);

    OcultarMensajes();

    return patron.test(tecla_final);
}

function AceptarLetrasNumerosComa(e) {

    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    // tecla = 8 -> es de retroceso.
    // tecla = 0 -> es de tabulador.
    // tecla = 32 -> es del espacio.
    // tecla = 44 -> es la d ela ',' "coma".
    if (tecla == 8 || tecla == 0 || tecla == 44 || tecla == 32) {

        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros y letras
    patron = /[A-Za-z0-9]/;
    tecla_final = String.fromCharCode(tecla);

    OcultarMensajes();

    return patron.test(tecla_final);
}

function Registrar() { // metodo que registra una nuevo voluntario.

    // Obteniendo los valores de los inputs.
    var nombre = document.getElementById("nombre").value;

    // Se puede continuar si solo si, el nombre se ingreso.
    if (nombre != "") {

        // Creando el STRING para enviar a la controladora.
        var enviar = "registrar=true" + "&nombre=" + nombre + "&telefono=" + document.getElementById("telefono").value + "&detalle=" + document.getElementById("detalle").value + " ";

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(enviar);

        xhr.onreadystatechange = function () {

            if (xhr.readyState == 4 && xhr.status == 200) {

                var resultado = xhr.responseText;

                //alert(resultado);
                if (resultado != 0) {
                    $("html").css("overflow","scroll");
                    //alert(resultado);
                    LimpiarCampos();
                    Consultar();
                    $("#mensaje2").show();

                } else { // Error !!!.

                    $("#mensaje3").show();
                    //alert("Error en la registrar un voluntario.\n" + resultado);
                }
            }
        }
    } else {

        // Si el nombre esta vacio.
        $("#lNombre").show(); // Mostrar un asterisco que indique donde esta el error.
        $("#mensaje5").show(); // Mostrar un mensaje que indique cual es el error.
    }
}

function Consultar(valor) { // Metodo que consulta la lista de bodegas regstradas.
    var pagina = 0;
    if(valor == 0)
        pagina = calcularPaginaparaRegistros();
    if(valor > 0)
        pagina = valor;

    if(pagina == 1)
        pagina = parseInt(pagina)-1;
    else
        pagina = parseInt(pagina)*3;

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("consultar=true" + "&pagina=" + pagina);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {

                $("tr").remove(".cla");
                $("#tabla").append(resultado);
            }
        }
    }
}

function ConfirmarEliminacion(id) {
    $("html").css("overflow","hidden");
    document.getElementById("modalEliminar").style.top = "0px";
    document.getElementById("label_id_voluntario").innerHTML = id;
}

function Eliminar() { // Metodo que elimina un voluntario.
    
    OcultarMensajes();

    document.getElementById("modalEliminar").style.top = "-500vh";

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("eliminar=" + document.getElementById("label_id_voluntario").innerHTML);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {

                //alert(resultado);
                if (resultado == 1) { // Exitoso.

                    alert("Registro eliminado");
                    Consultar();
                    $("#mensaje3").show();
                    $("html").css("overflow","scroll");                    
                    document.getElementById("label_id_voluntario").innerHTML = "";
                }
            } else {

                $("#mensaje1").show();
                //alert("Error en eliminar voluntario.\n" + resultado);
            }
        }
    }

    if (document.getElementById("btn").value == "Actualizar") {

        document.getElementById("btn").value = "Registrar";
    }
}

function Cancelar() {

    document.getElementById("cedulaDetalle").innerHTML = "";
    document.getElementById("modalEliminar").style.top = "-500vh";
}

function Seleccionar(id) { // Metodo que selecciona un voluntario.

    OcultarMensajes();
    $("html").css("overflow","hidden");

    document.getElementById("modal").style.top = "0px";
    document.getElementById('cedulaDetalle').style.margin = "0px -600px 0px 0px";
    document.getElementById('botones').style.margin = "0px 190px";

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("seleccionar=" + id);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {

                //alert(resultado);
                var cadena = resultado.split("_");
                document.getElementById("id_id").value = cadena[0];
                document.getElementById("nombre").value = cadena[1];
                document.getElementById("telefono").value = cadena[2];
                document.getElementById("detalle").value = cadena[3];
                document.getElementById("btn").value = "Actualizar";
            } else {

                $("#mensaje1").show();
                //alert("Error en seleccionar.\n" + resultado);
            }
        }
    }
}

function Actualizar() { // Metodo que actualiza una bodega.

    var nombre = document.getElementById("nombre").value;

    if (nombre != "") {

        var enviar = "actualizar=true" + "&id=" + document.getElementById("id_id").value + "&nombre=" + nombre + "&telefono=" + document.getElementById("telefono").value + "&detalle=" + document.getElementById("detalle").value + " ";

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(enviar);

        xhr.onreadystatechange = function () {

            if (xhr.readyState == 4 && xhr.status == 200) {

                var resultado = xhr.responseText;

                //alert(resultado);
                if (resultado != 0) {

                    //alert(resultado);
                    $("html").css("overflow","scroll");                    
                    document.getElementById("modal").style.top = "-500vh";
                    Consultar();
                    $("#mensaje4").show();
                } else {

                    $("#mensaje1").show();
                    //alert("Error en actualizar.\n" + resultado);
                }
            }
        }
    } else {

        $("#lNombre").show();
        $("#mensaje5").show();
    }
}

function Mas(i){
    
    if (document.getElementById(i).style.display == "none" || document.getElementById(i).style.display == "") {

        document.getElementById(i).style.display = "block";
        document.getElementById("btn" + i).value = "Menos...";
    } else {
        document.getElementById(i).style.display = "none";
        document.getElementById("btn" + i).value = "Mas...";
    }
    
    //alert(i);
}

function CancelarModal() {

    document.getElementById("modal").style.top = "-500vh";
    LimpiarCampos();
}

function detectarTeclaEnter_enBusqueda(e){

    var evt = e ? e : event;
    var key = window.Event ? evt.which : evt.keyCode;

    if(key == 13){

        buscarIntegrante();
    }

}


function buscarIntegrante(){
    var buscar = document.getElementById('buscar').value;
    var tablaVoluntario = document.getElementById('tabla');
    var td = "";
    var tr = "";
    var temp = "";
    var dato = "Sin resultado";
    var i = 0; 
    var puerta = false;
    if(buscar != ""){
        while(i < tablaVoluntario.rows.length){
            tr = tablaVoluntario.rows[i];
            for(var j = 0; j < tr.cells.length-1; j++){
                td = tr.cells[j].innerHTML;
                if(buscar.toUpperCase().includes(td.toUpperCase())){
                    temp = tr.cells[0].innerHTML + ",";
                    if(!puerta){
                        puerta = true;
                        dato = "";
                    }
                    if(!dato.includes(temp)){
                        dato += temp;
                    }                    
                }
            }
            i++;
        }
        if(dato != "Sin resultado"){
            for(var i = 1 ; i < tablaVoluntario.rows.length; i++){

                if(dato.includes(tablaVoluntario.rows[i].cells[0].innerHTML)){
                    tablaVoluntario.rows[i].style.display = "";
                }
                else{
                    tablaVoluntario.rows[i].style.display = "none";
                }
            }
        }
        else{
            if(dato == "Sin resultado"){

                buscar_en_baseD(buscar);

            }
            else
                resultadoBusqueda(dato);
        }
        
    }
    if(buscar == ""){
        resultadoBusqueda(buscar);
    }

}

function buscar_en_baseD(variable){

    var tablaVoluntario = document.getElementById("tabla");
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("buscarIntegranteEspecifico=" + variable);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;
            if (resultado != 0) {

                $("tr").remove(".cla");
                $("#tabla").append(resultado);
            }
            else if(resultado == ""){
                resultadoBusqueda("Sin resultado");
            }
        }

    }

}

function campoVacio(){
    if(document.getElementById('buscar').value == ""){
        resultadoBusqueda("");
    }   
}
function resultadoBusqueda(texto){  
    if(texto == "Sin resultado"){
        document.getElementById('buscar').value = "Sin resultados";
        document.getElementById('buscar').style.color = "red";
        document.getElementById('tabla').style.display = "";
    }
    else if(document.getElementById('buscar').value == ""){
        document.getElementById('buscar').style.color = "black";
        Consultar();
    }
}


function verificarPagina(paginaLimite,avanza){

    if(window.location.search == ""){
        document.getElementById('primeraPagina').style.pointerEvents = "none";
        location.href = "../CPresentacion/ventanaVoluntario.php?pagina=1";
    }
    if(window.location.search.includes(paginaLimite)){

        document.getElementById('siguientePagina').style.pointerEvents = "none";
    }
    if(window.location.search.includes(1)){
        document.getElementById('primeraPagina').style.pointerEvents = "none";
    }
    if(avanza)
        document.getElementById('primeraPagina').style.pointerEvents = "auto";
    if(!avanza)
        document.getElementById('siguientePagina').style.pointerEvents = "auto";
    if(avanza == null){
        if(window.location.search.includes(paginaLimite))
            document.getElementById('siguientePagina').style.pointerEvents = "none";    
        else
            document.getElementById('siguientePagina').style.pointerEvents = "auto";
        if(window.location.search.includes(1))
            document.getElementById('primeraPagina').style.pointerEvents = "none";    
        else
            document.getElementById('primeraPagina').style.pointerEvents = "auto";
    }
}

function verificarLimitePaginacion(avanza){
    
    var xhr=new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("contarFilas=true");

    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4 && xhr.status == 200){
            resultado = xhr.responseText;
            resultado = resultado/4;
            resultado = Math.ceil(resultado)-1;
            verificarPagina(resultado,avanza);

        }
    }
}

function cargarSiguienteInformacion(){
    var temp = calcularPaginaparaRegistros();
    temp = parseInt(temp)+1;
    history.pushState(null, "", "../CPresentacion/ventanaVoluntario.php?pagina= " + temp);
    verificarLimitePaginacion(true);
    Consultar(temp);


}
function cargarPreviaInformacion(){
    var temp = calcularPaginaparaRegistros();
    temp = parseInt(temp)-1;
    history.pushState(null, "", "../CPresentacion/ventanaVoluntario.php?pagina= " + temp);
    verificarLimitePaginacion(false);
    Consultar(temp);
}

function cargarTablaPaginacionNumerica(valor){
    history.pushState(null, "", "../CPresentacion/ventanaVoluntario.php?pagina= " + valor);
    verificarLimitePaginacion(null);
    Consultar(valor);

}

function crearPaginacion(){
    $("#pagination").empty();

    var resultado = 0;
    var xhr=new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("contarFilas=true");

    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4 && xhr.status == 200){
            resultado = xhr.responseText;
            resultado = resultado/4;
            resultado = Math.ceil(resultado)-1;
            for(var i = 0; i < resultado; i++){
                var listNode = document.getElementById('pagination'),
                    liNode = document.createElement("LI"),
                    txtNode = document.createTextNode(i);
                var href = document.createElement("a");
                href.textContent = i+1;
                href.setAttribute('onclick',"cargarTablaPaginacionNumerica('"+(parseInt(i)+1)+"')");
                liNode.appendChild(href);
                listNode.appendChild(liNode);

            }
            verificarPagina(resultado);
        }
    }

}

function calcularPaginaparaRegistros(){
    var url = window.location.search;
    var puertaRegistro = false;
    var numUrl = 0;
    if(url.length == 12){
        for(var i = 0; i < url.length; i++){
            if(puertaRegistro){
                if(url[i] == "%"){

                }
                else if(i >= 11){
                    numUrl += url[i];           
                }
            }
            if(url[i] == "="){
                puertaRegistro = true;
            }
        }
    }
    else if(url.length == 9){
        for(var i = 0; i < url.length; i++){
            if(puertaRegistro){
                numUrl += url[i];           
            }
            if(url[i] == "="){
                puertaRegistro = true;
            }
        }
    }
    return  numUrl;

}
