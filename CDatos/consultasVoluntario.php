<?php

	include 'conexion.php';

	class consultasVoluntario{

		public function consultasVoluntario() {}

		//REALIZA EL CONTENIDO DE LAS FILAS
		function contarFilas(){
			$conexion = new conexion();
			$respuesta = "";

			$contarFilas = "call contar_filas_Voluntario()";
			$resultado = mysqli_query($conexion->abrirConexion(),$contarFilas);
			if(mysqli_num_rows($resultado) > 0)
				while($row = mysqli_fetch_row($resultado))
					$respuesta = $row[0];
			$conexion->cerrarConexion();

			return $respuesta;
		}

		function buscarIntegranteEspecifico($dato){

			$conexion = new conexion();
			$resultado = $this->Query("call buscar_integrante_especifico_Voluntario('".$dato."')");

			if(mysqli_num_rows($resultado) > 0){

				$i = 0;
				$array = array();

				while($row = mysqli_fetch_row($resultado)){
					$array[$i] = new voluntario($row[0], $row[1], $row[2], $row[3]);
					$i++;
				}
				return $array;
			}

			return 0;
		}

		function Query($query) {

			$conexion = new conexion();
			$resultado = mysqli_query($conexion->abrirConexion(), $query);
			$conexion->cerrarConexion();
			
			return $resultado;
		}

		function Registrar($voluntario) {

			return $resultado = $this->Query("call insertar_un_Voluntario('".$voluntario->getNombre()."','".$voluntario->getTelefono()."','".$voluntario->getDetalle()."','".$voluntario->getId()."')");
		}

		function Consultar($pagina) {
			
			$resultado = $this->Query("call consultar_tabla_Voluntario('".$pagina."')");

			if (mysqli_num_rows($resultado) > 0) {

				$i = 0;
				$array = array();

				while ($row = mysqli_fetch_row($resultado)) {

					$array[$i] = new voluntario($row[0], $row[1], $row[2], $row[3]);
					$i++;
				}

				return $array;
			}

			return 0;
		}

		function Eliminar($id) {

			return $resultado = $this->Query("call eliminar_un_Voluntario('".$id."')");
		}

		function Seleccionar($id) {

			$resultado = $this->Query("call consultar_un_Voluntario('".$id."')");

			if (mysqli_num_rows($resultado) > 0) {

				$row = mysqli_fetch_row($resultado);

				return $voluntario = new voluntario($row[0], $row[1], $row[2], $row[3]);
			}

			return 0;
		}

		function Actualizar($voluntario) {

			return $resultado = $this->Query("call actualizar_un_Voluntario('".$voluntario->getId()."','".$voluntario->getNombre()."','".$voluntario->getTelefono()."','".$voluntario->getDetalle()."')");
		}
	}
?>