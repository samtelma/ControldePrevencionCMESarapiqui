<?php

declare(strict_types=1);

	class albergue {

		private $id;
		private $nombre;
		private $capacidad;
		private $encargado;
		private $localidad;
		private $telefono;
		private $luz;
		private $agua;
		private $cuartos;
		private $cocina;
		private $banos;

		public function albergue($id, 
								$nombre, 
								$capacidad, 
								$encargado, 
								$localidad, 
								$telefono, 
								$luz, 
								$agua, 
								$cuartos, 
								$cocina, 
								$banos) {

			$this->id        = $id;
			$this->nombre    = $nombre;
			$this->capacidad = $capacidad;
			$this->encargado = $encargado;
			$this->localidad = $localidad;
			$this->telefono  = $telefono;
			$this->luz       = $luz;
			$this->agua      = $agua;
			$this->cuartos   = $cuartos;
			$this->cocina    = $cocina;
			$this->banos     = $banos;
		}

		// Sets...
		public function setId($id) {
			$this->id = $id;
		}

		public function setNombre($nombre) {
			$this->nombre = $nombre;
		}

		public function setCapacidad($capacidad) {
			$this->capacidad = $capacidad;
		}

		public function setEncargado($encargado) {
			$this->encargado = $encargado;
		}

		public function setLocalidad($localidad) {
			$this->localidad = $localidad;
		}

		public function setTelefono($telefono) {
			$this->telefono = $telefono;
		}

		public function setLuz($luz) {
			$this->luz = $luz;
		}

		public function setAgua($agua) {
			$this->agua = $agua;
		}

		public function setCuartos($cuartos) {
			$this->cuartos = $cuartos;
		}

		public function setCocina($cocina) {
			$this->cocina = $cocina;
		}

		public function setBanos($banos) {
			$this->banos = $banos;
		}

		// Gets...
		public function getId() {
			return $this->id;
		}

		public function getNombre() {
			return $this->nombre;
		}

		public function getCapacidad() {
			return $this->capacidad;
		}

		public function getEncargado() {
			return $this->encargado;
		}

		public function getLocalidad() {
			return $this->localidad;
		}

		public function getTelefono() {
			return $this->telefono;
		}
		
		public function getLuz() {
			return $this->luz;
		}

		public function getAgua() {
			return $this->agua;
		}

		public function getCuartos() {
			return $this->cuartos;
		}

		public function getcocina() {
			return $this->cocina;
		}

		public function getBanos() {
			return $this->banos;
		}

		// toString...
		public function toString() {
			return $this->id."_".
					$this->nombre."_".
					$this->capacidad."_".
					$this->encargado."_".
					$this->localidad."_".
					$this->telefono."_".
					$this->luz."_".
					$this->agua."_".
					$this->cuartos."_".
					$this->cocina."_".
					$this->banos;
		}
	}
?>