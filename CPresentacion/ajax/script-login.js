// Funcion que baja el modal de inicio de secion.
$(document).ready(function ()
{
    mostrarInicioSession(); // Llama al metodo de bajar el modal.
    medirResolucion();
});

$(document).ready(function () {
    $(window).resize(function () {
        medirResolucion();
    });

});

function medirResolucion(){
    if($(window).width() <= 939){
        document.getElementById('label_sign-in').style.width = "30px";
    }
    if($(window).width() >959){
        document.getElementById('label_sign-in').style.width = "118px";
    }
    if($(window).width() > 907){
        document.getElementById('label_sign-in').style.width = "118px";
    }
    if($(window).width() <= 814){
        document.getElementById('label_sign-in').style.fontSize = "10px";
    }
    if($(window).width() > 814){
        document.getElementById('label_sign-in').style.fontSize = "medium";
    }
    if($(window).width() <= 516){
        document.getElementById('label_sign-in').innerHTML = "";
    }
    else{
        document.getElementById("label_sign-in").innerHTML = "Iniciar Sesión";
    }
}

// Muestra el modal de inicio de session.
function mostrarInicioSession()
{
    var label = document.getElementById("label_sign-in");
    if (label.innerText == "Iniciar Sesión" || label.innerText == "")
    {
        $("#modalLoginForm").modal("show");
        document.getElementById("usuario").value = ""; // Setea el campo usuario.
        document.getElementById("password").value = ""; // Setea el campo password.
    }
    else
    {
        location.reload(true);
    }
}

// Metodo de iniciar session, valida si es correcta los campos y hace una 
// peticion a la db de si es correcta.
function acceder()
{
    var usuario = document.getElementById("usuario").value;
    var password = document.getElementById("password").value;
    if(usuario != "" && password != ""){
        $("#modalLoginForm").modal("hide");
        document.getElementById("label_sign-in").innerText = "Prueba" + "(Salir)";
        document.getElementById('admins').style.display = "block";
        document.getElementById('admins2').style.display = "block";
        document.getElementById('admins3').style.display = "block";
    }
/*
    // Para que no se caiga la pagina.

    // Validamos si se ingresaron datos.
    if (usuario != "" && password != "")
    {
        // Consultamos en la tb para ver si es correcta.
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "CNegocio/controladoraLogin.php");
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        var enviar = "usuario=" + usuario + "&password=" + password;
        xhr.send(enviar);

        xhr.onreadystatechange = function ()
        {
            if (xhr.readyState == 4 && xhr.status == 200)
            {
                var resultado = xhr.responseText;
                //alert(resultado);
                // Verificamos el resultado.
                if (resultado != 0)
                {
                    // Datos correctos.
                    //alert("Datos correctos!");
                    $("#modalLoginForm").modal("hide");
                    document.getElementById("label_sign-in").innerText = resultado + "(Salir)";
                }
                else
                {
                    //$("#mensaje1").show();
                    //alert("Error en eliminar voluntario.\n" + resultado);
                    alert("Usuario o Contraseña Incorrectas");
                }
            }
        }
    }
    else
    {
        alert("Usuario o Contraseña Incorrectas");
    }
*/
}

$('#id_a_albergue').click(function ()
{
    location.herf = "../CPresentacion/ventanaAlbergue.php";
});
