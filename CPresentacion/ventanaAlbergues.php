<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Albergues</title>

    <link href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" rel="stylesheet" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="../CPresentacion/css/albergueCss.css" rel="stylesheet" type="text/css"/>

    <link rel="shortcut icon" href="../CPresentacion/imagenes/crc.png"/>
    <link reL="stylesheet" type="text/css" href="../CPresentacion/css/fonts.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"><!--agregar el icono de barras del mení responsive-->

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous""></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="../CPresentacion/ajax/jquery-3.3.1.min.js"></script>
    <script src="../CPresentacion/ajax/jquery.maskedinput-master/src/jquery.maskedinput.js" type="text/javascript"></script>
    <script src="../CPresentacion/ajax/ajaxAlbergues.js" type="text/javascript"></script>
</head>
<body>
    <?php include("includes/generic-header.html"); ?>
    <div id="page" class="container">
        <!-- Titulo de pagina -->
        <div id = "cabeceraPrincipal">
            <p id = "tituloCabecera">Secci&oacute;n de Albergues</p> 
            <p id = "encargado">Administrado(a) por: Michael Salas</p>
        <!-- Linea divisora -->
            <table id="opcionesMenuDirectorio">
                <thead>
                    <tr>
                        <td>
                            <!-- Boton Registrar nuevo Albergue -->
                            <button id="btn_registrar">Registrar Albergue</button>
                        </td>
                    </tr>
                </thead>
            </table>
        </div>
        <input type = 'text' id = 'buscar' maxlength = '40' size = '40' title = "Nombre, capacidad, localidad es su forma de b&uacute;squeda" onkeyup = "campoVacio()" onkeydown = "detectarTeclaEnter_enBusqueda(event);" placeholder = "Buscar albergue espec&iacute;fico    "></input>
        <!-- Formulario -->
        <!-- Tabla -->
        <div style = "position: relative;overflow: auto;">
            <table id="tabla">
                <tr>
                    <th id="id_numero-fila">Fila</th>
                    <th class="class_th">Nombre</th>
                    <th class="class_th">Capacidad</th>
                    <th class="class_th">Encargado</th>
                    <th class="class_th">Localidad</th>
                    <th class="class_th">Tel&eacute;fono</th>
                    <th class="class_th">Luz</th>
                    <th class="class_th">Agua</th>
                    <th class="class_th">Cuartos</th>
                    <th class="class_th">Cocina</th>
                    <th class="class_th">Baños</th>
                </tr>
            </table>
        </div>
                    <NAV>
                        <UL ID= "paginacion" CLASS= "paginacion">
                            <LI CLASS = "page-item" ID = 'paginaAnterior'>
                                <A CLASS = "page-link" ID = 'primeraPagina' ONCLICK = "cargarPreviaInformacion()">
                                    Anterior
                                </A>
                            </LI>

                            <LI ID = 'pagination' CLASS = "page-item">
                            </LI>
                            <LI CLASS = "page-item" ID = 'siguientePagina'>
                                <A CLASS = "page-link " ONCLICK = "cargarSiguienteInformacion()">
                                    Siguiente
                                </A>
                            </LI>
                        </UL>

                    </NAV>  

        <div id="modal">
            <!-- El cuadro visible -->
            <div id="modal-content">
                <!-- Cabecera -->
                <div class="modal-header">
                    <h2><p id="cedulaDetalle">Registro Albergue</p></h2>
                    <span class="close" onclick = "CancelarModal();">&times;</span>
                </div>
                <!-- Cuerpo del modal Formulario! -->
                <div class="modal-body">
                    <!-- Tabla del Formulario -->
                    <table id = "tablaxinformacionDetallada">
                        <tr>
                            <td hidden><input type="text" id = 'id_id' ></td>
                            <td>
                                <label for="nombre">Nombre:</label>
                                <font color=red ><label id="lNombre" class="asterisco"> *</label></font>
                                <br><input type="text" name="nombre" id="nombre" size="30" maxlength="30" placeholder="Michael Andres Salas Granados" onkeypress="return ValidarTexto(event)" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="capacidad">Capacidad:</label>
                                <br><input type="text" name="capacidad" id="capacidad" size="30" maxlength="11" onkeypress="return ValidarNumeros(event)" placeholder="100" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="encargado">Encargado:</label>
                                <br><select name="responsable" id="responsable" onclick="ObtenerTelefono();">
                                    <option value="">Sin definir</option> 
                                </select>
                                <br><label>Tel&eacute;fono:</label>
                                <br><input type="text" id="telefono" disabled="" value="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="localidad">Localidad:</label>
                                <br><input type="text" name="localidad" id="localidad" size="30" maxlength="30" placeholder="La victoria, Horquetas" onkeypress="return ValidarTexto(event)" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="servicios">Servicios:</label>
                                <br/>
                                <input type="checkbox" id="luz" value="luz"/>Luz
                                <br/>
                                <input type="radio" name="agua" id="agua_potable" value="agua_potable"/>Agua Potable
                                <br/>
                                <input type="radio" name="agua" id="agua_no_potable" value="agua_no_potable"/>Agua no Potable
                                <br/>
                                <!-- nuevos servicios.-->
                                <label for="cuartos">Cuartos:</label>
                                <input type="text" name="cuartos" id="cuartos" size="5" maxlength="5" placeholder="10" onkeypress="return ValidarNumeros(event)" />
                                <br/>
                                <label for="banos">Baños:</label>
                                <input type="text" name="banos" id="banos" size="5" maxlength="5" placeholder="10" onkeypress="return ValidarNumeros(event)" />
                                <br/>
                                <label for="cocina">Cocina:</label>
                                <input type="text" name="cocina" id="cocina" size="5" maxlength="5" placeholder="10" onkeypress="return ValidarNumeros(event)" />
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- Pie del formulario -->
                <div class="modal-footer">
                    <div id = 'botones'>
                        <input type="button" value="Registrar" id="btn" class="btn" onclick="BTN();" />
                        <input type="button" value="Cancelar" id="cancelar" class="btn" />                        
                    </div>
                </div>
            </div>
        </div>                    
        <!-- Modal confirmacion -->
        <div id="modalEliminar">
            <!-- El cuadro visible -->
            <div id="modal-contentEliminar">
                <!-- Cabecera -->
                <div class="modal-headerEliminar">
                    <h2><p id="cedulaDetalle">Eliminar Albergue</p></h2>
                    <span class="closeEliminar" onclick = "CancelarModal();">&times;</span>
                </div>
                <!-- Cuerpo del modal Formulario! -->
                <div class="modal-bodyEliminar">
                    <!-- Tabla del Formulario -->
                    <table id = "tablaxinformacionDetallada">
                        <tr>
                            <td id = "label_confirmacion">¿Desea realmente eliminarlo?</td>                                
                        </tr>
                        <tr>
                            <td><label id = "label_id_albergue" value = ""/></td>
                        </tr>
                    </table>
                </div>
                <!-- Pie del formulario -->
                <div class="modal-footerEliminar">
                    <input type="button" value="Eliminar" id="btn_confirmar" class="btn_confirmacion" onclick="Eliminar();" />
                    <input type="button" value="Cancelar" id="btn_cancelar_confirmacion" class="btn_confirmacion " onclick="Cancelar();"/>
                </div>
            </div>
        </div>
    </div>
    <?php include("includes/generic-footer.html"); ?>      
</body>
</html>