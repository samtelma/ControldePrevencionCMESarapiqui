<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="../CPresentacion/imagenes/crc.png" />
    <title>Agenda</title>   
    <script type="text/javascript" src = "../CPresentacion/ajax/ajaxAgenda.js"></script>
    <script src="../CPresentacion/ajax/jquery.maskedinput-master/src/jquery.maskedinput.js" type="text/javascript"></script>
    <script src="../CPresentacion/ajax/jquery-3.3.1.min.js"></script>
    <meta NAME="viewport" CONTENT="width=device-width, initial-scale=1, maximum-scale=1"/>
</head>
<body>
 
 <table>
    <tr>
       <th hidden>
            <INPUT TYPE = 'TEXT' NAME = 'id' ID = 'id'>
        </th>
        <th>
            <INPUT TYPE = 'TEXT' NAME = 'titulo' ID = 'titulo' PLACEHOLDER = 'Escriba el t&iacute;tulo de la actividad'>
        </th>
    </tr>
    <tr>
        <th>
            <select id = 'tipoActividad' name = 'tipoActividad'>
                <option VALUE = "" DISABLED selected>Seleccione el tipo de actividad</option>
                <option value = "Auditoria">Auditoria</option>
                <option value = "Campaña">Campaña</option>
                <option value = "Capacitaci&oacute;n">Capacitaci&oacute;n</option>
                <option value = "Foro">Foro</option>
                <option value = "Gira">Gira</option>
                <option value = "Rendici&oacute;n Cuenta">Rendici&oacute;n Cuenta</option>
                <option value = "Reuni&oacute;n">Reuni&oacute;n</option>
            </select>
            <select id = "privacidadActividad" name = "privacidadActividad">
                <option value = "" disabled selected>Selecciona la privacidad</option>
                <option value = "Privada">Privada</option>
                <option value = "P&uacute;blica">P&uacute;blica</option>
            </select>
        </th>
    </tr>
    <tr>
        <th>
            <input type = "date" name = "fechaActividad" id = "fechaActividad">
            <input type = "time" name = "horaActividad" id = "horaActividad">
        </th>
    </tr>
    <tr>
        <th>
            <textarea id = "descripcionActividad" name = "descripcionActividad" placeholder = "Reseña de actividad"></textarea>
        </th>
    </tr>
    <tr>
        <th>
            <input type = "text" name = "encargadoActividad" id = "encargadoActividad" placeholder = "Encargado de la actividad">
            <input type = "text" name = "informacionActividad" id = "informacionActividad" placeholder = "N&uacute;mero o correo para informaci&oacute;n" onkeypress="return validarInformacion(event)">
        </th>
    </tr>
    <tr>
        <th>
            <input type = "button" name = "aceptar" id = "aceptar" value = "Agendar" onclick="aceptarCambios()">
        </th>
    </tr>

 </table>

 <div style = "position: relative;overflow: auto;">
    <table>
        <tr>
            <th>T&iacute;tulo</th>
            <th>Tipo de Actividad</th>
            <th>Descripci&oacute;n</th>
            <th>Fecha</th>
            <th>Opci&oacute;n</th>
        </tr>
        <tr>
            <tbody id = "detalle"></tbody>
        </tr>
    </table>     
 </div>
</body>
</html>