<?php

class AlbergueTest extends \PHPUnit\Framework\TestCase{

    /** @test */
    public function generica(){
        
        $this->assertTrue(true); 
    }



    /** @test */
    public function asignar_obtener_codigo_de_albergue(){
        
        $albergue = new \CDominioT\albergue;
        

        $albergue->setId('1578-PV');
        $this->assertEquals($albergue->getId(), '1578-PV');
    }


    /** @test */
    public function probar_constructor_y_servicios_albergue(){
        
        $albergue = new \CDominioT\albergue(33, 'ptv', 250, 'roy', 'puerto viejo', 'basicos', '2766-1000');
        

        $albergue->setServicios('basicos');
        $this->assertEquals($albergue->getServicios(), 'basicos');
    }

    /** @test */
    public function asignar_obtener_telefono_de_albergue(){
        
        $albergue = new \CDominioT\albergue;
        

        $albergue->setTelefono(2766-1000);
        $this->assertEquals($albergue->getTelefono(), 2766-1000);
    }

}