<?php


	class voluntario {

		private $id;
		private $nombre;
		private $telefono;
		private $detalle;

		public function voluntario($id, $nombre, $telefono, $detalle) {

			$this->id = $id;
			$this->nombre = $nombre;
			$this->telefono = $telefono;
			$this->detalle = $detalle;
		}

		// Sets...
		public function setId($id) {

			$this->id = $id;
		}

		public function setNombre($nombre) {

			$this->nombre = $nombre;
		}

		public function setTelefono($telefono) {

			$this->telefono = $telefono;
		}

		public function setDetalle($detalle) {

			$this->detalle = $detalle;
		}

		// Gets...
		public function getId() {

			return $this->id;
		}

		public function getNombre() {

			return $this->nombre;
		}

		public function getTelefono() {

			return $this->telefono;
		}

		public function getDetalle() {

			return $this->detalle;
		}

		// toString...
		public function toString() {

			return $this->id."_".$this->nombre."_".$this->telefono."_".$this->detalle;
		}
	}
?>