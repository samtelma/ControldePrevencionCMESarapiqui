<?php
	include '../CDatos/consultasArticulo.php';  // Donde estan las consultas.

	if (isset($_POST['registrar'])) {
		
		echo Registrar();
	} else if (isset($_POST['consultar'])) {
		
		echo Consultar();
	} else if (isset($_POST['eliminar'])) {
		
		echo Eliminar();
	} else if (isset($_POST['seleccionar'])) {

		echo Seleccionar();
	} else if (isset($_POST['actualizar'])) {

		echo Actualizar();
	}
	function Registrar() {
		//REGISTRAR UN NUEVO INSUMO.
		$nombre      = $_POST['nombre'];
		$descripcion = $_POST['descripcion'];

		$consulta = new consultasArticulo();
		$resultado = $consulta->Registrar($nombre, $descripcion);

		return $resultado;
	} // Fin Registrar().
	function Consultar() {
		//CONSULTAR LA LISTA DE INSUMOS.
		$consultas = new consultasArticulo(); // Instanciamos la clase consultas.
		$resultado = $consultas->Consultar(); 

		return $resultado;
	} // Fin Consultar().
	function Eliminar() {
		// ELIMINAR UN CLIENTE.
		$id = $_POST['eliminar'];
		$consultas = new consultasArticulo();
		$resultado = $consultas->Eliminar($id);

		return $resultado;
	} // Fin Eliminar().
	function Seleccionar() {
		// SELECCIONAR UN CLIENTE.
		$id = $_POST['seleccionar'];
		$consultas = new consultasArticulo();

		return $consultas->Seleccionar($id);
	} // Fin Seleccionar().
	function Actualizar() {
		// ACTUALIZAR UN INSUMO.
		$id          = $_POST['id'];
		$nombre      = $_POST['nombre'];
		$descripcion = $_POST['descripcion'];
		// VALIDAR LOS ESPACIOS.

		$consulta = new consultasArticulo();
		$resultado = $consulta->Actualizar($id, $nombre, $descripcion);

		return $resultado;
	} // Fin Actualizar().
?>