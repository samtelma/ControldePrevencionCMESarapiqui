-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-04-2019 a las 23:28:24
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdcme`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE PROCEDURE `actualizar_agenda` (IN `id` INT(5), IN `tituloAgenda` VARCHAR(80), IN `privacidadAgenda` VARCHAR(50), IN `fechaAgenda` DATE, IN `horaAgenda` TIME, IN `descripcionAgenda` VARCHAR(500), IN `encargadoAgenda` VARCHAR(100), IN `informacionAgenda` VARCHAR(200), IN `tipoActividad` VARCHAR(100))  NO SQL
BEGIN
	UPDATE tbagenda SET
    tbagenda.tituloAgenda = tituloAgenda,
    tbagenda.tipoAgenda = tipoAgenda,
    tbagenda.privacidadAgenda = privacidadAgenda,
    tbagenda.fechaAgenda = fechaAgenda,
    tbagenda.horaAgenda = horaAgenda,
    tbagenda.descripcionAgenda = descripcionAgenda,
    tbagenda.encargadoAgenda = encargadoAgenda,
    tbagenda.informacionAgenda = informacionAgenda
    WHERE
    tbagenda.idAgenda = id;
END$$

CREATE PROCEDURE `actualizar_integrante` (IN `dato1` VARCHAR(50), IN `dato2` VARCHAR(20), IN `dato3` VARCHAR(100), IN `dato4` VARCHAR(100), IN `dato5` VARCHAR(100), IN `dato6` VARCHAR(18), IN `dato7` VARCHAR(100), IN `dato8` VARCHAR(30), IN `dato9` VARCHAR(100), IN `dato10` VARCHAR(150))  NO SQL
BEGIN
	UPDATE tbdirectorio SET
    	tbdirectorio.cedulaIntegrante = dato2,
        tbdirectorio.nombreIntegrante = dato3,
        tbdirectorio.apellido1Integrante = dato4,
        tbdirectorio.apellido2Integrante = dato5,
        tbdirectorio.telefonoIntegrante = dato6,
        tbdirectorio.institucionrepresentadaIntegrante = dato7,
        tbdirectorio.puestoIntegrante = dato8,
        tbdirectorio.comiteIntegrante = dato9,
        tbdirectorio.correoIntegrante = dato10 
	WHERE tbdirectorio.idIntegrante = dato1;
END$$

CREATE PROCEDURE `actualizar_una_bodega` (IN `_id` INT, `_nombrebodega` VARCHAR(30), `_descripcionbodega` VARCHAR(100), `_direccionbodega` VARCHAR(100), `_responsablebodega` INT(5))  begin
	update tbbodega 
	set 
		nombrebodega = _nombrebodega,
		descripcionbodega = _descripcionbodega,
		direccionbodega = _direccionbodega,
        responsablebodega = _responsablebodega
	where idbodega = _id;
end$$

CREATE PROCEDURE `actualizar_una_ZonaRiesgo` (IN `_id` INT, `_lugar` VARCHAR(100), `_tipo_riesgo` VARCHAR(30), `_albergue_cercano` VARCHAR(100))  begin
	update tbzonariesgo 
	set 
		lugarzonariesgo = _lugar,
		tipozonariesgo = _tipo_riesgo,
		alberguezonariesgo = _albergue_cercano
	where idzonariesgo = _id;
end$$

CREATE PROCEDURE `actualizar_un_Albergue` (IN `_id` INT, IN `_nombrealbergue` VARCHAR(30), IN `_capacidadalbergue` INT(4), IN `_encargadoalbergue` VARCHAR(100), IN `_localidadalbergue` VARCHAR(100), IN `_telefonoalbergue` INT(11), IN `_luz` INT(1), IN `_agua` INT(1), IN `_cuarto` INT(5), IN `_cocina` INT(5), IN `_bano` INT(5))  BEGIN
	UPDATE tbalbergues SET nombrealbergue=_nombrealbergue,
											capacidadalbergue=_capacidadalbergue,
											encargadoalbergue=_encargadoalbergue,
											localidadalbergue=_localidadalbergue,
											telefonoalbergue=_telefonoalbergue,
											luzalbergue=_luz,
											aguaalbergue=_agua,
											cuartosalbergue=_cuarto,
											cocinaalbergue=_cocina,
											banosalbergue=_bano
											WHERE idalbergue=_id;
END$$

CREATE PROCEDURE `actualizar_un_Insumos` (IN `_id` INT(5), IN `_articuloinsumo` VARCHAR(30), IN `_institucioninsumo` VARCHAR(30), IN `_cantidadinsumo` VARCHAR(5), IN `_observacioninsumo` VARCHAR(100), IN `_bodegainsumo` INT(5))  begin
	update tbinsumo 
	set 
		articuloinsumo = _articuloinsumo, 
        institucioninsumo = _institucioninsumo, 
        cantidadinsumo = _cantidadinsumo,
        observacioninsumo = _observacioninsumo,
        bodegainsumo = _bodegainsumo
	where idinsumo = _id;
end$$

CREATE PROCEDURE `actualizar_un_Responsable` (IN `_id` INT, `_cedularesponsable` VARCHAR(11), `_nombreresponsable` VARCHAR(11), `_apellido1responsable` VARCHAR(30), `_apellido2responsable` VARCHAR(30), `_telefonoresponsable` VARCHAR(11))  begin
	update tbresponsable 
	set 
		cedularesponsable = _cedularesponsable,
		nombreresponsable = _nombreresponsable,
		apellido1responsable = _apellido1responsable,
		apellido2responsable = _apellido2responsable,
		telefonoresponsable = _telefonoresponsable
	where idresponsable = _id;
end$$

CREATE PROCEDURE `actualizar_un_Voluntario` (IN `_id` INT, `_nombrevoluntario` VARCHAR(30), `_telefonovoluntario` VARCHAR(11), `_detallevoluntario` VARCHAR(100))  begin
	update tbvoluntario 
	set 
		nombrevoluntario = _nombrevoluntario,
		telefonovoluntario = _telefonovoluntario,
		detallevoluntario = _detallevoluntario
	where idvoluntario = _id;
end$$

CREATE PROCEDURE `buscar_comite` ()  NO SQL
BEGIN
	SELECT DISTINCT tbdirectorio.comiteIntegrante FROM tbdirectorio;
END$$

CREATE PROCEDURE `buscar_especifico_Albergue` (IN `_dato` VARCHAR(100))  begin
	select * from tbalbergues where nombrealbergue = _dato or capacidadalbergue = _dato or localidadalbergue = _dato;
end$$

CREATE PROCEDURE `buscar_especifico_Insumo` (IN `_dato` VARCHAR(100))  begin
	select * from tbinsumo where articuloinsumo = _dato
							or institucioninsumo = _dato
							or cantidadinsumo = _dato
							or bodegainsumo = _dato;
end$$

CREATE PROCEDURE `buscar_integrantes` (IN `dato` INT(10))  NO SQL
BEGIN
	SELECT * FROM tbdirectorio 
    ORDER BY tbdirectorio.idIntegrante DESC LIMIT dato,6;
END$$

CREATE PROCEDURE `buscar_integrante_especifico-2variables` (IN `dato1` VARCHAR(100), IN `dato2` VARCHAR(100))  NO SQL
BEGIN
	SELECT * FROM tbdirectorio WHERE 
    tbdirectorio.nombreIntegrante = dato1 AND tbdirectorio.apellido1Integrante = dato2 
    	OR 
   	tbdirectorio.nombreIntegrante = dato1 AND tbdirectorio.apellido2Integrante = dato2
    	OR
    tbdirectorio.nombreIntegrante = dato1 AND tbdirectorio.institucionrepresentadaIntegrante = dato2
    	OR
    tbdirectorio.nombreIntegrante = dato1 AND tbdirectorio.comiteIntegrante = dato2
    	OR
   	tbdirectorio.apellido1Integrante = dato1 AND tbdirectorio.apellido2Integrante = dato2
    	OR
    tbdirectorio.institucionrepresentadaIntegrante = dato1 AND tbdirectorio.comiteIntegrante= dato2;
END$$

CREATE PROCEDURE `buscar_integrante_especifico_3variables` (IN `dato1` VARCHAR(100), IN `dato2` VARCHAR(100), IN `dato3` VARCHAR(100))  NO SQL
BEGIN 
	SELECT * FROM tbdirectorio WHERE
    tbdirectorio.nombreIntegrante = dato1 AND 
    tbdirectorio.apellido1Integrante = dato2 AND 
    tbdirectorio.apellido2Integrante = dato3 OR
    tbdirectorio.nombreIntegrante = dato1 AND
    tbdirectorio.apellido2Integrante = dato2 AND
    tbdirectorio.apellido1Integrante = dato3 OR
    tbdirectorio.nombreIntegrante = dato1 AND
    tbdirectorio.apellido1Integrante = dato2 AND
    tbdirectorio.institucionrepresentadaIntegrante = dato3 OR
    tbdirectorio.nombreIntegrante = dato1 AND
    tbdirectorio.apellido2Integrante = dato2 AND
    tbdirectorio.institucionrepresentadaIntegrante = dato3 OR
    tbdirectorio.nombreIntegrante = dato1 AND
    tbdirectorio.apellido1Integrante = dato2 AND
    tbdirectorio.comiteIntegrante = dato3 OR
    tbdirectorio.apellido1Integrante = dato1 AND
    tbdirectorio.apellido2Integrante = dato2 AND
    tbdirectorio.nombreIntegrante = dato3 OR
    tbdirectorio.nombreIntegrante = dato1 AND
    tbdirectorio.institucionrepresentadaIntegrante = dato2 AND
    tbdirectorio.comiteIntegrante = dato3;
END$$

CREATE PROCEDURE `buscar_integrante_especifico_4variables` (IN `dato1` VARCHAR(100), IN `dato2` VARCHAR(100), IN `dato3` VARCHAR(100), IN `dato4` VARCHAR(100))  NO SQL
BEGIN
	SELECT * FROM tbdirectorio WHERE
    tbdirectorio.nombreIntegrante = dato1 AND
    tbdirectorio.apellido1Integrante = dato2 AND
    tbdirectorio.apellido2Integrante = dato3 AND
    tbdirectorio.institucionrepresentadaIntegrante = dato4
    	OR
	tbdirectorio.nombreIntegrante = dato1 AND
    tbdirectorio.apellido1Integrante = dato2 AND
    tbdirectorio.institucionrepresentadaIntegrante = dato3 AND
    tbdirectorio.comiteIntegrante = dato4;
END$$

CREATE PROCEDURE `buscar_integrante_especifico_Bodega` (IN `_dato` VARCHAR(100))  begin
    select * from tbbodega where responsablebodega = _dato or direccionbodega = _dato or nombrebodega = _dato;
end$$

CREATE PROCEDURE `buscar_integrante_especifico_Voluntario` (IN `_dato` VARCHAR(100))  begin
	select * from tbvoluntario where idvoluntario = _dato or nombrevoluntario = _dato or telefonovoluntario = _dato;
end$$

CREATE PROCEDURE `buscar_intregrante_especifico` (IN `_dato` VARCHAR(100))  NO SQL
BEGIN
	SELECT * FROM tbdirectorio WHERE
    tbdirectorio.idIntegrante = _dato OR
    tbdirectorio.nombreIntegrante = _dato OR
    tbdirectorio.apellido1Integrante = _dato OR
    tbdirectorio.apellido2Integrante = _dato OR
    tbdirectorio.institucionrepresentadaIntegrante = _dato OR
    tbdirectorio.cedulaIntegrante = _dato OR
    tbdirectorio.comiteIntegrante = _dato;
END$$

CREATE PROCEDURE `buscar_por_bodega_un_Insumo` (IN `_id` INT(5))  begin
	select idinsumo, articuloinsumo, institucioninsumo, cantidadinsumo, observacioninsumo, nombrebodega
		from tbinsumo 
		inner join tbbodega on tbinsumo.bodegainsumo = tbbodega.idbodega
		where idinsumo = _id;
end$$

CREATE PROCEDURE `buscar_registroAgenda` ()  NO SQL
BEGIN
	SELECT * FROM tbagenda;
END$$

CREATE PROCEDURE `buscar_responsable_especifico` (IN `dato` VARCHAR(80))  NO SQL
SELECT * FROM tbresponsable WHERE nombreresponsable = dato OR
																				apellido1responsable =dato OR
																				apellido2responsable = dato OR
																				cedularesponsable = dato$$

CREATE PROCEDURE `buscar_responsable_especifico_2variables` (IN `datoxBuscar1` VARCHAR(80), IN `datoxBuscar2` VARCHAR(80))  NO SQL
SELECT * FROM tbresponsable WHERE 
						nombreresponsable = datoxBuscar1 AND apellido1responsable = datoxBuscar2 OR 
						nombreresponsable= datoxBuscar1 AND apellido2responsable = datoxBuscar2 OR
						apellido1responsable= datoxBuscar1 AND apellido2responsable = datoxBuscar2$$

CREATE PROCEDURE `buscar_responsable_especifico_3variables` (IN `datoxBuscar` VARCHAR(80), IN `datoxBuscar2` VARCHAR(80), IN `datoxBuscar3` VARCHAR(80))  NO SQL
SELECT * FROM tbresponsable WHERE 
					nombreresponsable= datoxBuscar AND apellido1responsable = datoxBuscar2 AND		apellido2responsable = datoxBuscar3 OR
					nombreresponsable= datoxBuscar AND apellido2responsable = datoxBuscar2 AND
					apellido1responsable = datoxBuscar3 OR
					apellido1responsable= datoxBuscar AND apellido2responsable = datoxBuscar2 AND 
					nombreresponsable = datoxBuscar3$$

CREATE PROCEDURE `buscar_ultimo_iddirectorio` ()  NO SQL
BEGIN
	SELECT MAX(tbdirectorio.idIntegrante) FROM tbdirectorio;
END$$

CREATE PROCEDURE `buscar_ultimo_registroAgenda` ()  NO SQL
BEGIN
	SELECT MAX(tbagenda.idAgenda) FROM tbagenda;
END$$

CREATE PROCEDURE `cargar_datos_bodega_en_Insumos` ()  begin
	select nombrebodega from tbbodega;
end$$

CREATE PROCEDURE `cargar_nombres_responsables_Albergue` ()  begin
	select nombreresponsable, apellido1responsable, apellido2responsable from tbresponsable;
end$$

CREATE PROCEDURE `consultar_nombres_albergues` ()  begin
	select nombrealbergue from tbalbergues;
end$$

CREATE PROCEDURE `consultar_tabla_Albergue` (IN `_pagina` INT(10))  begin
	select * FROM tbalbergues 
				INNER JOIN tbresponsable ON tbalbergues.encargadoalbergue = tbresponsable.idresponsable
				ORDER BY idalbergue DESC LIMIT _pagina, 6;
end$$

CREATE PROCEDURE `consultar_tabla_Bodega` (IN `_pagina` INT(10))  begin
	/*select * from tbbodega order by idbodega desc limit _pagina, 6;*/
    select idbodega, nombrebodega, descripcionbodega, direccionbodega, nombreresponsable 
		from tbbodega inner join tbresponsable on tbbodega.responsablebodega = tbresponsable.idresponsable
        where idbodega != 1 ORDER BY idbodega DESC LIMIT _pagina,6;
end$$

CREATE PROCEDURE `consultar_tabla_Insumos` (IN `_pagina` INT(10))  begin
	select idinsumo, articuloinsumo, institucioninsumo, cantidadinsumo, observacioninsumo, nombrebodega
		from tbinsumo inner join tbbodega on tbinsumo.bodegainsumo = tbbodega.idbodega 
        order by idinsumo desc limit _pagina,6;
end$$

CREATE PROCEDURE `consultar_tabla_Responsable` (IN `_pagina` INT(10))  begin
	select * from tbresponsable where idresponsable != 0 order by idresponsable desc limit _pagina, 6;
end$$

CREATE PROCEDURE `consultar_tabla_Voluntario` (IN `_pagina` INT(10))  begin
	select * from tbvoluntario order by idvoluntario desc limit _pagina, 6;
end$$

CREATE PROCEDURE `consultar_tabla_ZonaRiesgo` ()  begin
	select * from tbzonariesgo;
end$$

CREATE PROCEDURE `consultar_una_Bodega` (IN `_id` INT)  begin
	select * from tbbodega where idbodega = _id;
end$$

CREATE PROCEDURE `consultar_una_ZonaRiesgo` (IN `_id` INT)  begin
	select * from tbzonariesgo where idzonariesgo = _id;
end$$

CREATE PROCEDURE `consultar_un_Insumos` (IN `_id` INT)  begin
	select idinsumo, articuloinsumo, institucioninsumo, cantidadinsumo, observacioninsumo, nombrebodega 
		from tbinsumo 
		inner join tbbodega on tbinsumo.bodegainsumo = tbbodega.idbodega
		where idinsumo = _id;
end$$

CREATE PROCEDURE `consultar_un_Responsable` (IN `_id` INT)  begin
	select * from tbresponsable where idresponsable = _id;
end$$

CREATE PROCEDURE `consultar_un_Voluntario` (IN `_id` INT)  begin
	select * from tbvoluntario where idvoluntario = _id;
end$$

CREATE PROCEDURE `contar_filas_Albergue` ()  begin
	select count(idalbergue) from tbalbergues;
end$$

CREATE PROCEDURE `contar_filas_Bodegas` ()  begin
	select count(idbodega) from tbbodega;
end$$

CREATE PROCEDURE `contar_filas_directorio` ()  NO SQL
BEGIN
	SELECT count(tbdirectorio.idIntegrante) FROM tbdirectorio;
END$$

CREATE PROCEDURE `contar_filas_Insumos` ()  begin
	select count(idinsumo) from tbinsumo;
end$$

CREATE PROCEDURE `contar_filas_Responsable` ()  begin
	select count(idresponsable) from tbresponsable;
end$$

CREATE PROCEDURE `contar_filas_Voluntario` ()  begin
	select count(idvoluntario) from tbvoluntario;
end$$

CREATE PROCEDURE `devolver_integrante` (IN `dato` VARCHAR(100))  NO SQL
BEGIN
	SELECT * FROM tbdirectorio WHERE
    tbdirectorio.idIntegrante = dato;
END$$

CREATE PROCEDURE `eliminar_agenda` (IN `id` INT(5))  NO SQL
BEGIN
	DELETE FROM tbagenda WHERE tbagenda.idAgenda = id;
END$$

CREATE PROCEDURE `eliminar_albergues_de_zonas_de_riesgo` (IN `_id` INT(10))  begin
    set @nombre := (select nombrealbergue from tbalbergues where idalbergue = _id);
	update tbzonariesgo set alberguezonariesgo = 'Sin definir'
    where alberguezonariesgo = @nombre;
end$$

CREATE PROCEDURE `eliminar_integrante` (IN `dato` VARCHAR(50))  NO SQL
BEGIN
	DELETE FROM tbdirectorio WHERE tbdirectorio.idIntegrante = dato;
END$$

CREATE PROCEDURE `eliminar_tabla_Albergue` ()  begin
	delete from tbalbergues;
end$$

CREATE PROCEDURE `eliminar_tabla_Insumos` ()  begin
	delete from tbinsumo;
end$$

CREATE PROCEDURE `eliminar_tabla_Responsable` ()  begin
	delete from tbresponsable;
end$$

CREATE PROCEDURE `eliminar_tabla_Voluntario` ()  begin
	delete from tbzonariesgo;
end$$

CREATE PROCEDURE `eliminar_tabla_ZonaRiesgo` ()  begin
	delete from tbzonariesgo;
end$$

CREATE PROCEDURE `eliminar_una_Bodega` (IN `_id` INT)  begin
    update tbinsumo set bodegainsumo = 1 where bodegainsumo = _id;
    delete from tbbodega where idbodega = _id;
end$$

CREATE PROCEDURE `eliminar_una_ZonaRiesgo` (IN `id` INT)  begin
	delete from tbzonariesgo where idzonariesgo = id;
end$$

CREATE PROCEDURE `eliminar_un_Albergue` (IN `_id` INT)  begin
    set @nombre := (select nombrealbergue from tbalbergues where idalbergue = _id);
	update tbzonariesgo set alberguezonariesgo = 'Sin definir' where alberguezonariesgo = @nombre;
    delete from tbalbergues where idalbergue = _id;
end$$

CREATE PROCEDURE `eliminar_un_Insumos` (IN `_id` INT)  begin
	delete from tbinsumo where idinsumo = _id;
end$$

CREATE PROCEDURE `eliminar_un_Responsable` (IN `_id` INT)  begin
	update tbbodega set responsablebodega = 0 where responsablebodega = _id;
	delete from tbresponsable where idresponsable = _id;
end$$

CREATE PROCEDURE `eliminar_un_Voluntario` (IN `_id` INT(5))  begin
	delete from tbvoluntario where idvoluntario = _id;
end$$

CREATE PROCEDURE `insertar_integrante` (IN `dato1` VARCHAR(20), IN `dato2` VARCHAR(20), IN `dato3` VARCHAR(100), IN `dato4` VARCHAR(100), IN `dato5` VARCHAR(100), IN `dato6` VARCHAR(20), IN `dato7` VARCHAR(100), IN `dato8` VARCHAR(50), IN `dato9` VARCHAR(100), IN `dato10` VARCHAR(150))  NO SQL
BEGIN 
	INSERT INTO tbdirectorio 
    VALUES
    (dato1,dato2,dato3,dato4,dato5,dato6,dato7,dato8,dato9,dato10);
END$$

CREATE PROCEDURE `insertar_una_Bodega` (IN `_nombreBodega` VARCHAR(30), `_descripcionBodega` VARCHAR(100), `_direccionBodega` VARCHAR(100), `_responsableBodega` VARCHAR(30))  begin
	insert into tbbodega (
		nombrebodega, 
        descripcionbodega,
        direccionbodega,
        responsablebodega
	) values (
		_nombreBodega,
		_descripcionBodega, 
		_direccionBodega,
        _responsableBodega
	);
end$$

CREATE PROCEDURE `insertar_una_ZonaRiesgo` (IN `_lugar` VARCHAR(100), `_tipo_riesgo` VARCHAR(30), `_albergue_cercano` VARCHAR(100), `_latitud` DOUBLE, `_longitud` DOUBLE)  begin
	insert into tbzonariesgo (
		lugarzonariesgo, 
        tipozonariesgo, 
        alberguezonariesgo, 
        latitud, 
        longitud)
        values (_lugar, _tipo_riesgo, _albergue_cercano, _latitud, _longitud);
end$$

CREATE PROCEDURE `insertar_un_Albergue` (IN `_nombrealbergue` VARCHAR(30), IN `_capacidadalbergue` INT(4), IN `_encargadoalbergue` INT(5), IN `_localidadalbergue` VARCHAR(150), IN `_telefono` INT(11), IN `_luz` TINYINT(1), IN `_agua` TINYINT(1), IN `_cuartos` INT(5), IN `_cocina` INT(5), IN `_banos` INT(5), IN `_idalbergue` INT(5))  BEGIN
	INSERT INTO tbalbergues  
VALUES
(_idalbergue,
 _nombrealbergue,
 _capacidadalbergue,
 _encargadoalbergue,
 _localidadalbergue,
 _telefono,
 _luz,
 _agua,
 _cuartos,
 _cocina,
 _banos);
END$$

CREATE PROCEDURE `insertar_un_Insumos` (IN `_articuloinsumo` VARCHAR(30), IN `_institucioninsumo` VARCHAR(11), IN `_cantidadinsumo` VARCHAR(5), IN `_observacioninsumo` VARCHAR(100), IN `_bodegainsumo` INT(5), IN `_idinsumo` INT(5))  begin
	insert into tbinsumo values (
        _idinsumo,
		_articuloinsumo,
		_institucioninsumo, 
		_cantidadinsumo,
        _observacioninsumo,
        _bodegainsumo
	);
end$$

CREATE PROCEDURE `insertar_un_Responsable` (IN `_cedularesponsable` VARCHAR(11), `_nombreresponsable` VARCHAR(11), `_apellido1responsable` VARCHAR(30), `_apellido2responsable` VARCHAR(30), `_telefonoresponsable` VARCHAR(11))  begin
	insert into tbresponsable (
	cedularesponsable,
    nombreresponsable,
    apellido1responsable,
    apellido2responsable,
    telefonoresponsable
	) values (
	_cedularesponsable,
    _nombreresponsable,
    _apellido1responsable,
    _apellido2responsable,
    _telefonoresponsable
	);
end$$

CREATE PROCEDURE `insertar_un_Voluntario` (IN `_nombrevoluntario` VARCHAR(30), IN `_telefonovoluntario` VARCHAR(11), IN `_detallevoluntario` VARCHAR(100), IN `_id` INT)  begin
	insert into tbvoluntario values (
        _id,
		_nombrevoluntario,
		_telefonovoluntario, 
		_detallevoluntario
	);
end$$

CREATE PROCEDURE `obtener_id_bodega_por_nombre_de_Insumos` (IN `_nombre` VARCHAR(100))  begin
	select idbodega from tbbodega where nombrebodega =_nombre;
end$$

CREATE PROCEDURE `obtener_id_responsable_por_nombre` (IN `_nombre` VARCHAR(100))  begin
	select idresponsable from tbresponsable where nombreresponsable = _nombre;
end$$

CREATE PROCEDURE `obtener_id_responsable_por_nombre_Albergue` (IN `_nombre` VARCHAR(30), IN `_apellido1` VARCHAR(100), IN `_apellido2` VARCHAR(100))  begin
	select idresponsable from tbresponsable 
    where nombreresponsable =_nombre AND 
    apellido1responsable = _apellido1 AND 
    apellido2responsable = _apellido2;
end$$

CREATE PROCEDURE `obtener_nombres_responsables_de_bodega` ()  begin
	select nombreresponsable from tbresponsable;
end$$

CREATE PROCEDURE `obtener_nombre_responsable_por_id_bodega` (IN `_id` VARCHAR(5))  begin
	select nombreresponsable from tbresponsable where idresponsable = _id;
end$$

CREATE PROCEDURE `registrar_agenda` (IN `id` INT(5), IN `tituloAgenda` VARCHAR(130), IN `tipoActividad` VARCHAR(100), IN `privacidadAgenda` VARCHAR(50), IN `fechaAgenda` DATE, IN `horaAgenda` TIME, IN `descripcionAgenda` VARCHAR(500), IN `encargadoAgenda` VARCHAR(100), IN `informacionAgenda` VARCHAR(200))  NO SQL
BEGIN
	INSERT INTO tbagenda VALUES (id,tituloAgenda,tipoActividad,privacidadAgenda,fechaAgenda,horaAgenda,descripcionAgenda,encargadoAgenda,informacionAgenda);
END$$

CREATE PROCEDURE `seleccionar_actividad_agenda` (IN `_id` INT(5))  NO SQL
BEGIN
	SELECT * FROM tbagenda WHERE tbagenda.idAgenda = _id;
END$$

CREATE PROCEDURE `seleccionar_Albergue` (IN `_dato` VARCHAR(50))  NO SQL
BEGIN
SELECT idalbergue, 
							nombrealbergue, 
							capacidadalbergue,
							nombreresponsable,
                            apellido1responsable,
                            apellido2responsable, 
							localidadalbergue, 
							telefonoalbergue,
							luzalbergue,
							aguaalbergue,
							cuartosalbergue,
							cocinaalbergue,
							banosalbergue FROM tbalbergues 
				INNER JOIN tbresponsable on tbalbergues.encargadoalbergue = tbresponsable.idresponsable
				WHERE idalbergue= _dato;
END$$

CREATE PROCEDURE `si_tiene_llave_foranea_Bodega` (IN `_id` VARCHAR(5))  begin
	select * from tbbodega inner join tbinsumo on tbinsumo.bodegainsumo = tbbodega.idbodega where idbodega = _id;
end$$

CREATE PROCEDURE `verificar_cedula_repetida_Responsable` (IN `_id` INT, `_cedula` VARCHAR(11))  begin
	select idresponsable, cedularesponsable from tbresponsable where cedularesponsable = _cedula;
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbagenda`
--

CREATE TABLE `tbagenda` (
  `idAgenda` int(5) NOT NULL,
  `tituloAgenda` varchar(130) NOT NULL,
  `tipoAgenda` varchar(100) NOT NULL,
  `privacidadAgenda` varchar(50) NOT NULL,
  `fechaAgenda` date NOT NULL,
  `horaAgenda` time NOT NULL,
  `descripcionAgenda` varchar(500) NOT NULL,
  `encargadoAgenda` varchar(100) NOT NULL,
  `informacionAgenda` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbagenda`
--

INSERT INTO `tbagenda` (`idAgenda`, `tituloAgenda`, `tipoAgenda`, `privacidadAgenda`, `fechaAgenda`, `horaAgenda`, `descripcionAgenda`, `encargadoAgenda`, `informacionAgenda`) VALUES
(1, 'Recolección de Basura', 'Campaña', 'Privada', '2019-04-02', '13:30:00', 'En Río Frío', 'Daniel Bustos', '85749663'),
(2, 'Default', 'Auditoria', 'Privada', '2019-04-04', '15:25:00', 'Default', 'Default', 'Default');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbalbergues`
--

CREATE TABLE `tbalbergues` (
  `idalbergue` int(5) NOT NULL,
  `nombrealbergue` varchar(30) NOT NULL,
  `capacidadalbergue` int(4) DEFAULT NULL,
  `encargadoalbergue` int(5) DEFAULT NULL,
  `localidadalbergue` varchar(100) NOT NULL,
  `telefonoalbergue` int(8) DEFAULT NULL,
  `luzalbergue` tinyint(1) NOT NULL,
  `aguaalbergue` tinyint(1) DEFAULT NULL,
  `cuartosalbergue` int(5) NOT NULL,
  `cocinaalbergue` int(5) NOT NULL,
  `banosalbergue` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbalbergues`
--

INSERT INTO `tbalbergues` (`idalbergue`, `nombrealbergue`, `capacidadalbergue`, `encargadoalbergue`, `localidadalbergue`, `telefonoalbergue`, `luzalbergue`, `aguaalbergue`, `cuartosalbergue`, `cocinaalbergue`, `banosalbergue`) VALUES
(34, 'ASADA Horquetas', 100, 15, 'Horquetas ', 84745896, 1, 1, 4, 1, 2),
(40, 'Colegio Río Frío', 550, 3, 'Rio Frio, Finca 11', 83838522, 1, 1, 20, 2, 6),
(41, 'UNA', 750, 17, 'Rio Frio, Frente San Marcos', 857415263, 1, 1, 16, 1, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbbodega`
--

CREATE TABLE `tbbodega` (
  `idbodega` int(5) NOT NULL,
  `nombrebodega` varchar(30) NOT NULL,
  `descripcionbodega` varchar(100) NOT NULL,
  `direccionbodega` varchar(100) NOT NULL,
  `responsablebodega` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbbodega`
--

INSERT INTO `tbbodega` (`idbodega`, `nombrebodega`, `descripcionbodega`, `direccionbodega`, `responsablebodega`) VALUES
(1, 'Sin definir', '', '', '0'),
(23, 'asada', 'asada', 'horquetas ', 'Michaell');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbcomite`
--

CREATE TABLE `tbcomite` (
  `idComite` int(10) NOT NULL,
  `nombreComite` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbdirectorio`
--

CREATE TABLE `tbdirectorio` (
  `idIntegrante` int(5) NOT NULL,
  `cedulaIntegrante` varchar(20) DEFAULT NULL,
  `nombreIntegrante` varchar(30) NOT NULL,
  `apellido1Integrante` varchar(30) NOT NULL,
  `apellido2Integrante` varchar(30) DEFAULT NULL,
  `telefonoIntegrante` varchar(18) DEFAULT NULL,
  `institucionrepresentadaIntegrante` varchar(50) DEFAULT NULL,
  `puestoIntegrante` varchar(60) NOT NULL,
  `comiteIntegrante` varchar(50) DEFAULT NULL,
  `correoIntegrante` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbdirectorio`
--

INSERT INTO `tbdirectorio` (`idIntegrante`, `cedulaIntegrante`, `nombreIntegrante`, `apellido1Integrante`, `apellido2Integrante`, `telefonoIntegrante`, `institucionrepresentadaIntegrante`, `puestoIntegrante`, `comiteIntegrante`, `correoIntegrante`) VALUES
(1, '123456789', 'Default', 'Default', 'Default', '11122233', 'Default', 'Propietario', 'Municipal de Emergencias', ''),
(2, '123456789', 'Default', 'Default', 'Default', '11122244', 'Default', 'Asistente', 'Ejecutivo de Emergencias', ''),
(8, '702520355', 'Yazmín', 'Jiménez ', 'Guerrero', '84081630', 'Banco Nacional', 'Propietario', 'Ninguno', ''),
(9, '115950328', 'Michael', 'Salas', 'Granados', '87542165', 'Design Soft', 'Propietario', 'Ninguno', ''),
(10, '115950407', 'Dennis', 'Muñoz', 'Madrigal', '27643713/89467505', 'Ninguna', 'Propietario', 'Ninguno', ''),
(11, '603750305', 'Josue', 'Avellan', 'Alvarado', '86186343', 'FUNDECOR', 'Asistente', 'Rio Frio', ''),
(12, '115950408', 'Juan', 'Granados', 'Acuña', '27643718', 'INDER', 'Asistente', 'Municipal de Emergencias', ''),
(13, '207824689', 'Ana', 'Rodriguez', 'Alfaro', '86547821', 'MAG', 'Asistente', 'Municipal de Emergencias', ''),
(14, '308541287', 'Priscila', 'Montoya', 'Aguilar', '85745123', 'ASADA', 'Propietario', 'Municipal de Emergencias', ''),
(15, '607389175', 'Alexa', 'Salazar', 'Picado', '20145789', 'MEP', 'Asistente', 'Ejecutivo de Emergencias', ''),
(16, '687924571', 'Joel', 'Alfaro', 'Ortiz', '85764923', 'FUNDECOR', 'Asistente', 'Municipal de Emergencias', ''),
(17, '705893115', 'Sofia', 'Aguilar', 'Morales', '60524841', 'CNE', 'Asistente', 'Municipal de Emergencias', ''),
(18, '504712846', 'Ramon', 'Baltodano', 'Ramirez', '86432812', 'Bomberos CR', 'Asistente', 'Municipal de Emergencias', ''),
(19, '604784125', 'MARILYN', 'DE LOS ANGELES', 'MARIN', '88547412', 'mep', 'Asistente', 'Municipal de Emergencias', 'mari.marin@mep.go.cr'),
(20, '115950406', 'Dennis', 'Muñoz', 'Azofeifa', '83838522', 'Ninguna', 'Ciudadano', 'Municipal de Emergencias', 'samtelma@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbinsumo`
--

CREATE TABLE `tbinsumo` (
  `idinsumo` int(5) NOT NULL,
  `articuloinsumo` varchar(30) NOT NULL,
  `institucioninsumo` varchar(30) NOT NULL,
  `cantidadinsumo` varchar(5) DEFAULT NULL,
  `observacioninsumo` varchar(100) DEFAULT NULL,
  `bodegainsumo` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbinsumo`
--

INSERT INTO `tbinsumo` (`idinsumo`, `articuloinsumo`, `institucioninsumo`, `cantidadinsumo`, `observacioninsumo`, `bodegainsumo`) VALUES
(17, '11', '3213123', '23123', '123132', 10),
(18, 'Colchones', 'UNA Sarapiqui', '150', 'limpios y listos para usar, estan empaquetados en el galeron de recreacion y turismo', 23);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbresponsable`
--

CREATE TABLE `tbresponsable` (
  `idresponsable` int(5) NOT NULL,
  `cedularesponsable` varchar(11) NOT NULL,
  `nombreresponsable` varchar(30) NOT NULL,
  `apellido1responsable` varchar(30) NOT NULL,
  `apellido2responsable` varchar(30) NOT NULL,
  `telefonoresponsable` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbresponsable`
--

INSERT INTO `tbresponsable` (`idresponsable`, `cedularesponsable`, `nombreresponsable`, `apellido1responsable`, `apellido2responsable`, `telefonoresponsable`) VALUES
(2, '1-1595-0328', 'Michaell', 'Salas', 'Granados', '86-75-54-06'),
(3, '1-1595-0406', 'Dennis', 'Muñoz', 'Azofeifa', '83-83-85-22'),
(13, '1-1111-1111', 'Luiz', 'Guillermo', 'Sanabria', '12-34-56-78'),
(14, '13456789', 'Josue', 'Avellan', 'Alvarado', '85858471'),
(15, '98765432', 'Rachel', 'Bolivar', 'Solano', '84745896'),
(16, '74185296', 'Gerberth ', 'Aleman', 'Fonseca', '87542163'),
(17, '96635285', 'Ileana', 'Schmidt', 'Fonseca', '857415263'),
(18, '96857478', 'Carlos', 'Escalante', 'Solano', '96855241'),
(19, '63524174', 'Wilberth', 'Rodriguez', 'Recino', '32658754'),
(20, '63524174', 'Steven', 'Cruz', 'Sancho', '84659887'),
(21, '321654714', 'Cristian', 'Brenes', 'Granados', '84635241'),
(22, '6587544212', 'Eithel', 'Trigueros', 'Rodriguez', '54184653');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbvoluntario`
--

CREATE TABLE `tbvoluntario` (
  `idvoluntario` int(5) NOT NULL,
  `nombrevoluntario` varchar(30) DEFAULT NULL,
  `telefonovoluntario` varchar(11) DEFAULT NULL,
  `detallevoluntario` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbvoluntario`
--

INSERT INTO `tbvoluntario` (`idvoluntario`, `nombrevoluntario`, `telefonovoluntario`, `detallevoluntario`) VALUES
(11, 'Luis', '22-22-22-22', 'Estoy desempleado, asi que tengo tiempo libre'),
(12, 'Pedro', '11-22-33-44', 'detalle  '),
(13, 'Carlos', '84524163', 'algo del mundo quiero hacer'),
(14, 'Maria', '85966352', 'quiero ayudar mi comunidad'),
(15, 'JORGE', '74859663', 'me gustar participar en actividades sociales  '),
(16, 'Emily', '85526341', 'siempre quise ayudar ante las emergencias'),
(17, 'Diego', '85966352', 'quiero vincular mi empresa con esta acción'),
(18, 'Alexandra', '85749663', 'quiero ayudar con la parte de atención de personas'),
(19, 'Kevin', '85965210', 'quiero aportar mi colaboración en los albergues con actividades formativas'),
(20, 'Yazmin', '85206374', 'soy lider de un grupo de scouts que podrían apoyar'),
(21, 'Esteban', '87986554', 'mi empresa puede aportar víveres '),
(24, 'Paula Vargas', '85749663', 'soy experta en actividades lúdicas para los niños en albergues ante alguna emergencia '),
(28, 'Kenneth Moraga', '85749632', 'Me gusta colaborar con la comunidad y mientras esté en la U quiero apoyar     '),
(29, 'Esteban Chaves', '2276-4852', 'Quiero hacer algo por la vida ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbzonariesgo`
--

CREATE TABLE `tbzonariesgo` (
  `idzonariesgo` int(5) NOT NULL,
  `lugarzonariesgo` varchar(100) DEFAULT NULL,
  `tipozonariesgo` varchar(30) DEFAULT NULL,
  `alberguezonariesgo` varchar(30) DEFAULT NULL,
  `latitud` double NOT NULL,
  `longitud` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbzonariesgo`
--

INSERT INTO `tbzonariesgo` (`idzonariesgo`, `lugarzonariesgo`, `tipozonariesgo`, `alberguezonariesgo`, `latitud`, `longitud`) VALUES
(18, 'Casa de Michael', 'Inundacion', 'Sin definir', 10.3228914, -83.9126362),
(19, 'Casa de Avellan', 'Sequia', 'Sin definir', 10.320309, -83.906806),
(21, 'Casa de Dennis', 'Caida de meteorito', 'Sin definir', 10.321221, -83.913768),
(22, 'Universidad', 'Lluvia acida', 'Sin definir', 10.3189586, -83.92290849999999),
(23, 'prueba', 'adasas', 'Sin definir', 10.324337, -83.925004),
(24, 'AAAAAa', 'aaa', 'Sin definir', 10.318960299999999, -83.92290679999999),
(25, 'zzzzzz', 'zzzzzzz', 'Sin definir', 10.321203667633291, -83.92549840327149),
(29, 'Carenalga', 'destello', 'Sin definir', 10.445450383447593, -84.04731461166989),
(30, 'a', 'a', 'Sin definir', 10.4456192, -84.0433664);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbagenda`
--
ALTER TABLE `tbagenda`
  ADD PRIMARY KEY (`idAgenda`);

--
-- Indices de la tabla `tbalbergues`
--
ALTER TABLE `tbalbergues`
  ADD PRIMARY KEY (`idalbergue`);

--
-- Indices de la tabla `tbbodega`
--
ALTER TABLE `tbbodega`
  ADD PRIMARY KEY (`idbodega`);

--
-- Indices de la tabla `tbcomite`
--
ALTER TABLE `tbcomite`
  ADD PRIMARY KEY (`idComite`);

--
-- Indices de la tabla `tbdirectorio`
--
ALTER TABLE `tbdirectorio`
  ADD PRIMARY KEY (`idIntegrante`);

--
-- Indices de la tabla `tbinsumo`
--
ALTER TABLE `tbinsumo`
  ADD PRIMARY KEY (`idinsumo`),
  ADD KEY `bodegainsumo` (`bodegainsumo`);

--
-- Indices de la tabla `tbvoluntario`
--
ALTER TABLE `tbvoluntario`
  ADD PRIMARY KEY (`idvoluntario`);

--
-- Indices de la tabla `tbzonariesgo`
--
ALTER TABLE `tbzonariesgo`
  ADD PRIMARY KEY (`idzonariesgo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbalbergues`
--
ALTER TABLE `tbalbergues`
  MODIFY `idalbergue` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT de la tabla `tbbodega`
--
ALTER TABLE `tbbodega`
  MODIFY `idbodega` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `tbinsumo`
--
ALTER TABLE `tbinsumo`
  MODIFY `idinsumo` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `tbvoluntario`
--
ALTER TABLE `tbvoluntario`
  MODIFY `idvoluntario` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `tbzonariesgo`
--
ALTER TABLE `tbzonariesgo`
  MODIFY `idzonariesgo` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
