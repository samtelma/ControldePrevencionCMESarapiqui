window.onload = function (event) {

    LimpiarCampos();
    Consultar();
    crearPaginacion();
    verificarPagina();
}

$(document).ready(function(){
    
   $('#btn_registrar').click(function(){
        document.getElementById("cedulaDetalle").style.margin = "0px -600px 0px 0px";
        document.getElementById("modal").style.top = "0px";
        document.getElementById('botones').style.margin = "0px 190px 0px 0px";
        Cargar();
        LimpiarCampos();
   });
   $('#cancelar').click(function(){

        document.getElementById("modal").style.top = "-500vh";
        LimpiarCampos();
   });

    document.getElementsByClassName("close")[0].onclick = function() {
            document.getElementById('modal').style.top = "-500vh";
    }

     window.onclick = function(event) {
        if (event.target == document.getElementById('modal')) {
            document.getElementById('modal').style.top = "-500vh";
            document.getElementById('modalEliminar').style.top = "-500vh";
        }
    }
/*
    document.getElementsByClassName("closeEliminar")[0].onclick = function() {
            document.getElementById('modalEliminar').style.top = "-500vh";
    }

    //CUANDO EL USUARIO DA CLICK EN CUALQUIER LUGAR FUERA DEL MODAL CIERRA EL MODAL
    window.onclick = function(event) {
        if (event.target == document.getElementById('modalEliminar')) {
            document.getElementById('modalEliminar').style.top = "-500vh";
        }
    }*/
});

function LimpiarCampos() {

    OcultarMensajes();

    document.getElementById("btn").value = "Registrar";
    document.getElementById("nombre").value = "";
    document.getElementById("descripcion").value = "";
    document.getElementById("direccion").value = ""; // Metodo que se encarga de limpiar los campos de textos.
    document.getElementById('buscar').value = "";
}

function OcultarMensajes() {

    $(".mensaje").hide();
    $(".asterisco").hide();
}

function BTN() { // Metodo que se encarga de ver si es registrar o actualizar.

    OcultarMensajes();

    if (document.getElementById("btn").value == "Registrar") {

        Registrar();
    } else {

        Actualizar();
    }
}

function AceptarTexto(e) {

    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8 || tecla == 0 || tecla == 44 || tecla == 32) {

        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros y letras
    patron = /[A-Za-z0-9]/;
    tecla_final = String.fromCharCode(tecla);

    return patron.test(tecla_final);
}

function Cargar() {

    try {

        // Obtenermos los datos proveniente de AJAX desde la controladora.
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "../CNegocio/controladoraBodega.php");
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send("cargar");

        xhr.onreadystatechange = function () {

            if (xhr.readyState == 4 && xhr.status == 200) {

                var resultado = xhr.responseText;

                //alert(resultado);
                if (resultado != "") {

                    //alert(resultado);
                    var res = resultado.split("+");
                    var select = document.getElementById("responsable");
                    select.options[0] = new Option("Sin definir");

                    for (var i = 0; i < res.length - 1; i++) {

                        select.options[i+1] = new Option(res[i]);
                    }
                } else { //Error!

                    //alert("Error en cargar datos del encargado!\n" + resultado);
                }
            }
        }
    } catch (error) {

        console.error(error);
    }
}

function Registrar() { // metodo que registra una nueva bodega.

    try {

        var nombre = document.getElementById("nombre").value;

        // Se puede continuar si solo si, el nombre se ingreso.
        if (nombre != "") {

            // Obtenemos los demas datos.
            var descripcion = document.getElementById("descripcion").value;
            var direccion   = document.getElementById("direccion").value;
            var responsable = document.getElementById("responsable").value;

            // Creando el STRING para enviar a la controladora.
            var enviar = "registrar=true" + "&nombre=" + nombre + 
                                            "&descripcion=" + descripcion + 
                                            "&direccion=" + direccion + 
                                            "&responsable=" + responsable;

            // Lo enviamos por AJAX a la controladora.
            var xhr = new XMLHttpRequest();
            xhr.open("POST", "../CNegocio/controladoraBodega.php");
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.send(enviar);

            xhr.onreadystatechange = function () {

                if (xhr.readyState == 4 && xhr.status == 200) {

                    var resultado = xhr.responseText;

                    alert(resultado);
                    if (resultado != 0) {

                        //alert(resultado);
                        LimpiarCampos();
                        Consultar();
                        $("#mensaje2").show();

                    } else { // Error !!!.

                        $("#mensaje3").show();
                        //alert("Error en la registrar una bodega.\n" + resultado);
                    }
                }
            }
        } else {

            // Si el nombre esta vacio.
            $("#lNombre").show(); // Mostrar un asterisco que indique donde esta el error.
            $("#mensaje5").show(); // Mostrar un mensaje que indique cual es el error.
        }
    } catch (error) {

        console.error(error);
    }
}

function Consultar() { // Metodo que consulta la lista de bodegas regstradas.

    var pagina = calcularPaginaparaRegistros();

    if (pagina == 1) {

        pagina = parseInt(pagina-1);
    } else {

        pagina = parseInt(pagina*3);
    }

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraBodega.php");
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("consultar=true" + "&pagina=" + pagina);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != "null") {

                $("tr").remove(".cla");
                $("#tabla").append(resultado);
            }
        }
    }
}

function ConfirmarEliminacion(id) {
    alert();
    document.getElementById("cedulaDetalle").style.margin = "0px 160px";
    document.getElementById("modalEliminar").style.top = "0px";
    document.getElementById("label_id_bodega").innerHTML = id;
}

function Eliminar() { // Metodo que elimina una bodega.
    document.getElementById("modalEliminar").style.top = "-500vh";
    var temp = document.getElementById("label_id_bodega").innerHTML;
    OcultarMensajes();

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraBodega.php");
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("eliminar=" + temp);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {

                //alert(resultado);
                if (resultado == 1) { // Exitoso.

                    Consultar();
                    $("#mensaje3").show();
                } else {

                    $("#mensaje6").show();
                }
            } else {

                $("#mensaje1").show();
                //alert("Error en eliminar bodega.\n" + resultado);
            }
        }
    }
}

function Cancelar() {

    document.getElementById("label_id_bodega").innerHTML = "";
    document.getElementById('modalEliminar').style.top = "-500vh";
}

function Seleccionar(id) { // Metodo que selecciona una bodega.

    OcultarMensajes();
    Cargar();
    document.getElementById("cedulaDetalle").innerHTML = "Actualizar Bodega";
    document.getElementById("modal").style.top = "0px";
    document.getElementById("cedulaDetalle").style.margin = "0px -600px 0px 0px";
    document.getElementById('botones').style.margin = "0px 190px 0px 0px";

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraBodega.php");
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("seleccionar=" + id);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {

                var cadena = resultado.split("-");
                document.getElementById("id_id").value = cadena[0];
                document.getElementById("nombre").value = cadena[1];
                document.getElementById("descripcion").value = cadena[2];
                document.getElementById("direccion").value = cadena[3];
                document.getElementById("responsable").value = cadena[4];
                document.getElementById("btn").value = "Actualizar";
            } else {

                $("#mensaje1").show();
                //alert("Error en seleccionar.\n" + resultado);
            }
        }
    }
}

function Actualizar() { // Metodo que actualiza una bodega.

    var nombre = document.getElementById("nombre").value;

    if (nombre != "") {

        // Obtenemos los demas datos.
        var id = document.getElementById("id_id").value;
        var descripcion = document.getElementById("descripcion").value;
        var direccion = document.getElementById("direccion").value;
        var responsable = document.getElementById("responsable").value;

        // Creamos el STRING para enviarlo a consulta.
        var enviar ="actualizar=true" + "&id=" + id + 
                                        "&nombre=" + nombre + 
                                        "&descripcion=" + descripcion + 
                                        "&direccion=" + direccion + 
                                        "&responsable=" + responsable;

        // Lo enviamos a la controladora por AJAX.                                        
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "../CNegocio/controladoraBodega.php");
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(enviar);

        xhr.onreadystatechange = function () {

            if (xhr.readyState == 4 && xhr.status == 200) {

                var resultado = xhr.responseText;

                //alert(resultado);
                if (resultado != 0) {

                    //alert(resultado);
                    //LimpiarCampos();
                    Consultar();
                    $("#mensaje4").show();
                } else {

                    $("#mensaje1").show();
                    //alert("Error en actualizar.\n" + resultado);
                }
            }
        }
    } else {

        $("#lNombre").show();
        $("#mensaje5").show();
    }
}

function Mas(i) {
    
    if (document.getElementById(i).style.display == "none" || document.getElementById(i).style.display == "") {

        document.getElementById(i).style.display = "block";
        document.getElementById("btn" + i).value = "Menos...";
    } else {

        document.getElementById(i).style.display = "none";
        document.getElementById("btn" + i).value = "Mas...";
    }
    
    //alert(i);
}

function CancelarModal() {

    document.getElementById("modal").style.top = "-500vh";
    LimpiarCampos();
}

function detectarTeclaEnter_enBusqueda(e) {

    var evt = e ? e : event;
    var key = window.Event ? evt.which : evt.keyCode;

    if (key == 13) {

        buscarBodega();
    }
}

function buscarBodega() {

    var buscar = document.getElementById('buscar').value;
    var tablaBodega = document.getElementById('tabla');
    var td = "";
    var tr = "";
    var temp = "";
    var dato = "Sin resultado";
    var i = 1; 
    var puerta = false;

    if (buscar != "") {

        while (i < tablaBodega.rows.length) {

            tr = tablaBodega.rows[i];
            for (var j = 1; j < tr.cells.length-1; j++) {

                td = tr.cells[j].innerHTML;

                if (td.toUpperCase().includes(buscar.toUpperCase())) {

                    temp = tr.cells[0].innerHTML + ",";

                    if (!puerta) {

                        puerta = true;
                        dato = "";
                    }
                    if (!dato.includes(temp)) {

                        dato += temp;
                    }                    
                }
            }
            i++;
        }
        if (dato != "Sin resultado") {

            for (var i = 1 ; i < tablaBodega.rows.length; i++) {

                if (dato.includes(tablaBodega.rows[i].cells[0].innerHTML)) {

                    tablaBodega.rows[i].style.display = "";
                }
                else {
                    tablaBodega.rows[i].style.display = "none";
                }
            }
        }
        else {
            if (dato == "Sin resultado") {

                buscar_en_baseD(buscar);
            }
            else {

                resultadoBusqueda(dato);
            }
        }
    }
    if (buscar == "") {

        resultadoBusqueda(buscar);
    }
}

function buscar_en_baseD(variable) {

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraBodega.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("buscarBodegaEspecifica=" + variable);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;
            
            if (resultado != 0) {

                $("tr").remove(".cla");
                $("#tabla").append(resultado);
            }
            else{

                resultadoBusqueda("Sin resultado");
            }
        }
    }
}

function campoVacio() {

    if (document.getElementById('buscar').value == "") {

        resultadoBusqueda("");
    }   
}

function resultadoBusqueda(texto) {

    if (texto == "Sin resultado") {

        document.getElementById('buscar').value = "Sin resultados";
        document.getElementById('buscar').style.color = "red";
        document.getElementById('tabla').style.display = "";
    }
    else if (document.getElementById('buscar').value == "") {

        document.getElementById('buscar').style.color = "black";
        Consultar();
    }
}

function verificarPagina(paginaLimite) {

    if (window.location.search == "") {

        document.getElementById('primeraPagina').style.pointerEvents = "none";
        location.href = "../CPresentacion/ventanaBodega.php?pagina=1";
    }
    if (window.location.search.includes(paginaLimite)) {

        document.getElementById('siguientePagina').style.pointerEvents = "none";
    }
    if (window.location.search.includes(1)) {

        document.getElementById('primeraPagina').style.pointerEvents = "none";
    }
}

function crearPaginacion() {

    $("#pagination").empty();

    var resultado = 0;
    var xhr=new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraBodega.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("contarFilas=true");

    xhr.onreadystatechange = function() {

        if (xhr.readyState == 4 && xhr.status == 200) {

            resultado = xhr.responseText;
            resultado = resultado/4;
            resultado = Math.ceil(resultado)-1;

            for (var i = 0; i < resultado; i++) {

                var listNode = document.getElementById('pagination'),
                    liNode = document.createElement("LI"),
                    txtNode = document.createTextNode(i);
                var href = document.createElement("a");
                href.textContent = i+1;
                href.setAttribute('href',"../CPresentacion/ventanaBodega.php?pagina="+(i+1));
                liNode.appendChild(href);
                listNode.appendChild(liNode);
            }
            verificarPagina(resultado);
        }
    }
}

function calcularPaginaparaRegistros() {

    var url = window.location.search;
    var puertaRegistro = false;
    var numUrl = 0;

    if (url.length == 12) {

        for (var i = 0; i < url.length; i++) {

            if (puertaRegistro) {

                if (url[i] == "%") {

                }
                else if(i >= 11) {

                    numUrl += url[i];           
                }
            }
            if (url[i] == "=") {

                puertaRegistro = true;
            }
        }
    }
    else if (url.length == 9) {

        for (var i = 0; i < url.length; i++) {

            if (puertaRegistro) {

                numUrl += url[i];           
            }
            if (url[i] == "=") {

                puertaRegistro = true;
            }
        }
    }
    return  numUrl;
}