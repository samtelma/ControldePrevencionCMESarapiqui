<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">

	<title>cme sarapiqu&iacute</title>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="shortcut icon" href="CPresentacion/imagenes/crc.png" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="CPresentacion/css/estilos.css" href="">

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="CPresentacion/ajax/ajaxLogin.js" type="text/javascript"></script>
	<script type="text/javascript" src="CPresentacion/ajax/script-login.js"></script>
</head>
<body>
	<!--====== SE LLAMA EL HEADER-LOGIN ======-->

	<?php include "includes/generic-header-login.html";?>

	<!--====== CONTIENE TODO LA PAGINA NORMAL ======-->

	<div class="container">
		<div id="contenedor">
			<section class="main row">
				<div id="carouselExampleIndicators" class="col-xs-12 col-sm-8 col-md-9 carousel slide" data-ride="carousel">
				  	<ol class="carousel-indicators">
				    	<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
				    	<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
				    	<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
				  	</ol>
				  	<div class="carousel-inner">
				    	<div class="carousel-item active">
				      		<img class="d-block w-100" src="CPresentacion/imagenes/carousel/1.jpg" alt="Primer slide">
				    	</div>
					    <div class="carousel-item">
					      	<img class="d-block w-100" src="CPresentacion/imagenes/carousel/2.jpg" alt="Segundo slide">
					    </div>
					    <div class="carousel-item">
					      	<img class="d-block w-100" src="CPresentacion/imagenes/carousel/3.jpg" alt="Tercer slide">
					    </div>
					    <div class="carousel-item">
					      	<img class="d-block w-100" src="CPresentacion/imagenes/carousel/4.jpg" alt="Cuarto slide">
					    </div>
					    <div class="carousel-item">
					      	<img class="d-block w-100" src="CPresentacion/imagenes/carousel/5.jpg" alt="Quinto slide">
					    </div>
					    <div class="carousel-item">
					      	<img class="d-block w-100" src="CPresentacion/imagenes/carousel/6.jpg" alt="Sexto slide">
					    </div>
				  	</div>
				  	<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
				    	<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				    	<span class="sr-only">Previous</span>
				  	</a>
				  	<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
				    	<span class="carousel-control-next-icon" aria-hidden="true"></span>
				    	<span class="sr-only" style="position: relative;">Next</span>
				  	</a>
				</div>
				<aside class="col-xs-12 col-sm-4 col-md-3">
				    <div class="contain">
					    <div id="myCarousel" class="carousel slide" data-ride="carousel">
				            <!-- Indicators -->
				            <ol class="carousel-indicators">
				                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				                <li data-target="#myCarousel" data-slide-to="1"></li>
				                <li data-target="#myCarousel" data-slide-to="2"></li>
				                <li data-target="#myCarousel" data-slide-to="3"></li>
				            </ol>
				            <!-- Wrapper for slides -->
				            <div class="carousel-inner">
				                <div class="carousel-item active">
				                    <img class="d-block w-100" src="CPresentacion/imagenes/carousel/anuncio-1.jpg" alt="primer anuncio">
				                </div>
				                <div class="carousel-item">
				                    <div class="fill" style=" background-color:#ffff00;">2</div>
				                </div>
				                <div class="carousel-item">
				                    <div class="fill" style=" background-color:#FF0000;">3
				                     <br><p>Comisi&oacute;n Nacional de Emergencias para m&aacute;s info: <a href="https://cne.go.cr/" rel="nofollow">CNE 	Costa Rica</a>
				                    </div>
				                </div>
				                <div class="carousel-item">
				                    <div class="fill" style=" background-color:#C0C0C0;">4</div>
				                </div>
				            </div>
				        </div>
				    </div>
				</aside>
			</section>
			<div class="row">

				<div id = "menudos" class=" col-xs-12 col-sm-9 col-md-6">
					<p>
						<h3>Rese&ntilde;a CME</h3>
							<h6>COMIT&Eacute;S REGIONALES, MUNICIPALES Y COMUNALES DE EMERGENCIA</h6>
								
								<div class="color5" id = "textosComplementarios">
									Comit&eacute;s regionales, municipales y comunales de emergencia son instancias permanentes de coordinaci&oacute;n en los niveles regional, municipal y comunal. Por medio de ellos, la CNE cumple su funci&oacute;n de coordinaci&oacute;n de las instituciones p&uacute;blicas, privadas, organismos no gubernamentales y la sociedad civil, que trabajan en la atenci&oacute;n de emergencias o desastres. Se integran con la representaci&oacute;n institucional o sectorial de los funcionarios con mayor autoridad en el nivel correspondiente. Las organizaciones no gubernamentales, las privadas, las municipales y comunales, definir&aacute;n su representaci&oacute;n por medio de la autoridad interna de cada una de ellas. 
								</div>						
					</p>

					<!--a href="#" class="boton">Leer m&aacute;s</a-->
					<button type="button" class="btn btn-info btn-sm">Leer m&aacute;s</button>

				</div>

				<div id = "menudos" class="col-xs-12 col-sm-6 col-md-3">
					<p>
						<h3>&Aacute;rea de voluntariado</h3>
						<div>
							<img id = "imagen" src="CPresentacion/imagenes/pic01.jpg" alt="">
						</div>
						<p class="color4">
							<br><label>¿Le gustaria formar parte de nuestros voluntarios?</label></br>
							<br><INPUT TYPE = 'button' value = 'Inscribirse' id = 'botonInscripcionVoluntariado' class="btn btn-info btn-sm"/></br>
						</p>

					</p>
				</div>

				<!--div class="clearfix visible-sm-block"></div-->  <!-- para ajustar las columnas pero ya lo hace por defecto solo que se alarga demasiado-->

				<div id = "menudos" class="col-xs-12 col-sm-6 col-md-3">   <!-- col-md-offset-3 SUPUESTAMENTE movia las columnas pero al parecer ya no lo hace-->
					<p>
						<h3>Consultar zonas de Riesgo</h3>
						<img id = "imagen" src="CPresentacion/imagenes/pic02.jpg" alt="">
						<p class="color4">
							<br>Clasificaci&oacute;n de las mayores zonas de riesgo del cant&oacute;n.</br>
							<br style="margin-top: 22px;"><input type="button" id ="consultarZonas" value = "Consultar" class="btn btn-info btn-sm"></br>
						</p>
					</p>
				</div>

				
			
				<div id="portfolio-wrapper">
					<div id="portfolio" class="container">
						<div class="title">
							<h2>Informaci&oacute;n Espec&iacute;fica</h2>
							<span class="byline">Agenda, Directorio</span> 
						</div>
						<div id = "columns">
							
							<div class="column1">
								<div class="box">
									<span id = "iconos"><i class="far fa-calendar-alt"></i></span>
									<h3>Agenda</h3>
									<br>Conozca de nuestras actividades para nuestra comunidad Sarapiqueña</br>
									<a href="#" class="button">Ver Agenda</a> 
								</div>
							</div>
							<div class="column2">
								<div class="box">
									<span id = "iconos"><i class="fas fa-clinic-medical"></i></span>
									<h3>Albergue</h3>
									<br>Sarapiqu&iacute; ante emergencias cuenta con albergues y personal adecuado</br>
									<a href="#" class="button">Ver Directorio</a> 
								</div>
							</div>
							<div class="column3">
								<div class="box">
									<span id = "iconos"><i class="far fa-address-book"></i></span>
									<h3>Directorio</h3>
									<br>Conozca quienes conforman nuestra Comisi&oacute;n de Emergencias</br>
									<a href="CPresentacion/ventanaDirectorio.php" class="button">Ver Directorio</a> 
								</div>
							</div>

						</div>					
					</div>
				</div>
			</div>	
			
		</div>
	</div>

	<!--==== MODAL DE INICIAR SECION ====--> 

	<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          	<div class="modal-content">
            	<div class="modal-header text-center" style="background: #333333 url(images/overlay.png);">
              		<h4 class="modal-title w-100 font-weight-bold" style="color: #FFF">Iniciar sesión</h4>
              		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                		<span aria-hidden="true">&times;</span>
              		</button>
            	</div>
            	<div class="modal-body mx-3">
              		<div class="md-form mb-5">
                		<i class="fas fa-user"></i>
                		<input type="text" id="usuario" class="form-control validate" placeholder="Tu usuario" maxlength="11" onkeypress="return aceptarSoloNumeros(event);">
              		</div>
              		<div class="md-form mb-4">
                		<i class="fas fa-lock"></i>
                		<input type="password" id="password" class="form-control validate" maxlength="30" placeholder="Tu contraseña">
             		</div>
            	</div>
            	<div class="modal-footer d-flex justify-content-center">
              		<button id="acceder" class="btn btn-outline-info" onclick="acceder();">Acceder</button>
              		<button type="button" class="btn btn-outline-danger waves-effect ml-auto" data-dismiss="modal" >Close</button>
            	</div>
          	</div>
        </div>
    </div>

    <!--====== SE LLAMA EL FOOTER ======-->

	<?php include "includes/generic-footer.html";?>

</body>
</html>
