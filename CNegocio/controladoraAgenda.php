<?php
	include "../CDatos/agendaConsultas.php";
	include "../CNegocio/validarEspacios.php";

	if(isset($_POST['buscarRegistros'])){
		$consultas = new agendaConsultas();
		echo $consultas->buscarRegistros();
	}
	else if(isset($_POST['seleccionar'])){
		$consultas = new agendaConsultas();
		echo $consultas->seleccionarActividad($_POST['seleccionar']);
	}
	else if(isset($_POST['actualizarAgenda'])){
		$id = $_POST['id'];
		$titulo = $_POST['titulo'];
		$tipoActividad = $_POST['tipoActividad'];
		$privacidadActividad = $_POST['privacidadActividad'];
		$fechaActividad = $_POST['fechaActividad'];
		$horaActividad = $_POST['horaActividad'];
		$descripcionActividad = $_POST['descripcionActividad'];
		$encargadoActividad = $_POST['encargadoActividad'];
		$informacionActividad = $_POST['informacionActividad'];
		$agenda = new agenda($id,$titulo,$tipoActividad,$privacidadActividad,$fechaActividad,$horaActividad,$descripcionActividad,$encargadoActividad,$informacionActividad);
		$consultas = new agendaConsultas();

		echo $consultas->actualizarAgenda($agenda);
	}
	else if(isset($_POST['eliminar'])){
		$consultas = new agendaConsultas();
		echo $consultas->eliminarAgenda($_POST['eliminar']);
	}

	else if(isset($_POST['registrarAgenda'])){
		$titulo = $_POST['titulo'];
		$tipoActividad = $_POST['tipoActividad'];
		$privacidadActividad = $_POST['privacidadActividad'];
		$fechaActividad = $_POST['fechaActividad'];
		$horaActividad = $_POST['horaActividad'];
		$descripcionActividad = $_POST['descripcionActividad'];
		$encargadoActividad = $_POST['encargadoActividad'];
		$informacionActividad = $_POST['informacionActividad'];
		$agenda = new agenda(0,$titulo,$tipoActividad,$privacidadActividad,$fechaActividad,$horaActividad,$descripcionActividad,$encargadoActividad,$informacionActividad);
		$consultas = new agendaConsultas();

		echo $consultas->registrarAgenda($agenda);
	}
?>