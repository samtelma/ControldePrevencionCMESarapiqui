<?php
	include '../CDatos/consultasAlbergues.php';  // Donde estan las consultas.
	require '../CDominio/albergue.php';  // Objeto albergue.

    if (isset($_POST['cargarDatosEncargado'])) {

		$consultas = new consultasAlbergues(); // Se instancia la clase consultaAlbergue.
		$resultado = $consultas->CargarDatosEncargados(); // Se obtien los datos de el encargado.

		echo $resultado;

	} else if (isset($_POST['registrar'])) {

		$consulta = new consultasAlbergues(); // Se instancia la clase consultaalbergues.
		
		// Se obtien los datos.
		$nombre    = $_POST['nombre'];
		$capacidad = $_POST['capacidad'];
		$encargado = $_POST['encargado'];
		$localidad = $_POST['localidad'];
		$telefono  = $_POST['telefono'];
		$luz       = $_POST['luz'];
		$agua      = $_POST['agua'];
		$cuartos   = $_POST['cuartos'];
		$cocina    = $_POST['cocina'];
		$banos     = $_POST['banos'];
		
		// Creamos el objeto.
		$albergue = new albergue(0, $nombre, 
									$capacidad, 
									$encargado, 
									$localidad, 
									$telefono,
									$luz,
									$agua,
									$cuartos,
									$cocina,
									$banos);

		// Lo enviamos a consulta.
		$resultado = $consulta->Registrar($albergue);

		echo $resultado;

	} else if (isset($_POST['consultar'])) {

		$pagina = $_POST['pagina'];
		$consultas = new consultasAlbergues();
		$resultado = $consultas->Consultar($pagina); 
		$datos = "";

		if ($resultado != 0) {


			for ($i = 0; $i < count($resultado); $i++) {
				
				// Verifico el agua.
				$agua = "no hay";
				if ($resultado[$i]->getAgua() == "1") {
					$agua = "Potable";
				}
				else if ($resultado[$i]->getAgua() == "0"){
					$agua = "no potable";
				}

				// Verifico la luz.
				$luz = "Si";
				if ($resultado[$i]->getLuz() == "0") {
					$luz = "No";
				}

				// Creo la tabla.
				$datos.="<tr class = 'cla'>" .
		                "<td>" . ($i+1) . "</td>" .
		                "<td>" . $resultado[$i]->getNombre() . "</td>" .
		                "<td>" . $resultado[$i]->getCapacidad() . "</td>" .
		                "<td>" . $resultado[$i]->getEncargado() . "</td>" .
		                "<td>" . $resultado[$i]->getLocalidad() . "</td>" .
		                "<td>" . $resultado[$i]->getTelefono() . "</td>" .
		                "<td>" . $luz . "</td>" .
		                "<td>" . $agua . "</td>" .
		                "<td>" . $resultado[$i]->getCuartos() . "</td>" .
		                "<td>" . $resultado[$i]->getCocina() . "</td>" .
		                "<td>" . $resultado[$i]->getBanos() . "</td>" .
		                "<td>" .
		                "<button class='btnActualizar' onclick = 'Seleccionar(" . $resultado[$i]->getId() . ")'><i class='far fa-edit'></i></button>" .
		                "<button class='btnEliminar' onclick = 'ConfirmarEliminacion(" . $resultado[$i]->getId() . ")'><i class='fas fa-trash-alt'></i></button>" .
		                "</td>" .
		                "</tr>";
			}
		}

		echo $datos; 

	} else if (isset($_POST['consultarAlberguesVisita'])) {

		$pagina = $_POST['pagina'];
		$consultas = new consultasAlbergues();
		$resultado = $consultas->Consultar($pagina); 
		$datos = "";

		if ($resultado != 0) {


			for ($i = 0; $i < count($resultado); $i++) {
				
				// Verifico el agua.
				$agua = "no hay";
				if ($resultado[$i]->getAgua() == "1") {
					$agua = "Potable";
				}
				else if ($resultado[$i]->getAgua() == "0"){
					$agua = "no potable";
				}

				// Verifico la luz.
				$luz = "Si";
				if ($resultado[$i]->getLuz() == "0") {
					$luz = "No";
				}
				$encargado = $consultas->obtenerNombreResponsableporID($resultado[$i]->getEncargado());

				// Creo la tabla.
				$datos.="<tr class = 'cla'>" .
		                "<td>" . ($i+1) . "</td>" .
		                "<td>" . $resultado[$i]->getNombre() . "</td>" .
		                "<td>" . $resultado[$i]->getCapacidad() . "</td>" .
		                "<td>" . $encargado. "</td>" .
		                "<td>" . $resultado[$i]->getLocalidad() . "</td>" .
		                "<td>" . $resultado[$i]->getTelefono() . "</td>" .
		                "<td>" . $luz . "</td>" .
		                "<td>" . $agua . "</td>" .
		                "<td>" . $resultado[$i]->getCuartos() . "</td>" .
		                "<td>" . $resultado[$i]->getCocina() . "</td>" .
		                "<td>" . $resultado[$i]->getBanos() . "</td>" .
		                "</tr>";
			}
		}

		echo $datos; 

	} else if (isset($_POST['eliminar'])) {
		
		$consultas = new consultasAlbergues(); // Instanciamos la clase consulta.
		$resultado =  $consultas->Eliminar($_POST['eliminar']); // La enviamos a la clase consulta.

		echo $resultado;

	} else if (isset($_POST['seleccionar'])) {

		$consultas = new consultasAlbergues(); // Instanciamos la clase consulta.
		$resultado = $consultas->Seleccionar($_POST['seleccionar']); // Lo obtenemos de la clase consulta.

		echo $resultado;

	} else if (isset($_POST['actualizar'])) {

		$consulta = new consultasAlbergues();

		// Se obtien los datos.
		$id        = $_POST['id'];
		$nombre    = $_POST['nombre'];
		$capacidad = $_POST['capacidad'];
		$encargado = $_POST['encargado'];
		$localidad = $_POST['localidad'];
		$telefono  = $_POST['telefono'];
		$luz       = $_POST['luz'];
		$agua      = $_POST['agua'];
		$cuartos   = $_POST['cuartos'];
		$cocina    = $_POST['cocina'];
		$banos     = $_POST['banos'];

		// Creamos el objeto.
		$albergue =new albergue($id,
								$nombre, 
								$capacidad, 
								$encargado, 
								$localidad, 
								$telefono,
								$luz,
								$agua,
								$cuartos,
								$cocina,
								$banos);

		// Lo enviamos a consultaAlbergue.
		$resultado = $consulta->Actualizar($albergue);

		echo $resultado;

	} else if (isset($_POST['buscarAlbergueEspecifico'])) {

		$consultas = new consultasAlbergues();
		$variable = $_POST['buscarAlbergueEspecifico'];
		$resultado = $consultas->buscarAlbergueEspecifico($variable);
		$datos = ""; 

		if ($resultado != 0) {

			$datos = "";

			for ($i = 0; $i < count($resultado); $i++) {
					
				$datos.="<tr class = 'cla'>" .
		                "<td>" . ($i+1) . "</td>" .
		                "<td>" . $resultado[$i]->getNombre() . "</td>" .
		                "<td>" . $resultado[$i]->getCapacidad() . "</td>" .
		                "<td>" . $resultado[$i]->getEncargado() . "</td>" .
		                "<td>" . $resultado[$i]->getLocalidad() . "</td>" .
		                "<td>" . $resultado[$i]->getServicios() . "</td>" .
		                "<td>" . $resultado[$i]->getTelefono() . "</td>" .
		                "<td>" .
		                "<button class='btnActualizar' onclick = 'Seleccionar(" . $resultado[$i]->getId() . ")'><i class='far fa-edit'></i></button>" .
		                "<button class='btnEliminar' onclick = 'ConfirmarEliminacion(" . $resultado[$i]->getId() . ")'><i class='fas fa-trash-alt'></i></button>" .
		                "</td>" .
		                "</tr>";
			}
		}

		echo $datos;

	} else if (isset($_POST['contarFilas'])) {

		$consulta = new consultasAlbergues();
		$resultado = $consulta->contarFilas();

		echo $resultado;

	} else if (isset($_POST['telefono'])) {

		$nombre = $_POST['telefono'];
		$consulta = new consultasAlbergues();
		$resultado = $consulta->TelefonoResponsable($nombre);

		echo $resultado;
	}
?>