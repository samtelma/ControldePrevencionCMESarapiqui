<!DOCTYPE html>
<HTML>
	<HEAD>
		<link rel="shortcut icon" href="../CPresentacion/imagenes/crc.png" />
		<TITLE>Directorio</TITLE>
		<SCRIPT SRC="../CPresentacion/ajax/jquery-3.3.1.min.js"></SCRIPT>
		<SCRIPT SRC="../CPresentacion/ajax/ajaxDirectorio.js" TYPE="text/javascript"></SCRIPT>
   		<script src="../CPresentacion/ajax/jquery.maskedinput-master/src/jquery.maskedinput.js" type="text/javascript"></script>

		<META http-equiv="Content-Type" CONTENT="text/html; charset=utf-8"/>

		<link href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" rel="stylesheet" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
		
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"><!--agregar el icono de barras del mení responsive-->
		<link rel="stylesheet" href="../CPresentacion/css/estilos.css" href="">

		<LINK REL="stylesheet" type="text/css" href="../CPresentacion/css/directorioCss.css"/>
		<LINK REL="stylesheet" type="text/css" href="../CPresentacion/css/fonts.css"/>

		<META NAME="viewport" CONTENT="width=device-width, initial-scale=1, maximum-scale=1"/>

	</HEAD>
	<BODY>
		
		<?php include("includes/generic-header.html"); ?>
			

		<div id="page" class="container">

			<!-- Aqui adentro deberan ir todos los elementos-->
			<DIV id = 'cabeceraPrincipal'>

				<P ID = 'tituloCabecera'>Secci&oacute;n de Directorio</P>	
				<P ID = 'encargado' CLASS = 'encargad'>Encargado</P>	

				<TABLE ID = 'opcionesMenuDirectorio'>

				</TABLE>
			</DIV>


			<INPUT TYPE = 'TEXT' ID = 'buscar' MAXLENGTH = '40' SIZE = '40' TITLE = "Nombre, Apellido, Instituci&oacute;n es lo que busca" ONKEYUP = "campoVacio()" ONKEYDOWN = "detectarTeclaEnter_enBusqueda(event);" PLACEHOLDER = "Buscar integrante"></INPUT>

			<DIV style = "position: relative;overflow: auto;">

				<TABLE ID = "tabla">

					<THEAD>
						<TR>
							<TH>Nombre</TH>
							<TH>Primer Apellido:</TH>
							<TH>Segundo Apellido:</TH>
							<!--<TH>Telefono</TH>-->
							<TH>Instituci&oacute;n Representada:</TH>	
							<!--<TH >Opciones</TH>-->
						</TR>	
					</THEAD>

					<TR>

						<TBODY ID = 'tablaDirectorio'></TBODY>


					</TABLE>	

					<NAV>
						<UL ID= "paginacion" CLASS= "paginacion" style='list-style-type: none;'>
							<LI CLASS = "page-item" ID = 'paginaAnterior'>
								<A CLASS = "page-link" ID = 'primeraPagina' ONCLICK = "cargarPreviaInformacion()">
									Anterior
								</A>
							</LI>

							<LI ID = 'pagination' CLASS = "page-item">
							</LI>
							<LI CLASS = "page-item" ID = 'siguientePagina'>
								<A CLASS = "page-link " ONCLICK = "cargarSiguienteInformacion()">
									Siguiente
								</A>
							</LI>
						</UL>

					</NAV>	
			</DIV>
		</DIV>
	<p>
		<?php include("includes/generic-footer.html"); ?>		
	</p>
	</BODY>
</HTML>