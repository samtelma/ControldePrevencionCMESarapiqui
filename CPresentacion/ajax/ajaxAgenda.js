window.onload = function(event) {
	consultarAgenda();
	limpiarCampos();
}

function consultarAgenda(){

	var xhr = new XMLHttpRequest();
	xhr.open("POST", "../CNegocio/controladoraAgenda.php");
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("buscarRegistros=true");
  	xhr.onreadystatechange = function () {

	    if (xhr.readyState == 4 && xhr.status == 200) {
	    	var resultado = xhr.responseText;
	    	var datos = resultado.split("*");
	        $("tr").remove(".cla"); 

	    	for(var i = 0; i < datos.length - 1; i++){
				var cadena = datos[i].split(",");		
		    	
		    	$("#detalle").append("" +
		    			"<tr class = 'cla'>" +
		    				"<td>" + cadena[1] + "</td>" +
		    				"<td>" + cadena[2] + "</td>" +
		    				"<td>" + cadena[6] + "</td>" +
		    				"<td>" + cadena[4] + "</td>" +
		    				"<td> <input type = 'button' id = 'seleccionar' name = 'seleccionar' value = 'Ver' onclick = seleccionar('"+cadena[0]+"') /></td>" +
		    				"<td> <input type = 'button' id = 'eliminar' name = 'eliminar' value = 'Eliminar' onclick = seleccionar('"+cadena[0]+ '*'+"') /></td>" +
		    			"</tr>"
		    		);
	    	}
		}
	}
}

var contador = 0;
var bloquearQuery = true;
function validarInformacion(e){
	var numeros = "0123456789";
    tecla = (document.all) ? e.keyCode : e.which;
    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
        return true;
    }
        alert(e);
    // Patron de entrada, en este caso solo acepta numeros
    patron =/[0-9]/;
    tecla_final = String.fromCharCode(tecla);
	if(numeros.includes(tecla_final)){
		contador++;
		bloquearQuery = true;
		sumar_o_restarParaTelefonoInformacion(false);
	}
	if(contador >= 3){
		if(contador > 4){
			document.getElementById('informacionActividad').value += "-";
			contador = 0;
		}
		sumar_o_restarParaTelefonoInformacion(false);

	    return patron.test(tecla_final);
	}
    
}

function limpiarCampos(){
	document.getElementById('id').value = "";
	document.getElementById('titulo').value = "";
	document.getElementById('tipoActividad').selectedIndex = 0;
	document.getElementById('privacidadActividad').selectedIndex = 0;
	document.getElementById('fechaActividad').value = "";
	document.getElementById('horaActividad').value = "";
	document.getElementById('descripcionActividad').value = "";
	document.getElementById('encargadoActividad').value = "";
	document.getElementById('informacionActividad').value = "";
}

function sumar_o_restarParaTelefonoInformacion(valor){
	$("html").keyup(function(event){
		if(event.keyCode == 8 || event.keyCode == 46){
			if(valor)
				contador-=1;
			bloquearQuery = false;
		}
	});
}

function aceptarCambios(){
	if(document.getElementById('aceptar').value == "Agendar")
		registrarAgenda();
	if(document.getElementById('aceptar').value == "Actualizar")
		actualizarAgenda();
	if(document.getElementById('aceptar').value == "Eliminar")
		eliminarAgenda();
}
function registrarAgenda(){
	var titulo = document.getElementById('titulo').value;
	var tipoActividad = document.getElementById('tipoActividad').value;
	var privacidadActividad = document.getElementById('privacidadActividad').value;
	var fechaActividad = document.getElementById('fechaActividad').value;
	var horaActividad = document.getElementById('horaActividad').value;
	var descripcionActividad = document.getElementById('descripcionActividad').value;
	var encargadoActividad = document.getElementById('encargadoActividad').value;
	var informacionActividad = document.getElementById('informacionActividad').value;
	var enviar = 	"registrarAgenda=true" +
					"&titulo=" + titulo +
					"&tipoActividad=" + tipoActividad +
					"&privacidadActividad=" + privacidadActividad +
					"&fechaActividad=" + fechaActividad +
					"&horaActividad=" + horaActividad +
					"&descripcionActividad=" + descripcionActividad +
					"&encargadoActividad=" + encargadoActividad + 
					"&informacionActividad=" + informacionActividad;
	var xhr = new XMLHttpRequest();
	xhr.open("POST","../CNegocio/controladoraAgenda.php");
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send(enviar);

	xhr.onreadystatechange = function(){
		if(xhr.readyState == 4 && xhr.status == 200){
			alert("Registro exitoso");
			limpiarCampos();
			consultarAgenda();
		}
	}	
}

function actualizarAgenda(){
	var id = document.getElementById('id').value;
	var titulo = document.getElementById('titulo').value;
	var tipoActividad = document.getElementById('tipoActividad').value;
	var privacidadActividad = document.getElementById('privacidadActividad').value;
	var fechaActividad = document.getElementById('fechaActividad').value;
	var horaActividad = document.getElementById('horaActividad').value;
	var descripcionActividad = document.getElementById('descripcionActividad').value;
	var encargadoActividad = document.getElementById('encargadoActividad').value;
	var informacionActividad = document.getElementById('informacionActividad').value;
	var enviar = 	"actualizarAgenda=true" +
					"&id=" + id +
					"&titulo=" + titulo +
					"&tipoActividad=" + tipoActividad +
					"&privacidadActividad=" + privacidadActividad +
					"&fechaActividad=" + fechaActividad +
					"&horaActividad=" + horaActividad +
					"&descripcionActividad=" + descripcionActividad +
					"&encargadoActividad=" + encargadoActividad + 
					"&informacionActividad=" + informacionActividad;
	var xhr = new XMLHttpRequest();
	xhr.open("POST","../CNegocio/controladoraAgenda.php");
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send(enviar);

	xhr.onreadystatechange = function(){
		if(xhr.readyState == 4 && xhr.status == 200){
			var resultado = xhr.responseText;
			if(resultado > 0){
				alert("Actualizacion exitoso");
				document.getElementById('aceptar').value = "Agendar";
				limpiarCampos();
				consultarAgenda();				
			}
			else
				alert("Error de actualizacion");

		}
	}		
}

function seleccionar(id){

	if(id.includes("*"))
		document.getElementById('aceptar').value = "Eliminar";
	else
		document.getElementById('aceptar').value = "Actualizar";

	var xhr = new XMLHttpRequest();
	xhr.open("POST","../CNegocio/controladoraAgenda.php");
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("seleccionar="+id);
	xhr.onreadystatechange = function(){
		if(xhr.readyState == 4 && xhr.status == 200){
			var resultado = xhr.responseText;
			resultado = resultado.split(",");
			document.getElementById('id').value = resultado[0];
			document.getElementById('titulo').value = resultado[1];
			document.getElementById('fechaActividad').value = resultado[4]; 
			document.getElementById('horaActividad').value = resultado[5];
			document.getElementById('descripcionActividad').value = resultado[6];
			document.getElementById('encargadoActividad').value = resultado[7];
			document.getElementById('informacionActividad').value = resultado[8];

			var opciones = document.getElementById('tipoActividad');
			for(var i = 0; i < opciones.length; i++){
				if(resultado[2].includes(opciones[i].value))
					opciones.selectedIndex = i;
			}
			if(resultado[3] == "Privada")
				document.getElementById('privacidadActividad').selectedIndex = 1;
			else
				document.getElementById('privacidadActividad').selectedIndex = 2;
		}
	}
}

function eliminarAgenda(){

	xhr = new XMLHttpRequest();
	xhr.open("POST","../CNegocio/controladoraAgenda.php");
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("eliminar="+document.getElementById('id').value);

	xhr.onreadystatechange = function(){
		if(xhr.readyState == 4 && xhr.status == 200){
			var resultado = xhr.responseText;

			if(resultado > 0){
				alert("Registro eliminado");
				document.getElementById('aceptar').value = "Agendar";
				consultarAgenda();
				limpiarCampos();
			}
			else
				alert("Problema con eliminar");
		}
	}
}