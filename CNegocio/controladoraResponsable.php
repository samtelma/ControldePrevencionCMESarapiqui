<?php
	include '../CDatos/consultasResponsable.php';  // Donde estan las consultas.
	require '../CDominio/responsable.php';

	if (isset($_POST['registrar'])) {
		
		// Obtenemos los datos.
		$cedula    = $_POST['cedula'];
		$consulta = new consultasResponsable();

		if ($consulta->VerificarDirectorio($cedula)) {

			$nombre    = $_POST['nombre'];
			$apellido1 = $_POST['apellido1'];
			$apellido2 = $_POST['apellido2'];
			$telefono  =$_POST['telefono'];

			// Se crea el objeto Responsable.
			$responsable = new responsable(0, 
											$cedula, 
											$nombre, 
											$apellido1, 
											$apellido2,
											$telefono);

			$resultado = $consulta->RegistrarResponsable($responsable);

			echo $resultado;
		} else {

			echo "3";
		}
	}

	if (isset($_POST['consultar'])) {

		$pagina    = $_POST['pagina'];
		$consultas = new consultasResponsable();
		$resultado = $consultas->ConsultarResponsables($pagina); 

		// Comprobamos los resultados y creamos la tabla.
		if ($resultado != 0) {

			$datos = "";

			for ($i = 0; $i < count($resultado); $i++) {
				
				$datos.="<tr class = 'cla'>" .
		                "<td>" . ($i+1) . "</td>" .
		                "<td>" . $resultado[$i]->getCedula() . "</td>" .
		                "<td>" . $resultado[$i]->getNombre() . "</td>" .
		                "<td>" . $resultado[$i]->getApellido1() . "</td>" .
		                "<td>" . $resultado[$i]->getApellido2() . "</td>" .
		                "<td>" . $resultado[$i]->getTelefono() . "</td>" .
		                "<td>" .
		                "<button class='btnActualizar' onclick = 'Seleccionar(" . $resultado[$i]->getId() . ")'><i class='far fa-edit'></i></button>" .
		                "<button class='btnEliminar' onclick = 'ConfirmarEliminacion(" . $resultado[$i]->getId() . ")'><i class='fas fa-trash-alt'></i></button>" .
		                "</td>" .
		                "</tr>";
			}

			echo $datos;
		}

		echo 0;
	}

	if (isset($_POST['eliminar'])) {

		$consultas = new consultasResponsable();

	//	if ($consultas->ConsultarSiTieneLlaveFornea($_POST['eliminar']) != 1) {

			echo $consultas->EliminarResponsable($_POST['eliminar']);
	//	} 

		//echo 2;
	}

	if (isset($_POST['seleccionar'])) {

		$consultas = new consultasResponsable();

		echo $consultas->Seleccionar($_POST['seleccionar']);
	}

	if (isset($_POST['actualizar'])) {

		// Se crea el objeto responsable.
		$responsable = new responsable($_POST['id'], $_POST['cedula'], $_POST['nombre'], $_POST['apellido1'], $_POST['apellido2'], $_POST['telefono']);

		// Si too esta correcto, se atualiza.
		$consulta = new consultasResponsable();

		echo $consulta->ActualizarResponsable($responsable);
	}

	else if(isset($_POST['contarFilas'])){
		$consulta = new consultasResponsable();
		echo $consulta->contarFilas();
	}

	else if (isset($_POST['buscarResponsableEspecifico'])) {

		$variable = $_POST['buscarResponsableEspecifico'];
		$consultas = new consultasResponsable();
		$temp = "";
		$puerta = true;
		//DEFINE LA LONGITUD DEL DATO POR BUSCAR
		for($i = 0; $i < strlen($variable); $i++){

			//SI EL DATO DETECTA UN ESPACIO QUITA EL ESPACIO Y AGREGA UNA COMA 
			if($variable[$i] == " " && $puerta){
				$temp .= ",";
			}
			//SI EL DATO NO CONTIENE ESPACIOS CONSTRUYE LA PALABRA LETRA POR LETRA
			if($variable[$i] != " " && $puerta){
				$temp .= $variable[$i];
			}
			//SI EL DATO TIENE UN + QUIERE SIGNIFICAR QUE BUSCA UN COMITÉ
			if(strpos($variable, "*") == true && $puerta){
				$temp = trim($variable,'*');
				$puerta = false;
			}
		}

		$resultado = $consultas->buscarResponsableEspecifico($temp); 

		if ($resultado != 0) {

			$datos = "";
			for ($i = 0; $i < count($resultado); $i++) {
				
				$datos.="<tr class = 'cla'>" .
		                "<td>" . ($i+1) . "</td>" .
		                "<td>" . $resultado[$i]->getCedula() . "</td>" .
		                "<td>" . $resultado[$i]->getNombre() . "</td>" .
		                "<td>" . $resultado[$i]->getApellido1() . "</td>" .
		                "<td>" . $resultado[$i]->getApellido2() . "</td>" .
		                "<td>" . $resultado[$i]->getTelefono() . "</td>" .
		                "<td>" .
		                "<button class='btnActualizar' onclick = 'Seleccionar(" . $resultado[$i]->getId() . ")'><i class='far fa-edit'></i></button>" .
		                "<button class='btnEliminar' onclick = 'ConfirmarEliminacion(" . $resultado[$i]->getId() . ")'><i class='fas fa-trash-alt'></i></button>" .
		                "</td>" .
		                "</tr>";
			}

			echo $datos;
		}

		echo $resultado;
	}

	else if (isset($_POST['directorio'])) {

		$cedula = $_POST['directorio'];
		$consultas = new consultasResponsable();
		$resultado = $consultas->VerificarDirectorio($cedula);

		echo $resultado;
	}
?>