<?php
	include '../CDatos/consultasVoluntario.php';  // Donde estan las consultas.
	require '../CDominio/voluntario.php';  // Objeto bodega.

	if (isset($_POST['registrar'])) { // Registra un voluntario.

		// Se obtien los valores.
		$nombre   = $_POST['nombre'];
		$telefono = $_POST['telefono'];
		$detalle  = $_POST['detalle'];

		$voluntario = new voluntario(0, $nombre, $telefono, $detalle); // Se crea el objeto voluntario.
		$consulta = new consultasVoluntario(); // Se instancia la clase consulta.
		$resultado = $consulta->Registrar($voluntario); // Se envia a consulta.

		echo $resultado; // Se retorna el resultado.

	} else if (isset($_POST['consultar'])) { // Consulta toda la tabla de voluntarios.

		$pagina    = $_POST['pagina'];
		$consultas = new consultasVoluntario(); // Instanciamos la clase consultas.
		$resultado = $consultas->Consultar($pagina);  // Se envia a consulta.

		if ($resultado != 0) { // Se verifica el resultado.

			$datos = ""; // Variable de retorno.

			for ($i = 0; $i < count($resultado); $i++) { // Se recorre la estructura devuelta por la DB.
				
				$detalle = $resultado[$i]->getDetalle(); // Detalle.
				$parrafo = ""; // Es lo que se va mostrar en detalle.

				// Se evalua el detalle, si ponemos un boton mass... o no.
				if (strlen($detalle) <= 51) { // Si el detalle tien menos de 50 caracteres.
					
					$parrafo = "<p>" . $detalle . "</p>"; // Simplemente el detalle se queda como esta.
				}
				else { // Si tiene mas de 50 caracteres.
					
					// Si en la posicion 49 del string es un espacio.
					if (substr($detalle, 49,1) == " ") { // Hago la divicion  normal.

						$parrafo = "<p>" . substr($detalle, 0, 50) . "</p><p id='".($i+1)."' class='p' style = 'display:none' >" .
						substr($detalle, 50, -1) . "</p><input type='button' id='btn".($i+1)."' class='btn_mas' value='Mas...' onclick='Mas(".($i+1).");' />";
					}
					else { // Si no, significa que hay una palabra en medio de donde se va dividir el detalle.

						// Se recorre desde la posicion 49 del string hasta encontrar un espacio.
						for ($j = 50; $j < strlen($detalle); $j++) { // Recorre el string(detalle).
							
							if (substr($detalle, $j, 1) == " ") { // Si el caracter es un espacio.
								
								$parrafo = "<p>" . substr($detalle, 0, $j) . "</p><p id='".($i+1)."' class='p' style = 'display:none' >" .
								substr($detalle, $j, -1) . "</p><input type='button' id='btn".($i+1)."' class='btn_mas' value='Mas...' onclick='Mas(".($i+1).");' />";
								break; // Sale del for.
							} // if.
						} // for.
					} // else.
				} // else.

				// Se crea la estructura HTML.
				$datos.="<tr class = 'cla'>" .
		                "<td>" . ($i+1) . "</td>" .
		                "<td>" . $resultado[$i]->getNombre() . "</td>" .
		                "<td>" . $resultado[$i]->getTelefono() . "</td>" .
		                "<td><p>" . $parrafo . "</td>" .
		                "<td>" .
		                "<button class='btnActualizar' ID = 'actualizar' onclick = 'Seleccionar(" . $resultado[$i]->getId() . ")'><i class='far fa-edit'></i></button>" .
		                "<button class='btnEliminar' ID = 'eliminar' onclick = 'ConfirmarEliminacion(" . $resultado[$i]->getId() . ")'><i class='fas fa-trash-alt'></i></button>" .
		                "</td>" .
		                "</tr>";
			} // for.

			echo $datos; // Se retorna la structura HTML.
		}
		else { // Si no hay datos en la DB.

			echo $resultado; // Se retorna lo que devolvio la BD.
		} // else

	} else if (isset($_POST['eliminar'])) { // Elimina un voluntario.

		// Se obtiene el id.
		$id = $_POST['eliminar'];

		$consultas = new consultasVoluntario(); // Se instancia laclase consulta.
		$resultado = $consultas->Eliminar($id); // Se envia a consulta.

        echo $resultado; // Se retorna el resultado.

	} else if (isset($_POST['seleccionar'])) { // Selecciona un voluntario(edit).

		// Se obtien el id del voluntario.
		$id = $_POST['seleccionar'];

		$consultas = new consultasVoluntario(); // Se instancia la clase consulta.
		$resultado = $consultas->Seleccionar($id); // Se envia a consulta.

		echo $resultado->toString(); // Se retorna el objeto.

	} else if (isset($_POST['actualizar'])) { // Se actualiza un voluntario.

		// Se obtien los valores.
		$id 	  = $_POST['id'];
		$nombre   = $_POST['nombre'];
		$telefono = $_POST['telefono'];
		$detalle  = $_POST['detalle'];

		$voluntario = new voluntario($id, $nombre, $telefono, $detalle); // Se crea el objeto bodega.
		$consulta = new consultasVoluntario(); // Se instancia la clase consulta.
		$resultado = $consulta->Actualizar($voluntario); // Se envia a consulta.

		echo $resultado; // Se retorna el resultado.

	} else if (isset($_POST['buscarIntegranteEspecifico'])) { // Se busca un integrante espesifico.

		// Se obtiene el valor.
		$variable = $_POST['buscarIntegranteEspecifico'];
		$consultas = new consultasVoluntario(); // Se instancia ña cñase consulta.
		$resultado = $consultas->buscarIntegranteEspecifico($variable); // Se envia a consulta.

		if ($resultado != 0) { // Se verifica el resultado.

			$datos = ""; // Variable de retorno.

			for ($i = 0; $i < count($resultado); $i++) { // Se recorre la estructura que devolvio la B.
				
				$detalle = $resultado[$i]->getDetalle(); // Detalle.					
				$parrafo = ""; // El parrafo.

				if (strlen($detalle) < 50) { // Si el detalle tiene mas de 50 caracteres.
					
					$parrafo = "<p>" . $resultado[$i]->getDetalle() . "</p>";
				}
				else {

					$parrafo = "<p>" . substr($resultado[$i]->getDetalle(), 0, 50) . "</p><p id='".($i+1)."' class='p'>" . 
					substr($resultado[$i]->getDetalle(), 50, -1) . "</p><input type='button' id='btn".($i+1)."' class='btn_mas' value='Mas...' onclick='Mas(".($i+1).");' />";
				} // else.

				$datos.="<tr class = 'cla'>" .
		                "<td>" . ($i+1) . "</td>" .
		                "<td>" . $resultado[$i]->getNombre() . "</td>" .
		                "<td>" . $resultado[$i]->getTelefono() . "</td>" .
		                "<td><p>" . $parrafo . "</td>" .
		                "<td>" .
		                "<button class='btnActualizar' id = 'actualizar' onclick = 'Seleccionar(" . $resultado[$i]->getId() . ")'><i class='far fa-edit'></i></button>" .
		                "<button class='btnEliminar' id = 'eliminar' onclick = 'ConfirmarEliminacion(" . $resultado[$i]->getId() . ")'><i class='fas fa-trash-alt'></i></button>" .
		                "</td>" .
		                "</tr>";
			} // for.

			echo $datos;
		} // if.

		echo $resultado;

	} else if (isset($_POST['contarFilas'])) { // Contador de filas.

		$consulta = new consultasVoluntario(); // Se instancia la clase consulta.
		$resultado = $consulta->contarFilas(); // Se envia a la controladora.

		echo $resultado; // Se retorna el resultado obtenido.

	} else if (isset($_POST['verDetalle'])) {

		// Se obtien el id.
		$id = $_POST['id'];
		$consultas = new consultasVoluntario(); // Se instancia  la clase consulta.
		$resultado = $consultas->Seleccionar($id); // Se envia a consulta.

		echo $resultado->toString(); // Se retorna el objeto.

	}
?>