<?php

	include '../CDatos/consultasInsumo.php';  // Donde estan las consultas.
	require '../CDominio/insumo.php';

    if (isset($_POST['cargar'])) {

		$consultas = new consultasInsumo();

		echo $consultas->CargarDatosBodega(); 

	} else if (isset($_POST['registrar'])) {

		$consulta = new consultasInsumo();

		echo $consulta->Registrar(new insumo(0, $_POST['articulo'], $_POST['institucion'], $_POST['cantidad'], $_POST['observacion'], $consulta->OntenerIdBodegaPorNombre($_POST['bodega'])));

	} else if (isset($_POST['consultar'])) {
		$pagina = $_POST['pagina'];
		$consultas = new consultasInsumo(); // Instanciamos la clase consultas.
		$resultado = $consultas->ConsultarInsumos($pagina);

		if ($resultado != 0) {

			$datos = "";

			for ($i = 0; $i < count($resultado); $i++) {
					
				$parrafo = "";

				if (strlen($resultado[$i]->getObservacion()) < 50) {
					
					$parrafo = "<p>" . $resultado[$i]->getObservacion() . "</p>";
				} else {

					$parrafo = "<p>" . substr($resultado[$i]->getObservacion(), 0, 50) . "</p><p id='".($i+1)."' class='p'>" . 
					substr($resultado[$i]->getObservacion(), 50, -1) . "</p><input type='button' id='btn".($i+1)."' value='Mas...' onclick='Mas(".($i+1).");' />";
				}

				$datos.="<tr class = 'cla'>" .
		                "<td>" . ($i+1) . "</td>" .
		                "<td>" . $resultado[$i]->getArticulo() . "</td>" .
		                "<td>" . $resultado[$i]->getInstitucion() . "</td>" .
		                "<td>" . $resultado[$i]->getCantidad() . "</td>" .
		                "<td><p>" . $parrafo . "</td>" .
		                "<td>" . $resultado[$i]->getBodega() . "</td>" .
		                "<td>" .
		                "<button class='btnActualizar' onclick = 'Seleccionar(" . $resultado[$i]->getId() . ")'><i class='far fa-edit'></i></button>" .
		                "<button class='btnEliminar' onclick = 'ConfirmarEliminacion(" . $resultado[$i]->getId() . ")'><i class='fas fa-trash-alt'></i></button>" .
		                "</td>" .
		                "</tr>";
			}

			echo $datos;
		}

		echo $resultado;

	} else if (isset($_POST['eliminar'])) {
		
		$consultas = new consultasInsumo();

		echo $consultas->EliminarInsumo($_POST['eliminar']);

	} else if (isset($_POST['seleccionar'])) {

		$consultas = new consultasInsumo();
		$resultado = $consultas->SeleccionarInsumo($_POST['seleccionar']);

		echo $resultado->toString();

	} else if (isset($_POST['actualizar'])) {

		$consulta = new consultasInsumo();

		echo $consulta->Actualizar(new insumo($_POST['id'], $_POST['articulo'], $_POST['institucion'], $_POST['cantidad'], $_POST['observacion'], $consulta->OntenerIdBodegaPorNombre($_POST['bodega'])));

	}
	else if(isset($_POST['contarFilas'])){
		$consulta = new consultasInsumo();
		echo $consulta->contarFilas();
	}
	else if(isset($_POST['buscarInsumoEspecifico'])){
		$variable = $_POST['buscarInsumoEspecifico'];
		$consultas = new consultasInsumo();
		$resultado = $consultas->buscarInsumoEspecifico($variable);

		if ($resultado != 0) {

			$datos = "";

			for ($i = 0; $i < count($resultado); $i++) {
					
				$parrafo = "";

				if (strlen($resultado[$i]->getObservacion()) < 50) {
					
					$parrafo = "<p>" . $resultado[$i]->getObservacion() . "</p>";
				} else {

					$parrafo = "<p>" . substr($resultado[$i]->getObservacion(), 0, 50) . "</p><p id='".($i+1)."' class='p'>" . 
					substr($resultado[$i]->getObservacion(), 50, -1) . "</p><input type='button' id='btn".($i+1)."' value='Mas...' onclick='Mas(".($i+1).");' />";
				}

				$datos.="<tr class = 'cla'>" .
		                "<td>" . ($i+1) . "</td>" .
		                "<td>" . $resultado[$i]->getArticulo() . "</td>" .
		                "<td>" . $resultado[$i]->getInstitucion() . "</td>" .
		                "<td>" . $resultado[$i]->getCantidad() . "</td>" .
		                "<td><p>" . $parrafo . "</td>" .
		                "<td>" . $resultado[$i]->getBodega() . "</td>" .
		                "<td>" .
		                "<button class='btnActualizar' onclick = 'Seleccionar(" . $resultado[$i]->getId() . ")'><i class='far fa-edit'></i></button>" .
		                "<button class='btnEliminar' onclick = 'ConfirmarEliminacion(" . $resultado[$i]->getId() . ")'><i class='fas fa-trash-alt'></i></button>" .
		                "</td>" .
		                "</tr>";
			}

			echo $datos;
		}

		echo $resultado;
	}
?>