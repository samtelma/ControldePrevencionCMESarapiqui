window.onload = function(event){	
	mostrarIntegrantes(calcularPaginaparaRegistros());
	crearPaginacion();
	limpiarCampos();
	verificarLimitePaginacion(false);
}

function detectarTeclaEnter_enBusqueda(e){

    var evt = e ? e : event;
    var key = window.Event ? evt.which : evt.keyCode;

    if(key == 13){

    	buscarIntegrante();
    }

}

function verificarPagina(paginaLimite,avanza){
    if(window.location.search == ""){
        document.getElementById('primeraPagina').style.pointerEvents = "none";
		history.pushState(null, "", "../CPresentacion/ventanaDirectorio.php?pagina= 1");
    }
    if(window.location.search.includes(paginaLimite)){
        document.getElementById('siguientePagina').style.pointerEvents = "none";
    }
    if(window.location.search.includes(1)){
        document.getElementById('primeraPagina').style.pointerEvents = "none";
    }
    if(avanza)
    	document.getElementById('primeraPagina').style.pointerEvents = "auto";
    if(!avanza)
        document.getElementById('siguientePagina').style.pointerEvents = "auto";
    if(avanza == null){
        if(window.location.search.includes(paginaLimite))
            document.getElementById('siguientePagina').style.pointerEvents = "none";    
        else
            document.getElementById('siguientePagina').style.pointerEvents = "auto";
        if(window.location.search.includes(1))
            document.getElementById('primeraPagina').style.pointerEvents = "none";    
        else
            document.getElementById('primeraPagina').style.pointerEvents = "auto";
    }
}

function verificarLimitePaginacion(avanza){
	
    var xhr=new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("contarFilas=true");

    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4 && xhr.status == 200){
            resultado = xhr.responseText;
            resultado = resultado/4;
            resultado = Math.ceil(resultado)-1;
            verificarPagina(resultado,avanza);

        }
    }
}

function cargarSiguienteInformacion(){
    var temp = calcularPaginaparaRegistros();
    temp = parseInt(temp)+1;
    history.pushState(null, "", "../CPresentacion/ventanaDirectorio.php?pagina= " + temp);
    verificarLimitePaginacion(true);
    mostrarIntegrantes(temp);


}
function cargarPreviaInformacion(){
    var temp = calcularPaginaparaRegistros();
    temp = parseInt(temp)-1;
    history.pushState(null, "", "../CPresentacion/ventanaDirectorio.php?pagina= " + temp);
    verificarLimitePaginacion(false);
    mostrarIntegrantes(temp);
}

function crearPaginacion(){
	$("#pagination").empty();
	var resultado = 0;
	var xhr=new XMLHttpRequest();
	xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("contarFilas=true");

	xhr.onreadystatechange = function(){
		if(xhr.readyState == 4 && xhr.status == 200){
			resultado = xhr.responseText;
			resultado = resultado/4;
			resultado = Math.ceil(resultado)-1;
			for(var i = 0; i < resultado; i++){
				var listNode = document.getElementById('pagination'),
					liNode = document.createElement("LI"),
					txtNode = document.createTextNode(i);
				var href = document.createElement("a");
				href.textContent = i+1;
				href.setAttribute('onclick',"cargarTablaPaginacionNumerica('"+(parseInt(i)+1)+"')");
				liNode.appendChild(href);
				listNode.appendChild(liNode);

			}
			verificarPagina(resultado);
		}
	}

}

function cargarTablaPaginacionNumerica(valor){
	history.pushState(null, "", "../CPresentacion/ventanaDirectorio.php?pagina= " + valor);
    verificarLimitePaginacion(null);
	mostrarIntegrantes(valor);

}

function calcularPaginaparaRegistros(){
	var url = window.location.search;
	var puertaRegistro = false;
	var numUrl = 0;
	if(url.length == 12){
		for(var i = 0; i < url.length; i++){
			if(puertaRegistro){
				if(url[i] == "%"){

				}
				else if(i >= 11){
					numUrl += url[i];			
				}
			}
			if(url[i] == "="){
				puertaRegistro = true;
			}
		}
	}
	else if(url.length == 9){
		for(var i = 0; i < url.length; i++){
			if(puertaRegistro){
				numUrl += url[i];			
			}
			if(url[i] == "="){
				puertaRegistro = true;
			}
		}
	}
	return  numUrl;

}

function mostrarIntegrantes(valor){
    
    var pagina = 0;
    if(valor == 0)
        pagina = calcularPaginaparaRegistros();
    if(valor > 0)
        pagina = valor;

    if(pagina == 1)
        pagina = parseInt(pagina)-1;
    else
        pagina = parseInt(pagina)*3;

	document.getElementById('encargado').innerHTML = "Administrado(a) por: Dennis Muñoz Azofeifa";
	var xhr=new XMLHttpRequest();
	xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("buscarIntegrantes=true" + "&pagina=" + pagina);
  	xhr.onreadystatechange = function () {

	    if (xhr.readyState == 4 && xhr.status == 200) {

	        var resultado = xhr.responseText;
	        if (resultado != "null") {
	            //Se aplica un split, para separar cada integrante.
	            var cadena = resultado.split("+");
	            $("tr").remove(".cla"); 
	            for (var i = 0; i < cadena.length -1; i++) { //Ciclo para mostrar cada integrante.

	               var datos = cadena[i].split(","); //Se aplica split, para dividir cada atributo del integrante.
	               if(datos[2] != "Default")
	               $("#tablaDirectorio").append(""+
	               	"<tr class = 'cla'>"+
		               	"<td>" + datos[2] + "</td>" +
		               	"<td>" + datos[3] + "</td>" +
		               	"<td>" + datos[4] + "</td>" +
		               	"<td>" + datos[6] + "</td>" +
	               	"</tr>"
	               	);
	            } 
	        } 

		}
	} 
}

//metodo para limpiar todos los campos de texto
function limpiarCampos(){
	document.getElementById('buscar').value = "";
}

function resultadoBusqueda(texto){	
  	if(texto == "Sin resultado"){
  		document.getElementById('buscar').value = "Sin resultados";
  		document.getElementById('buscar').style.color = "red";
  		document.getElementById('tablaDirectorio').style.display = "";
  	}
  	else if(document.getElementById('buscar').value == ""){
  		document.getElementById('buscar').style.color = "black";
  		mostrarIntegrantes();
  	}
}


function buscarIntegrante(){
	var buscar = document.getElementById('buscar').value;
	var tablaDirectorio = document.getElementById('tablaDirectorio');
	var td = "";
	var tr = "";
	var temp = "";
	var dato = "Sin resultado";
	var i = 0; 
	if(buscar != ""){
		while(i < tablaDirectorio.rows.length){
			tr = tablaDirectorio.rows[i];
			for(var j = 0; j < tr.cells.length-3; j++){
				td = tr.cells[j].innerHTML;
				if(buscar.toUpperCase() == td.toUpperCase()){
					temp = tr.cells[0].innerHTML + ",";
					if(!dato.includes(temp)){
						dato += temp;
					}
				}										
			}
			i++;
		}
		if(dato != "Sin resultado"){
			for(var i = 0 ; i < tablaDirectorio.rows.length; i++){
				if(dato.includes(tablaDirectorio.rows[i].cells[0].innerHTML) || 
					dato.includes(tablaDirectorio.rows[i].cells[1].innerHTML) || 
					dato.includes(tablaDirectorio.rows[i].cells[2].innerHTML) ||
					dato.includes(tablaDirectorio.rows[i].cells[3].innerHTML)){
					tablaDirectorio.rows[i].style.display = "";
				}
				else{
					tablaDirectorio.rows[i].style.display = "none";
				}
			}
		}
		else{
			if(dato == "Sin resultado"){

				buscar_en_baseD(buscar);

			}
			else
				resultadoBusqueda(dato);
		}
		
	}
	if(buscar == ""){
		resultadoBusqueda(buscar);
	}

}

function buscar_en_baseD(variable){

	var tablaDirectorio = document.getElementById("tablaDirectorio");
	var xhr = new XMLHttpRequest();
	xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("buscarIntegranteEspecifico=" + variable);

	xhr.onreadystatechange = function () {

		if (xhr.readyState == 4 && xhr.status == 200) {

			var resultado = xhr.responseText;
			if (resultado != "") {
	            //Se aplica un split, para separar cada integrante.
	            var cadena = resultado.split("+");
	            $("tr").remove(".cla"); 
	            for (var i = 0; i < cadena.length -1; i++) { //Ciclo para mostrar cada integrante.

	               var datos = cadena[i].split(","); //Se aplica split, para dividir cada atributo del integrante.
	               $("#tablaDirectorio").append(""+
	               	"<tr class = 'cla'>"+
		               	"<td>" + datos[2] + "</td>" +
		               	"<td>" + datos[3] + "</td>" +
		               	"<td>" + datos[4] + "</td>" +
		               	"<td>" + datos[6] + "</td>" +
		               	"<td hidden>" + datos[0] +"</td>" +
	               	"</tr>"
	               	);
	            } 
	        } 
	        else if(resultado == ""){
	        	resultadoBusqueda("Sin resultado");
	        }
	    }

	}

}
function campoVacio(){
	if(document.getElementById('buscar').value == ""){
		resultadoBusqueda("");
	}	
}

