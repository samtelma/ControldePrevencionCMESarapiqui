window.onload=function(){

    showSlides();
    limpiarCampos();
    llamarVentanaInicioSesion();
}

var slideIndex = 0;


function llamarVentanaInicioSesion(){ 
/**********************Modal*************************/
// Get the modal

    document.getElementById('myModal').style.top = "0px";

    var modal = document.getElementById('myModal');

    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];
    
    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {

        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {

        if (event.target == modal) {

            modal.style.display = "none";
        }
    }
}

function showSlides() {
    
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");

    for (i = 0; i < slides.length; i++) {

       slides[i].style.display = "none";  
    }
    slideIndex++;
    if (slideIndex > slides.length) {slideIndex = 1}    
    for (i = 0; i < dots.length; i++) {

        dots[i].className = dots[i].className.replace(" active", "");
    }

    slides[slideIndex-1].style.display = "block";  
    dots[slideIndex-1].className += " active";
    setTimeout(showSlides, 6000); // La imagen cambia cada 3 segundos
}

function iniciarSesion(){

    if(validarCedula() == true){
        var cedula = document.getElementById('cedulaInicio').value;
        var seguridad = document.getElementById('seguridadInicio').value;
        var envio = "iniciarSesion=true" + 
                    "&cedulaInicio=" + cedula +
                    "&seguridad=" + seguridad;

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
        xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        xhr.send(envio);

        xhr.onreadystatechange = function(){

            if(xhr.readyState == 4 && xhr.status == 200){
            
            }
        }
    }

}

function limpiarCampos(){
    document.getElementById('cedulaInicio').value = "";
    document.getElementById('seguridadInicio').value = "";
}

function validarCedula(){
    var contador = 0;
    var filtro = "123456789";
    var respuesta = false;
    var cedula = document.getElementById("cedulaInicio").value;
    for(var i = 0; i < cedula.length; i++){
        if(!filtro.includes(cedula[i])){
            contador++;
        }
    }

    if(contador >= 3 || cedula.length < 9 && cedula.length != 0){
        document.getElementById('cedulaInicio').style.borderColor = "red";
    }
    else if(contador < 3 || document.getElementById('cedulaInicio').value == ""){
        document.getElementById('cedulaInicio').style.borderColor = "";
        respuesta = true;   
    }

    return respuesta;
}

function detectarTeclaEnter_enSeguridad(e){

    var evt = e ? e : event;
    var key = window.Event ? evt.which : evt.keyCode;

    if(key == 13){

        iniciarSesion();
    }
}