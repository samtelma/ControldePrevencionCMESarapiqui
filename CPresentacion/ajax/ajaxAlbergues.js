window.onload = function(event) {
   
    history.pushState(null, "", "../CPresentacion/ventanaAlbergues.php?pagina= 1");
    Consultar(calcularPaginaparaRegistros());
    crearPaginacion();
}

$(document).ready(function() {
    
    $('#btn_registrar').click(function(){
        $("html, body").css("overflow","hidden");
        document.getElementById('cedulaDetalle').style.margin = "0px -600px 0px 0px";
        document.getElementById('botones').style.margin = "0px 190px";
        document.getElementById("modal").style.top = "0px";
        LimpiarCampos();
        CargarDatosResponsable("Sin definir");
    });
    $('#cancelar').click(function(){
        $("html, body").css("overflow","scroll");
        document.getElementById("modal").style.top = "-500vh";
        LimpiarCampos();
    });
    $('.btnEliminar').click(function(){
        $("html, body").css("overflow","hidden");
        document.getElementById("div_modal_confirmacion").style.top = "0px";
        LimpiarCampos();
    });
    $('#btn_cancelar_confirmacion').click(function(){
        $("html, body").css("overflow","scroll");
        document.getElementById("modal").style.top = "-500vh";
        LimpiarCampos();
    });
    document.getElementsByClassName("close")[0].onclick = function() {
        $("html, body").css("overflow","scroll");
        document.getElementById('modal').style.top = "-500vh";
    }

    window.onclick = function(event) {

        if (event.target == document.getElementById('modal')) {
            $("html, body").css("overflow","scroll");
            document.getElementById('modal').style.top = "-500vh";
        }
    }
});

function OcultarMensajes() {

    $(".mensaje").hide();
    $(".asterisco").hide();
}
/*
function ValidarTexto(e) {

   tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    // tecla = 8 -> es de retroceso.
    // tecla = 0 -> es de tabulador.
    // tecla = 32 -> es del espacio.
    // tecla = 44 -> es la d ela ',' "coma".
    if (tecla == 8 || tecla == 0 || tecla == 32 || tecla == 44) {

        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros y letras
    patron = /[A-Za-z0-9]/;
    tecla_final = String.fromCharCode(tecla);

    OcultarMensajes();

    return patron.test(tecla_final);
}
*/
function ValidarNumeros(e) {

    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite.
    if (tecla == 8 || tecla == 0) {

        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros.
    patron =/[0-9]/;
    tecla_final = String.fromCharCode(tecla);

    return patron.test(tecla_final);
}

function LimpiarCampos() { // Metodo que limpia los inputs de la pagina.

    OcultarMensajes();

    document.getElementById("nombre").value = "";
    document.getElementById("capacidad").value = "";
    document.getElementById("localidad").value = "";
    document.getElementById("luz").checked = false;
    document.getElementById("agua_potable").checked = false;
    document.getElementById("agua_no_potable").checked = false;
    document.getElementById("telefono").value = "";
    document.getElementById("cuartos").value = "";
    document.getElementById("cocina").value = "";
    document.getElementById("banos").value = "";
    document.getElementById("btn").value = "Registrar";
    document.getElementById('buscar').value = "";
}

function OcultarMensajes() { // Metodo que oculta (visiblemente), los mensajes (DIV).

    $(".mensaje").hide();  
    $(".asterisco").hide();
}

function CargarDatosResponsable(responsable) { // Metodo que carga los datos de responssable.

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraAlbergues.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("cargarDatosEncargado=true");

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {

                var res = resultado.split("+");
                var select = document.getElementById("responsable");

                for (var i = 0; i < res.length - 1; i++) {

                    select.options[i+1] = new Option(res[i]);

                    if (responsable == res[i]) {
                        
                        select.options[i+1].selected = true;
                    }
                }
            } else{

                alert("Error en cargar datos de encargados!\n" + resultado);
            }
        }
    }
}

function BTN() { // Metodo que verifica si el boton es para registrar o actualizar.

    OcultarMensajes();


    if (document.getElementById("btn").value == "Registrar") {

        Registrar();
    } else {

        Actualizar();
    }
}

function Registrar() { // Metodo que registra un albergue.
   
    try {
        
        var nombre    = document.getElementById("nombre").value;
        
        if (nombre != "") {

            // Obtenemos los datos.
            var encargado = document.getElementById("responsable").value;
            var capacidad = document.getElementById("capacidad").value;
            var localidad = document.getElementById("localidad").value;
            var telefono  = document.getElementById("telefono").value;
            var luz  = 0;
            var cuartos = document.getElementById('cuartos').value;
            var cocina = document.getElementById('cocina').value;
            if (document.getElementById("luz").checked) { // Validamos la luz.

                luz = 1;
            }
            var agua = -1;
            if (document.getElementById("agua_potable") != null) { // Validamos el agua.
                
                if (document.getElementById("agua_potable").checked) {

                    agua = 1; // True.
                }
            }
            if (document.getElementById("agua_no_potable") != null) {

                if (document.getElementById("agua_no_potable").checked) {

                    agua = 0; // False.
                }
            } 

            var banos = document.getElementById("banos").value;


            var enviar="registrar=true" +
                        "&nombre="    + nombre +
                        "&capacidad=" + capacidad +
                        "&encargado=" + encargado +
                        "&localidad=" + localidad +
                        "&telefono="  + telefono +
                        "&luz="       + luz +
                        "&agua="      + agua +
                        "&cuartos="   + cuartos +
                        "&cocina="    + cocina +
                        "&banos="     + banos;
        
            //alert(enviar);
            var xhr = new XMLHttpRequest();
            xhr.open("POST", "../CNegocio/controladoraAlbergues.php");
            xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
            xhr.send(enviar);
          
            xhr.onreadystatechange = function () {

                if (xhr.readyState == 4 && xhr.status == 200) {

                    var resultado = xhr.responseText;
                
                    if (resultado != 0) {

                        crearPaginacion();
                        Consultar();
                        LimpiarCampos();
                        document.getElementById('modal').style.top = "-500vh";
                    } else{

                        alert("Error en registrar albergue!.\n" + resultado);
                    }
                }
            }
        } else {

            $("#lNombre").show();
            //alert("Campo nombre vacio!");
        }
    } catch (error) {

        console.error(error);
    }
}

function Consultar(valor) { // Metodo que consulta la lista de albergures en la base de datos.

    var pagina = 0;
    if(valor == 0)
        pagina = calcularPaginaparaRegistros();
    if(valor > 0)
        pagina = valor;

    if(pagina == 1)
        pagina = parseInt(pagina)-1;
    else
        pagina = parseInt(pagina)*3;

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraAlbergues.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("consultar=true" + "&pagina=" + pagina);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {
         	
                $("tr").remove(".cla");
                $("#tabla").append(resultado);
            }
        }
    }
}

function ConfirmarEliminacion(id) {

    document.getElementById("modalEliminar").style.top = "0px";
    document.getElementById("label_id_albergue").innerHTML = id;
}

function Eliminar() { // Metodo que elimina un voluntario.

    var temp = document.getElementById("label_id_albergue").innerHTML;
    OcultarMensajes();

    document.getElementById("modalEliminar").style.top = "-500vh";

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraAlbergues.php");
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("eliminar=" + temp);
    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {

                //alert(resultado);
                if (resultado == 1) { // Exitoso.
                    crearPaginacion();
                    Consultar();
                    document.getElementById("label_id_albergue").innerHTML = "";
                }
            } else {

                alert("Error en eliminar albergue.\n" + resultado);
            }
        }
    }
    if (document.getElementById("btn").value == "Actualizar") {

        document.getElementById("btn").value = "Registrar";
    }
}

function Cancelar() {

    document.getElementById("label_id_albergue").innerHTML = "";
    document.getElementById("modalEliminar").style.top = "-500vh";
}

function Seleccionar(id) {
   
    OcultarMensajes();
    $("html, body").css("overflow","hidden");

    document.getElementById("modal").style.top = "0px";
    document.getElementById('cedulaDetalle').style.margin = "0px -600px 0px 0px";
    document.getElementById('botones').style.margin = "0px 190px";
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraAlbergues.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("seleccionar=" + id);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {
                //alert(resultado);
                var cadena = resultado.split("_");
                document.getElementById("cedulaDetalle").innerHTML = cadena[1];
                document.getElementById("id_id").value = cadena[0];                
                document.getElementById("nombre").value = cadena[1];
                document.getElementById("capacidad").value = cadena[2];
                CargarDatosResponsable(cadena[3] + " " + cadena[4] + " " + cadena[5]);
                document.getElementById("localidad").value = cadena[6];
                document.getElementById("telefono").value  = cadena[7];

                // Evaluar la luz.
                if (cadena[8] == "1") {
                    document.getElementById("luz").checked = true;
                } else {
                    document.getElementById("luz").checked;
                }

                // Evaluar el agua.
                if (cadena[9] == "1") {
                    document.getElementById("agua_potable").checked = true;
                } else if (cadena[9] == "0") {
                    document.getElementById("agua_no_potable").value = true;
                } else {
                    document.getElementById("agua_potable").checked = false;
                    document.getElementById("agua_no_potable").checked = false;
                }

                document.getElementById("cuartos").value = cadena[10];
                document.getElementById("cocina").value = cadena[11];
                document.getElementById("banos").value = cadena[12];

                document.getElementById("btn").value = "Actualizar";
            } else {

                $("#mensaje1").show();
                //alert("Error en seleccionar albergue!\n" + resultado);
            }
        }
    }
}

function Actualizar() {

    var id = document.getElementById("id_id").value;
    var nombre    = document.getElementById("nombre").value;
    var capacidad = document.getElementById("capacidad").value;
    var encargado = document.getElementById("responsable").value;
    var localidad = document.getElementById("localidad").value;
    var telefono  = document.getElementById("telefono").value;

    // Valido la luz.
    var luz  = 0;
    if (document.getElementById("luz").checked) { // Validamos la luz.

        luz = 1;
    }

    // Valido el agua.
    var agua = -1;
    if (document.getElementById("agua_potable") != null) { // Validamos el agua.
        
        if (document.getElementById("agua_potable").checked) {

            agua = 1; // True.
        }
    }
    if (document.getElementById("agua_no_potable") != null) {

        if (document.getElementById("agua_no_potable").checked) {

            agua = 0; // False.
        }
    } 

    var cuartos = document.getElementById("cuartos").value;
    var cocina = document.getElementById("cocina").value;
    var banos = document.getElementById("banos").value;

    var enviar ="actualizar=true" +
                "&id="        + id +
                "&nombre="    + nombre    +
                "&capacidad=" + capacidad +
                "&encargado=" + encargado +
                "&localidad=" + localidad +
                "&telefono="  + telefono +
                "&luz="       + luz +
                "&agua="      + agua +
                "&cuartos="   + cuartos +
                "&cocina="    + cocina +
                "&banos="     + banos;
   
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraAlbergues.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send(enviar);
   
    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {

                alert("Actualizaci&oacute;n exitosa");
                document.getElementById('modal').style.top = "-500vh";
                LimpiarCampos();
                Consultar();
            } else{

                $("#mensaje1").show();
                //alert("Error en actualizar albergue.\n" + resultado);
            }
        }
    }
}

function CancelarModal() {

    document.getElementById("modalEliminar").style.top = "-500vh";
    LimpiarCampos();
}

function detectarTeclaEnter_enBusqueda(e) {

    var evt = e ? e : event;
    var key = window.Event ? evt.which : evt.keyCode;

    if (key == 13) {

        buscarAlbergue();
    }
}

function buscarAlbergue() {

    var buscar = document.getElementById('buscar').value;
    var tablaAlbergue = document.getElementById('tabla');
    var td = "";
    var tr = "";
    var temp = "";
    var dato = "Sin resultado";
    var i = 1; 
    var puerta = false;

    if (buscar != "") {

        while (i < tablaAlbergue.rows.length) {

            tr = tablaAlbergue.rows[i];

            for (var j = 1; j < tr.cells.length-1; j++) {

                td = tr.cells[j].innerHTML;

                if (td.toUpperCase().includes(buscar.toUpperCase())) {

                    temp = tr.cells[0].innerHTML + ",";

                    if (!puerta) {

                        puerta = true;
                        dato = "";
                    }
                    if (!dato.includes(temp)) {

                        dato += temp;
                    }                    
                }
            }
            i++;
        }
        if (dato != "Sin resultado") {

            for (var i = 1 ; i < tablaAlbergue.rows.length; i++) {

                if (dato.includes(tablaAlbergue.rows[i].cells[0].innerHTML)) {

                    tablaAlbergue.rows[i].style.display = "";
                }
                else {

                    tablaAlbergue.rows[i].style.display = "none";
                }
            }
        }
        else {
            if (dato == "Sin resultado") {

                buscar_en_baseD(buscar);
            }
            else {

                resultadoBusqueda(dato);
            }
        }
    }
    if (buscar == "") {

        resultadoBusqueda(buscar);
    }
}

function buscar_en_baseD(variable) {

    alert(variable);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraAlbergues.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("buscarAlbergueEspecifico=" + variable);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            if (resultado != 0) {

                $("tr").remove(".cla");
                $("#tabla").append(resultado);
            }
            else {

                resultadoBusqueda("Sin resultado");
            }
        }
    }
}

function campoVacio() {

    if (document.getElementById('buscar').value == "") {

        resultadoBusqueda("");
    }   
}

function resultadoBusqueda(texto) { 

    if (texto == "Sin resultado") {

        document.getElementById('buscar').value = "Sin resultados";
        document.getElementById('buscar').style.color = "red";
        document.getElementById('tabla').style.display = "";
    }
    else if (document.getElementById('buscar').value == "") {

        document.getElementById('buscar').style.color = "black";
        Consultar();
    }
}

function verificarPagina(paginaLimite, avanza) {

    if (window.location.search == "") {

        document.getElementById('primeraPagina').style.pointerEvents = "none";
        location.href = "../CPresentacion/ventanaAlbergues.php?pagina=1";
    }
    if(window.location.search.includes(paginaLimite)){
        document.getElementById('siguientePagina').style.pointerEvents = "none";
    }
    if(window.location.search.includes(1)){
        document.getElementById('primeraPagina').style.pointerEvents = "none";
    }
    if(avanza)
        document.getElementById('primeraPagina').style.pointerEvents = "auto";
    if(!avanza)
        document.getElementById('siguientePagina').style.pointerEvents = "auto";
    if(avanza == null){
        if(window.location.search.includes(paginaLimite))
            document.getElementById('siguientePagina').style.pointerEvents = "none";    
        else
            document.getElementById('siguientePagina').style.pointerEvents = "auto";
        if(window.location.search.includes(1))
            document.getElementById('primeraPagina').style.pointerEvents = "none";    
        else
            document.getElementById('primeraPagina').style.pointerEvents = "auto";
    }
}

function verificarLimitePaginacion(avanza){
    
    var xhr=new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraAlbergues.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("contarFilas=true");

    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4 && xhr.status == 200){
            resultado = xhr.responseText;
            resultado = resultado/4;
            resultado = Math.ceil(resultado)-1;
            verificarPagina(resultado,avanza);

        }
    }
}

function cargarSiguienteInformacion(){
    var temp = calcularPaginaparaRegistros();
    temp = parseInt(temp)+1;
    history.pushState(null, "", "../CPresentacion/ventanaAlbergues.php?pagina= " + temp);
    verificarLimitePaginacion(true);
    Consultar(temp);


}
function cargarPreviaInformacion(){
    var temp = calcularPaginaparaRegistros();
    temp = parseInt(temp)-1;
    history.pushState(null, "", "../CPresentacion/ventanaAlbergues.php?pagina= " + temp);
    verificarLimitePaginacion(false);
    Consultar(temp);
}

function cargarTablaPaginacionNumerica(valor){
    history.pushState(null, "", "../CPresentacion/ventanaAlbergues.php?pagina= " + valor);
    verificarLimitePaginacion(null);
    Consultar(valor);

}

function crearPaginacion() {

    $("#pagination").empty();

    var resultado = 0;
    var xhr=new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraAlbergues.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("contarFilas=true");

    xhr.onreadystatechange = function() {

        if (xhr.readyState == 4 && xhr.status == 200) {

            resultado = xhr.responseText;
            resultado = resultado/4;
            resultado = Math.ceil(resultado)-1;

            for (var i = 0; i < resultado; i++) {

                var listNode = document.getElementById('pagination'),
                    liNode = document.createElement("LI"),
                    txtNode = document.createTextNode(i);
                var href = document.createElement("a");
                href.textContent = i+1;
                href.setAttribute('onclick',"cargarTablaPaginacionNumerica('"+(parseInt(i)+1)+"')");
                liNode.appendChild(href);
                listNode.appendChild(liNode);
            }
            verificarPagina(resultado);
        }
    }
}

function calcularPaginaparaRegistros() {

    var url = window.location.search;
    var puertaRegistro = false;
    var numUrl = 0;
    if (url.length == 12) {

        for (var i = 0; i < url.length; i++) {

            if (puertaRegistro) {

                if (url[i] == "%") {
                }
                else if (i >= 11) {

                    numUrl += url[i];           
                }
            }
            if (url[i] == "=") {

                puertaRegistro = true;
            }
        }
    }
    else if (url.length == 9) {

        for (var i = 0; i < url.length; i++) {

            if (puertaRegistro) {

                numUrl += url[i];           
            }
            if (url[i] == "=") {

                puertaRegistro = true;
            }
        }
    }
    return  numUrl;
}


function ObtenerTelefono() {

    if (document.getElementById("responsable").value != "") {

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "../CNegocio/controladoraAlbergues.php");
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send("telefono=" + document.getElementById("responsable").value);

        xhr.onreadystatechange = function () {

            if (xhr.readyState == 4 && xhr.status == 200) {

                var resultado = xhr.responseText;

                //alert(resultado);
                if (resultado != 0) {

                    //alert(resultado);
                    if (resultado != "") { // Exitoso.

                        document.getElementById("telefono").value = resultado;
                    }
                } else {

                    alert("Error al conseguir el telefono.\n" + resultado);
                }
            }
        }
    }
}