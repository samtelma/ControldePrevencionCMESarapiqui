<?php
	
	include '../CDatos/directorioConsultas.php';
	include '../CNegocio/validarEspacios.php';

	if(isset($_POST['registrar'])){

		echo registrarIntegrante();
	}

	else if(isset($_POST['contarFilas'])){
		$consulta = new directorioConsultas();
		echo $consulta->contarFilas();
	}

	else if(isset($_POST['verDetalle'])){
		$id = $_POST['id'];
		$consulta = new directorioConsultas();
		echo $consulta->buscarIntegranteEspecifico($id);
	}

	else if(isset($_POST['buscarIntegrantes'])){
		$pagina = $_POST['pagina'];
		$consultas = new directorioConsultas();
		echo $consultas->buscarIntegrantes($pagina);
		
	}
	else if(isset($_POST['actualizar'])){

		echo actualizarIntegrante();
	}
	else if(isset($_POST['seleccionar'])){

		$id = $_POST['seleccionar'];
		$consultas = new directorioConsultas();
		echo $consultas->devolverIntegrante($id);
	}
	else if(isset($_POST['eliminar'])){
		$id = $_POST['eliminar'];
		$consultas = new directorioConsultas();
		echo $consultas->eliminarIntegrante($id);
	}
	else if(isset($_POST['buscarIntegranteEspecifico'])){
		$dato = $_POST['buscarIntegranteEspecifico'];
		$temp = "";
		$puerta = true;
		//DEFINE LA LONGITUD DEL DATO POR BUSCAR
		for($i = 0; $i < strlen($dato); $i++){

			//SI EL DATO DETECTA UN ESPACIO QUITA EL ESPACIO Y AGREGA UNA COMA 
			if($dato[$i] == " " && $puerta){
				$temp .= ",";
			}
			//SI EL DATO NO CONTIENE ESPACIOS CONSTRUYE LA PALABRA LETRA POR LETRA
			if($dato[$i] != " " && $puerta){
				$temp .= $dato[$i];
			}
			//SI EL DATO TIENE UN + QUIERE SIGNIFICAR QUE BUSCA UN COMITÉ
			if(strpos($dato, "*") == true && $puerta){
				$temp = trim($dato,'*');
				$puerta = false;
			}
			
		}
		$consultas = new directorioConsultas();
		echo $consultas->buscarIntegranteEspecifico($temp);
		
	}
	else if(isset($_POST['buscarComiteEspecifico'])){
		$dato = $_POST['buscarComiteEspecifico'];
		$consulta = new directorioConsultas();
		echo $consulta->buscarIntegranteEspecifico($dato);
	}
	else if(isset($_POST['buscarComite'])){
		$consulta = new directorioConsultas();
		echo $consulta->buscarComite();
	}


	function actualizarIntegrante(){

		$respuesta = ""; 
		$id = $_POST['idActualizar'];
		$cedula = $_POST['cedulaActualizar'];
		$nombre = $_POST['nombreActualizar'];
		$primer_apellido = $_POST['primer_apellidoActualizar'];
		$segundo_apellido = $_POST['segundo_apellidoActualizar'];
		$telefono = $_POST['telefonoActualizar'];
		$institucion = $_POST['institucionRepresentadaActualizar'];
		$puesto = $_POST['seleccionarPuestoActualizar'];
		$comite = $_POST['comite'];
		$correo = $_POST['correoActualizar'];

		$validar = new validarEspacios();

		if($validar->validarCampos($cedula) 
			&& $validar->validarCampos($nombre)
			&& $validar->validarCampos($primer_apellido)
			&& $validar->validarCampos($puesto)){

			$integrante= new directorio($cedula,$nombre,$primer_apellido,$segundo_apellido,$telefono,$institucion,$puesto,$comite,$correo);
			$consulta = new directorioConsultas();
			$resultado = $consulta->actualizarIntegrante($id,$integrante);

			if($resultado == 1){
				$respuesta = 1;
			}
			else{
				$respuesta = 2;
			}
		}
		else{
			$respuesta = 0;
		}

		return $respuesta;
	}

	function registrarIntegrante(){

		$respuesta = "";
		$cedula = $_POST['cedula'];
		$nombre = $_POST['nombre'];
		$primer_apellido = $_POST['primer_apellido'];
		$segundo_apellido = $_POST['segundo_apellido'];
		$telefono = $_POST['telefono'];
		$institucion = $_POST['institucionRepresentada'];
		$puesto = $_POST['seleccionarPuesto'];
		$correo = $_POST['correo'];

		//VALIDAR LOS ESPACIOS.
		$validar = new validarEspacios();
		if ($validar->validarCampos($cedula) &&
			$validar->validarCampos($nombre) &&
			$validar->validarCampos($primer_apellido) &&
			$validar->validarCampos($puesto)){
			//ESPACIOS ESCRITOS CORRECTAMENTE.
			//CREAR UN INTEGRANTE AL DIRECTORIO.
			$integrante = new directorio($cedula, $nombre, $primer_apellido, $segundo_apellido, $telefono,$institucion,$puesto,"", $correo);

			if(strpos($integrante->getCedulaIntegranteD(), "-") == true){
				$cedula = split("-", $integrante->getCedulaIntegranteD());
				$respuesta = $cedula[0]."".$cedula[1]."".$cedula[2];

			}

			//ENVIARLO A CONSULTAS DE INSERTAR EL INTEGRATE.
			$consulta = new directorioConsultas();
			$resultado = $consulta->insertarIntegrante(
				$integrante->getCedulaIntegranteD(),
				$integrante->getNombreIntegranteD(),
				$integrante->getApellido1IntegranteD(),
				$integrante->getApellido2IntegranteD(),
				$integrante->getTelefonoIntegranteD(),
				$integrante->getInstitucionIntegranteD(),
				$integrante->getPuestoIntegranteD(),
				$integrante->getCorreoIntegranteD());
			
			//VERIFICANDO RESULTADO.
			if ($resultado == 1) {
				//DATOS ENVIADOS CORRECTAMENTE.
				$respuesta = 1; // Solo va ser uno, si la consulta esta correcta.
			} //Fin if.
		} // Si no estan correcto los datos, se queda en '0'.

		else{
			$respuesta = 0;
		}
		
		return $respuesta;
	}




?>