window.onload = function(event){

	document.getElementById('tituloCabecera').innerHTML += "Administraci&oacute;n de Preparativos y Respuestas";
	mostrarTituloAdministrador();
}

function mostrarTituloAdministrador(){

	document.getElementById('encargado').innerHTML += "Dennis Muñoz";
}

$(document).ready(function(){
    var modal = document.getElementById('myModal');

    //LLAMA EL OBJETO QUE PERMITE CERRAR EL MODAL
    var span = document.getElementsByClassName("close")[0];

    //CUANDO EL USUARIO DA CLICK EN LA X DEL MODAL HARA QUE SE CIERRE
    span .onclick = function() {
        cancelarDato();
    }

    //CUANDO EL USUARIO DA CLICK EN CUALQUIER LUGAR FUERA DEL MODAL HARA QUE SE CIERRE
    window.onclick = function(event) {
        if (event.target == modal) {
            cancelarDato();
        }
    }


});


var guardarArea = 0;
function desplegarInformacion(valor){
	document.getElementById('buscar').value = "";
    history.pushState(null, "", "../CPresentacion/ventanaAdminPrepResp.php?pagina= 1");

    if(valor == '1')
    	informacionAlertas();
	   	
    if(valor == '2')
    	informacionAlbergues();
    
    if(valor == '3')    		
    	informacionAnuncios();
	    	
    if(valor == '4')    		
    	informacionResponsable();
	
 	if(valor == '5')    		
    	informacionVoluntarios();
	
	if(valor == '6')    		
    	informacionZonaRiesgo();
    
    if(valor == "7")
    	informacionDirectorio();

}

function informacionAlertas(){
	document.getElementById('tituloCabecera').innerHTML = "Secci&oacute;n de Alertas";
	document.getElementById('opcionesMenu').style.display = "block";
}

function informacionAnuncios(){
	document.getElementById('tituloCabecera').innerHTML = "Secci&oacute;n de Anuncios";
	document.getElementById('opcionesMenu').style.display = "block";
}

function informacionAlbergues(){
	document.getElementById('tituloCabecera').innerHTML = "Secci&oacute;n de Albergues";
	document.getElementById('opcionesMenu').style.display = "block";
}

function informacionResponsable(){
	document.getElementById('tituloCabecera').innerHTML = "Secci&oacute;n de Responsable";
	document.getElementById('opcionesMenu').style.display = "block";
}

function informacionVoluntarios(){
    //VARIABLE QUE PERMITE LE INGRESO Y LA CARGA DE INFORMACION DE ACUERDO AL AREA
    guardarArea = 5;
    //MODIFICACION DE CAMPOS GENERALES Y APARICION
    document.getElementById('tituloCabecera').innerHTML = "Secci&oacute;n de Voluntarios";
    document.getElementById('opcionesMenu').style.display = "block";
    document.getElementById('buscar').title = "Nombre, Apellido es lo que busca";
    document.getElementById('buscar').style.display = "block";
    document.getElementById('opcionesMenu').style.display = "block";
    document.getElementById('btn_registrar').style.display = "block";
    document.getElementById('btn_registrar').value = "Registrar Voluntario";
    document.getElementById('tabla').style.display = "block";
    document.getElementById('paginacion').style.display = "block";
    crearPaginacion("../CNegocio/controladoraVoluntario.php");
    //LLAMADA DE METODOS
    mostrarIntegrantes(calcularPaginaparaRegistros());
    //ELIMINA EL CONTENIDO DE LA TABLA PARA HACER LA CARGA RESPECTIVA
    $("#tabla THEAD").remove();
    $("#tablaDirectorio tr").remove();
    $("#tabla").append(""+
        "<THEAD>"+
                "<TR>" +
                    "<TH>Fila</TH>"+
                    "<TH>Nombre:</TH>"+
                    "<TH>Tel&eacute;fono:</TH>"+
                    "<TH>Detalle:</TH>"+
                    "<TH>Opciones</TH>"+
                "</TR>" +
        "</THEAD><TR>"
    );
    verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",false);
}

function informacionZonaRiesgo(){
	document.getElementById('tituloCabecera').innerHTML = "Secci&oacute;n de Zonas de Riegos";
	document.getElementById('opcionesMenu').style.display = "block";
}

//FUNCIONES DEL DIRECTORIO
function informacionDirectorio(){
	//VARIABLE QUE PERMITE LE INGRESO Y LA CARGA DE INFORMACION DE ACUERDO AL AREA
    guardarArea = 7;
	//MODIFICACION DE CAMPOS GENERALES Y APARICION
	document.getElementById('buscar').value = "";
	document.getElementById('tituloCabecera').innerHTML = "Secci&oacute;n de Directorio";
	document.getElementById('buscar').placeholder = "Buscar integrante";
	document.getElementById('buscar').title = "Nombre, Apellido, Instituci&oacute;n es lo que busca";
	document.getElementById('buscar').style.display = "block";
	document.getElementById('opcionesMenu').style.display = "block";
	document.getElementById('tabla').style.display = "block";
	document.getElementById('paginacion').style.display = "block";
	//LLAMADA DE METODOS
	crearPaginacion("../CNegocio/controladoraDirectorio.php");
	mostrarIntegrantes(calcularPaginaparaRegistros());
    $("#tabla THEAD").remove();
    $("#tablaDirectorio tr").remove();
	$("#tabla").append(""+
		"<THEAD>"+
				"<TR>" +
					"<TH>Nombre</TH>"+
					"<TH>Primer Apellido:</TH>"+
					"<TH>Segundo Apellido:</TH>"+
					"<TH>Tel&eacute;fono:</TH>"+
					"<TH>Correo electr&oacute;nico:</TH>"+
					"<TH>Instituci&oacute;n Representada:</TH>"+
					"<TH>Comit&eacute; Representado:</TH>"+
					"<TH>Opciones</TH>"+
				"</TR>" +
		"</THEAD><TR>"
		);
	verificarLimitePaginacion("../CNegocio/controladoraDirectorio.php",false);
}

//AL CARGAR UN MODAL DE ACTUALIZAR O ELIMINAR CARGA LOS COMITES EXISTENTES REGISTRADOS
function llenarOpcionesComite(valor){
	
	var xhr=new XMLHttpRequest();
	xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("buscarComite=true");

	xhr.onreadystatechange = function(){

		if(xhr.readyState == 4 && xhr.status == 200){

			var resultado = xhr.responseText;
			if(resultado != ""){
				var datos = resultado.split(',');
				$("#seleccionarComite").empty()
				$("#seleccionarComite").append(new Option("Seleccione una opción","",true,true));
				document.getElementById('seleccionarComite').options[0].disabled = true;
				for(var i = 0; i <= datos.length-1; i++){
					if(datos[i] != ""){
                        if(datos[i] == valor)
                            $("#seleccionarComite").append(new Option(datos[i],datos[i],true,true));
                        else
                            $("#seleccionarComite").append(new Option(datos[i],datos[i],true,false));
						$("#seleccionarComite OPTION[VALUE = 'Ninguno']").remove()
						$("#seleccionarComite OPTION[VALUE = 'Otro']").remove()
						$("#seleccionarComite").append(new Option("Otro","Otro",true,false));
                        if(valor == "Ninguno")
						  $("#seleccionarComite").append(new Option("Ninguno","Ninguno",true,true));
                        else
                            $("#seleccionarComite").append(new Option("Ninguno","Ninguno",true,false)); 
					}
				}			
			}	
		}
	}
}

//EN CASO DE USAR LA FUNCION DE OTRO AGREGAR EL NUEVO COMITE PARA SU REGISTRO
function agregarNuevoComite(){

	if(document.getElementById('seleccionarComite').value == "Otro"){
		var nuevoComite = prompt("Ingrese el nombre del comite","");
        if(nuevoComite != null){
            $("#seleccionarComite").append(new Option(nuevoComite,nuevoComite,true,true));
            $("#seleccionarComite OPTION[VALUE = 'Ninguno']").remove()
            $("#seleccionarComite OPTION[VALUE = 'Otro']").remove()
            $("#seleccionarComite").append(new Option("Otro","Otro",true,false));
            $("#seleccionarComite").append(new Option("Ninguno","Ninguno",true,false));
        }
        if(nuevoComite == null){
            document.getElementById('seleccionarComite').selectedIndex = 0;
        }
	}

}

//CUANDO SE CARGA LA INFORMACIÓN SI EL SUJETO SELECCIONADO CUENTA CON DOS NUMEROS
//ESTAN SEPARADOS POR / Y HABILITA EL OTRO CAMPO DE TELEFONO
function habilitarbotonSegundoTelefono(valor){
    if(valor.includes("/")){
        document.getElementById('botonTelefonoExtra').disabled = true;
        document.getElementById('telefonoExtra').style.display = "block";
        document.getElementById('telefono').value = valor.split("/")[0];
        document.getElementById('segundoTelefono').value = valor.split("/")[1];
    }   

}

//FIN DE LAS FUNCIONES DEL DIRECTORIO
//
//
//
//

//METODOS DEL AREA DE VOLUNTARIOS
function Mas(i){
    if (document.getElementById(i).style.display == "none" || document.getElementById(i).style.display == "") {

        document.getElementById(i).style.display = "block";
        document.getElementById("btn" + i).value = "Menos...";
    } else {
        document.getElementById(i).style.display = "none";
        document.getElementById("btn" + i).value = "Mas...";
    }
    
    //alert(i);
}

function registrarVoluntario() { // metodo que registra una nuevo voluntario.

    // Obteniendo los valores de los inputs.
    var nombre = document.getElementById("nombre").value;

    // Se puede continuar si solo si, el nombre se ingreso.
    if (nombre != "") {

        // Creando el STRING para enviar a la controladora.
        var enviar = "registrar=true" + "&nombre=" + nombre + "&telefono=" + document.getElementById("telefono").value + "&detalle=" + document.getElementById("detalle").value + " ";

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(enviar);

        xhr.onreadystatechange = function () {

            if (xhr.readyState == 4 && xhr.status == 200) {

                var resultado = xhr.responseText;

                //alert(resultado);
                if (resultado != 0) {


                    document.getElementById('myModal').style.top = "-100vh";
                    setTimeout("verVentanaConfirmacion('registro')",500);

                } else { // Error !!!.

                    document.getElementById('myModal').style.top = "-100vh";
                    setTimeout("verVentanaConfirmacion('registroError')",500);
                }
            }
        }
    } 
}

function eliminarVoluntario() { // Metodo que elimina un voluntario.
    document.getElementById("myModal").style.top = "-120vh";

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("eliminar=" + document.getElementById("id_id").value);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {

                //alert(resultado);
                if (resultado == 1) { // Exitoso.

                    setTimeout("verVentanaConfirmacion('eliminar')",300);
                    document.getElementById("id_id").value = "";
                }
            } 
        }
    }
}

function actualizarVoluntario(){

    document.getElementById('myModal').style.top = "-120vh";
    var nombre = document.getElementById("nombre").value;
    var telefono = document.getElementById('telefono').value.split("-")[0] + "" + document.getElementById('telefono').value.split("-")[1];
    if (nombre != "") {
        var enviar = "actualizar=true" + "&id=" + document.getElementById("id_id").value + "&nombre=" + nombre + "&telefono=" + telefono + "&detalle=" + document.getElementById("detalle").value + " ";

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(enviar);

        xhr.onreadystatechange = function () {

            if (xhr.readyState == 4 && xhr.status == 200) {

                var resultado = xhr.responseText;

                //alert(resultado);
                if (resultado != 0) {

                    setTimeout("verVentanaConfirmacion('actualizar')",300);
                    document.getElementById("id_id").value = "";
                } else {

                    setTimeout("verVentanaConfirmacion('actualizarError')",300);
                    document.getElementById("id_id").value = "";
                }
            }
        }
    } 
}

function AceptarLetrasNumeros(e) {

    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    // tecla = 8 -> es de retroceso.
    // tecla = 0 -> es de tabulador.
    // tecla = 32 -> es del espacio.
    if (tecla == 8 || tecla == 0 || tecla == 32) {

        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros y letras
    patron = /[A-Za-z0-9]/;
    tecla_final = String.fromCharCode(tecla);

    return patron.test(tecla_final);
}

function AceptarLetrasNumerosComa(e) {

    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    // tecla = 8 -> es de retroceso.
    // tecla = 0 -> es de tabulador.
    // tecla = 32 -> es del espacio.
    // tecla = 44 -> es la d ela ',' "coma".
    if (tecla == 8 || tecla == 0 || tecla == 44 || tecla == 32) {

        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros y letras
    patron = /[A-Za-z0-9]/;
    tecla_final = String.fromCharCode(tecla);

    return patron.test(tecla_final);
}


//FIN DE METODOS DEL AREA DE VOLUNTARIOS
//
//
//
//




//METODOS GLOBALES

function verModalRegistrar(){
    
    if(guardarArea == 5){
        document.getElementById('encabezadoModal').innerHTML = "Registrar Voluntario";
        document.getElementById('aceptarDatos').value = "Registrar";
        document.getElementById('myModal').style.top = "0px";   
        cargaModalGlobal(2,"");        
    }
}

function mostrarIntegrantes(valor){

	var pagina = 0;
	if(valor == 0)
		pagina = calcularPaginaparaRegistros();
	if(valor > 0)
		pagina = valor;

	if(pagina == 1)
		pagina = parseInt(pagina)-1;
	else
		pagina = parseInt(pagina)*3;

	switch(guardarArea){

		case guardarArea = 7:
			var xhr=new XMLHttpRequest();
			xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
			xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xhr.send("buscarIntegrantes=true" + "&pagina=" + pagina);
		  	xhr.onreadystatechange = function () {

			    if (xhr.readyState == 4 && xhr.status == 200) {

			        var resultado = xhr.responseText;
			        if (resultado != "null") {
                        if(resultado.split(",")[4] == "")
                            document.getElementById("encabezadoModal").innerHTML = resultado.split(",")[2] + " " + resultado.split(",")[3];    
                        else
                            document.getElementById("encabezadoModal").innerHTML = resultado.split(",")[2] + " " + resultado.split(",")[3] + " " + resultado.split(",")[4];    			        	
			            //Se aplica un split, para separar cada integrante.
			            var cadena = resultado.split("+");
			            $("tr").remove(".cla"); 
			            for (var i = 0; i < cadena.length -1; i++) { //Ciclo para mostrar cada integrante.
			               var datos = cadena[i].split(","); //Se aplica split, para dividir cada atributo del integrante.
			               if(datos[2] != "Default"){
		    	               	$("#tablaDirectorio").append(""+
		    		                	"<tr class = 'cla'>"+
		    		                    	"<td hidden>" +  datos[1]  + " </td>" +
		    		                    	"<td>" + datos[2] + "</td>" +
		    			                    "<td>" + datos[3] + "</td>" +
		    			                    "<td>" + datos[4] + "</td>" +
		    			                    "<td>" + datos[5] + "</td>" +
		    			                    "<td>" + datos[9] + "</td>" +
		    			                    "<td>" + datos[6] + "</td>" +
		    			                    "<td>" + datos[8] + "</td>" +
		    			                    "<td>" + "<BUTTON ID = 'actualizar' NAME = 'actualizar' ONCLICK = 'Seleccionar("+datos[0]+")'><i class='far fa-edit'></i></BUTTON>"+	
		    			                    "<INPUT TYPE ='TEXT' NAME = 'idAjax' ID = 'idAjax' VALUE = "+ datos[0] +" MAXLENGTH = '1' SIZE = '1' DISABLED HIDDEN/>" + "</td>" +
		    			                "</tr>"
		    			        );
		                    }
			            } 
			        } 

				}
			} 
			break;
		case guardarArea = 5:
            var xhr = new XMLHttpRequest();
            xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.send("consultar=true" + "&pagina=" + pagina);

            xhr.onreadystatechange = function () {

                if (xhr.readyState == 4 && xhr.status == 200) {

                    var resultado = xhr.responseText;
                        
                    if (resultado != 0) {
                        $("tr").remove(".cla"); 
                        $("#tablaDirectorio").append(resultado);
                    }
                }
            }
			break;
	}

	
}

function cargaModalGlobal(puerta,variableId){

	switch(guardarArea){

		case guardarArea = 7:
			var xhr=new XMLHttpRequest();
			xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
			xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xhr.send("verDetalle=true" + "&id=" + variableId);
			xhr.onreadystatechange = function () {

				if (xhr.readyState == 4 && xhr.status == 200) {

					var resultado = xhr.responseText;
					if (resultado != "null") {
						if(resultado.split(",")[4] == "")
							document.getElementById("encabezadoModal").innerHTML = resultado.split(",")[2] + " " + resultado.split(",")[3];    
						else
							document.getElementById("encabezadoModal").innerHTML = resultado.split(",")[2] + " " + resultado.split(",")[3] + " " + resultado.split(",")[4];    
						var cadena = resultado.split(",");
						$("#tablaxinformacionDetallada").append("" +
							"<TBODY>" +
								"<TR hidden>" +
									"<TD>" + "<INPUT TYPE='text' NAME='id' ID='id' VALUE = '"+cadena[0]+"'/>" + "</TD>" +
								"</TR>" +
							"<TR>" +
								"<TD>C&eacute;dula:" +
									"<INPUT TYPE='text' NAME='cedula' ID='cedula' SIZE = '18' MAXLENGTH = '15' ONBLUR = validarCedula('comprobarCampo') PLACEHOLDER = 'Campo no obligatorio' VALUE = '"+cadena[1]+"' disabled/>" +
								"</TD>" +
							"</TR>" +
							"<TR>" +
								"<TD>Nombre:" +
									"<INPUT TYPE='text' NAME='nombre' ID='nombre' SIZE = '30' MAXLENGTH = '30' ONBLUR = validarCampos('comprobarCampo','nombre') PLACEHOLDER = 'Escriba su nombre'VALUE = '"+cadena[2]+"' disabled>" +
								"</TD>" +
							"</TR>" + 
							"<TR>" +
								"<TD>Apellidos:"+
									"<INPUT TYPE='text' NAME='primer_apellido' ID='primer_apellido' SIZE = '30' MAXLENGTH = '30' ONBLUR = validarCampos('comprobarCampo','apellido1') PLACEHOLDER = 'Escriba su primer apellido' VALUE = '"+cadena[3]+"' disabled/>" +
									"<INPUT TYPE='text' NAME='segundo_apellido' ID='segundo_apellido' SIZE = '30' MAXLENGTH = '30' ONBLUR = validarCampos('comprobarCampo','apellido2') PLACEHOLDER = 'Escriba su segundo apellido' VALUE = '"+cadena[4]+"' disabled/>" +
								"</TD>" +
							"</TR>" +
							"<TR>" +
								"<TD>Tel&eacute;fono:" +
									"<INPUT TYPE='text' NAME='telefono' ID='telefono' SIZE = '15' MAXLENGTH = '8' ONKEYPRESS = 'return validacionNumerico(event)' ONBLUR = validarCampos('comprobarCampo','telefono') PLACEHOLDER = 'Escriba su teléfono' VALUE = '"+cadena[5]+"' disabled/>" +
									"<INPUT TYPE ='button' NAME='botonTelefonoExtra' ID='botonTelefonoExtra' VALUE = '+' ONCLICK = insertarTelefonoExtra('registrar')></TD>" +
							"</TR>" +
							"<TR ID = 'telefonoExtra' style = 'display: none;' ALIGN = 'center'>" +
								"<TD>Tel&eacute;fono Extra:" +
									"<INPUT TYPE='text' NAME='segundoTelefono' ID='segundoTelefono' SIZE = '15' MAXLENGTH = '8' ONKEYPRESS = 'return validacionNumerico(event)' ONBLUR = validarCampos('comprobarCampo','segundoTelefono') disabled/>" +
								"</TD>" +
							"</TR>" +
							"<TR>" +
								"<TD>Correo electr&oacute;nico:"+
									"<INPUT TYPE='text' NAME='correo' ID='correo' SIZE = '25' MAXLENGTH = '70' ONBLUR = validarCampos('comprobarCampo','correo') PLACEHOLDER = 'Escriba su correo electrónico' VALUE = '"+cadena[9]+"' disabled/>" +
								"</TD>" +
							"</TR>" +
							"<TR>" +
								"<TD>Instituci&oacute;n:<INPUT TYPE='text' NAME='institucionRepresentada' ID='institucionRepresentada' MAXLENGTH = '40' SIZE = '40' ONBLUR = validarCampos('comprobarCampo','institucion') PLACEHOLDER = 'Escriba su institución o empresa' VALUE = '"+cadena[6]+"' disabled/>" +
							"</TR>" );
						if(cadena[7] == "Propietario"){
							$("#tablaxinformacionDetallada").append("" +
								"<TR>" +
									"<TD>Puesto:" +
										"<SELECT ID = 'seleccionarPuesto' NAME = 'seleccionarPuesto' ONBLUR = validarCampos('comprobarCampo','puesto') disabled>" +
											"<OPTION VALUE = '' SELECTED DISABLED>Seleccionar puesto</OPTION>" +
											"<OPTION VALUE = 'Propietario' ID = 'Propietario' NAME  = 'Propietario' SELECTED>Propietario</OPTION>" +
											"<OPTION VALUE = 'Asistente' ID = 'Asistente' NAME = 'Asistente'>Asistente</OPTION>" +
										"</SELECT>" +
									"</TD>" +
								"</TR>"
								);
						}
						else if(cadena[7] == "Asistente"){
							$("#tablaxinformacionDetallada").append("" +
								"<TR>" +
									"<TD>Puesto:" +
										"<SELECT ID = 'seleccionarPuesto' NAME = 'seleccionarPuesto' ONBLUR = validarCampos('comprobarCampo','puesto') disabled>" +
											"<OPTION VALUE = '' SELECTED DISABLED>Seleccionar puesto</OPTION>" +
											"<OPTION VALUE = 'Propietario' ID = 'Propietario' NAME  = 'Propietario'>Propietario</OPTION>" +
											"<OPTION VALUE = 'Asistente' ID = 'Asistente' NAME = 'Asistente' SELECTED>Asistente</OPTION>" +
										"</SELECT>" +
									"</TD>" +
								"</TR>"
								);
						}                
						$("#tablaxinformacionDetallada").append("" +
							"<TR>" +
								"<TD>Comit&eacute;:"+
									"<SELECT ID = 'seleccionarComite' NAME = 'seleccionarComite' ONCLICK = agregarNuevoComite() ONCHANGE = agregarNuevoComite() ONBLUR = validarCampos('comprobarCampo','comite') disabled>" +
									"</SELECT>" +
								"</TD>" +
							"</TR>"+
						"</TBODY>"
						);

					}
		                //LLAMA EL METODO QUE DESHABILITA EL BOTON DE AGREGAR UN SEGUNDO TELEFONO Y SEPARA LOS TELEFONOS EN LOS CAMPOS
		                //RESPECTIVOS, EVITANDO COLOCAR LOS DOS TELEFONOS CON EL / EN UN MISMO CAMPO
		                habilitarbotonSegundoTelefono(cadena[5]);
		                llenarOpcionesComite(cadena[8]);
		                $("#telefono").mask("9999-9999");   
		                $("#segundoTelefono").mask("9999-9999");    
		                $(".asterisco").hide();
		            }
		        }
			break;

		case guardarArea = 5:
            if(puerta == 1){
                var xhr=new XMLHttpRequest();
                xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
                xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                xhr.send("verDetalle=true" + "&id=" + variableId);
                xhr.onreadystatechange = function () {

                    if (xhr.readyState == 4 && xhr.status == 200) {

                        var resultado = xhr.responseText;
                        if (resultado != "null") {
                            var cadena = resultado.split("_");
                            document.getElementById('encabezadoModal').innerHTML = cadena[1];
                            $("#tablaxinformacionDetallada").append("" +
                                "<tbody>" +
                                    "<tr hidden><td><input type = 'text' id = 'id_id' value = '"+cadena[0]+"' /></td></tr>" +
                                    "<tr>" +
                                        "<td>" +
                                            "<label for='nombre'>Nombre Completo:</label>" +
                                            "<font color=red ><label id='lNombre' class='asterisco'> *</label></font>" +
                                            "<input type='text' name='nombre' id='nombre' size='30' maxlength='30' value = '"+cadena[1]+"' placeholder='Michael Andres Salas Granados' onkeypress=return AceptarLetrasNumeros(event) />" +
                                        "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td>" +
                                            "<label for='telefono'>Tel&eacute;fono:</label>" +
                                            "<input type='text' name='telefono' id='telefono' size='10' maxlength='11' value= '"+cadena[2]+"' onkeypress= return validacionNumerico(event) placeholder='88-77-66-55' />" +
                                        "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td>" +
                                            "<label for='detalle'>Detalle:</label>" +
                                            "<textarea id='detalle' rows='5' cols='52' maxlength='300' onkeypress=return AceptarLetrasNumerosComa(event) placeholder='Escribe aqu&iacute; porqu&eacute; quieres ser voluntario' >"+cadena[3]+"</textarea>" +
                                        "</td>" +
                                    "</tr>" +
                                "</tbody>"
                            );
                            $("#telefono").mask("9999-9999");  
                        }
                    }
                }
            }
            if(puerta == 2){
                $("#tablaxinformacionDetallada").append("" +
                    "<tbody>" +
                        "<tr hidden><td><input type = 'text' id = 'id_id'/></td></tr>" +
                        "<tr>" +
                            "<td>" +
                                "<label for='nombre'>Nombre Completo:</label>" +
                                "<font color=red ><label id='lNombre' class='asterisco'> *</label></font>" +
                                "<input type='text' name='nombre' id='nombre' size='30' maxlength='30' placeholder='Michael Andres Salas Granados' onkeypress=return AceptarLetrasNumeros(event) />" +
                            "</td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>" +
                                "<label for='telefono'>Tel&eacute;fono:</label>" +
                                "<input type='text' name='telefono' id='telefono' size='10' maxlength='11' onkeypress=return validacionNumerico(event) placeholder='88-77-66-55' />" +
                            "</td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>" +
                                "<label for='detalle'>Detalle:</label>" +
                                "<textarea id='detalle' rows='5' cols='52' maxlength='300' onkeypress=return AceptarLetrasNumerosComa(event) placeholder='Escribe aqu&iacute; porqu&eacute; quieres ser voluntario' ></textarea>" +
                            "</td>" +
                        "</tr>" +
                    "</tbody>"
                );
                $("#telefono").mask("9999-9999");  

            }
            if(puerta == 3){
                var xhr=new XMLHttpRequest();
                xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
                xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                xhr.send("verDetalle=true" + "&id=" + variableId);
                xhr.onreadystatechange = function () {

                    if (xhr.readyState == 4 && xhr.status == 200) {

                        var resultado = xhr.responseText;
                        if (resultado != "null") {
                                                        alert(resultado);
                            var cadena = resultado.split("_");
                            document.getElementById('encabezadoModal').innerHTML = cadena[1];
                            $("#tablaxinformacionDetallada").append("" +
                                "<tbody>" +
                                    "<tr hidden><td><input type = 'text' id = 'id_id' value = '"+cadena[0]+"' /></td></tr>" +
                                    "<tr>" +
                                        "<td>" +
                                            "<label for='nombre'>Nombre Completo:</label>" +
                                            "<font color=red ><label id='lNombre' class='asterisco'> *</label></font>" +
                                            "<input type='text' name='nombre' id='nombre' size='30' maxlength='30' value = '"+cadena[1]+"' placeholder='Michael Andres Salas Granados' onkeypress=return AceptarLetrasNumeros(event) disabled/>" +
                                        "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td>" +
                                            "<label for='telefono'>Tel&eacute;fono:</label>" +
                                            "<input type='text' name='telefono' id='telefono' size='10' maxlength='11' value= '"+cadena[2]+"' onkeypress=return validacionNumerico(event) placeholder='88-77-66-55' disabled/>" +
                                        "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td>" +
                                            "<label for='detalle'>Detalle:</label>" +
                                            "<textarea id='detalle' rows='5' cols='52' maxlength='300' onkeypress=return AceptarLetrasNumerosComa(event) placeholder='Escribe aqu&iacute; porqu&eacute; quieres ser voluntario' disabled>"+cadena[3]+"</textarea>" +
                                        "</td>" +
                                    "</tr>" +
                                "</tbody>"
                            );
                            $("#telefono").mask("9999-9999");  
                        }
                    }
                }                
            }            
        break;			
	}

}

//LOS CAMBIOS GENERADOS SON ACEPTADOS ACORDE A LA SECCION ELEGIDA
//LOS CAMBIOS DE ELIMINAR, ACTUALIZAR Y REGISTRAR
function aceptarDatos(){

    if(guardarArea == 7){
        switch(document.getElementById('aceptarDatos').value){
            case document.getElementById('aceptarDatos').value = "Cerrar":
                cancelarDato();
                break;
        }
    }
    if(guardarArea == 5){
        switch(document.getElementById('aceptarDatos').value){
            case document.getElementById('aceptarDatos').value = "Actualizar":
                actualizarVoluntario();
                break;
            case document.getElementById('aceptarDatos').value = "Eliminar":
                eliminarVoluntario();
                break;
            case document.getElementById('aceptarDatos').value = "Registrar":
                registrarVoluntario();
                break;
        }
    }    
}

//DESTRUYE EL TBODY PARA LIMPIAR TODO EL MODAL, INDISTINTAMENTE SEA DE REGISTRAR, ACTUALIZAR O ELIMINAR
function cancelarDato(){
    $("#tablaxinformacionDetallada TBODY").remove();
    document.getElementById('myModal').style.top = "-100vh";
    document.getElementById('telefonoExtra').style.display = 'none';
}

function cargarSiguienteInformacion(){
	document.getElementById('buscar').value = "";
	document.getElementById('buscar').style.color = "black";
	var temp = calcularPaginaparaRegistros();
	temp = parseInt(temp)+1;
	history.pushState(null, "", "../CPresentacion/ventanaAdminPrepResp.php?pagina= " + temp);
	segundaSolicitudInformacion(temp,true);

}
function cargarPreviaInformacion(){
	document.getElementById('buscar').style.color = "black";
	document.getElementById('buscar').value = "";
	var temp = calcularPaginaparaRegistros();
	temp = parseInt(temp)-1;
	history.pushState(null, "", "../CPresentacion/ventanaAdminPrepResp.php?pagina= " + temp);
	segundaSolicitudInformacion(temp,false);
}

//SURGE A RAIZ DE EVITAR UN REFRESCAMIENTO DE TODA LA PAGINA
//CARGANDO SOLO LA TABLA CON LA PAGINACION CORRESPONDIENTE
//ADEMAS DE VERIFICAR SI YA ESTA EN ALGUN LIMITE DE PAGINA
function segundaSolicitudInformacion(pagina,avanzo,retrocedio){

	if(guardarArea == 7){
		verificarLimitePaginacion("../CNegocio/controladoraDirectorio.php",avanzo,retrocedio);
		mostrarIntegrantes(pagina);
	}
	if(guardarArea == 5){
		verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",avanzo,retrocedio);
		mostrarIntegrantes(pagina);
	}
}

//CARGA EL MODAL PARA ACTUALIZAR
function Seleccionar(txt){

    if(guardarArea == 7){
        document.getElementById('myModal').style.top = "0px";
        document.getElementById('aceptarDatos').value = "Cerrar";
        document.getElementById('cancelarDatos').style.display = "none";
        cargaModalGlobal(1,txt);        
    }
    if(guardarArea == 5){
        document.getElementById('myModal').style.top = "0px";
        document.getElementById('aceptarDatos').value = "Actualizar";
        document.getElementById('cancelarDatos').style.display = "block";
        cargaModalGlobal(1,txt);     	
    }

}

function ConfirmarEliminacion(id) {

    if(guardarArea == 5){
        document.getElementById("myModal").style.top = "0px";
        document.getElementById('aceptarDatos').value = "Eliminar";
        //document.getElementById('cancelarDatos').style.display = "block";
        cargaModalGlobal(3,id);            
    }
}

function verVentanaConfirmacion(opcion){
    if(opcion == "registro"){
        document.getElementById('modalConfirmacion').style.top= "0px";
        document.getElementById('confirmacion').innerHTML = "¡Registro Exitoso!";
        var modal = document.getElementById('modalConfirmacion');

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("closeConfirmacion")[0];
        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.top = "-120vh"; 
            if(guardarArea == 5){
                crearPaginacion("../CNegocio/controladoraVoluntario.php");
                verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",false);   
            }
            mostrarIntegrantes(calcularPaginaparaRegistros());
            cancelarDato();
        }
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.top = "-120vh"; 
                if(guardarArea == 5){
                    crearPaginacion("../CNegocio/controladoraVoluntario.php");
                    verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",false);   
                }
                mostrarIntegrantes(calcularPaginaparaRegistros());
                cancelarDato();
            }
        }
    }
    else if(opcion == "registroError"){
        document.getElementById('modalConfirmacion').style.top= "0px";
        document.getElementById('confirmacion').style.backgroundColor = "#f44336";
        document.getElementById('botonConfirmacion').style.background = "#f44336";
        document.getElementById('confirmacion').innerHTML = "¡Problema de Registro!";
        var modal = document.getElementById('modalConfirmacion');

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("closeConfirmacion")[0];
        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.top = "-120vh"; 
            cancelarDato();
        }
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.top = "-120vh"; 
                cancelarDato();
            }
        }
    }
    else if(opcion == "actualizar"){
        document.getElementById('modalConfirmacion').style.top= "0px";
        document.getElementById('confirmacion').innerHTML = "¡Actualización Exitosa!";
        var modal = document.getElementById('modalConfirmacion');

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("closeConfirmacion")[0];
        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.top = "-120vh"; 
            if(guardarArea == 5){
                crearPaginacion("../CNegocio/controladoraVoluntario.php");
                verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",false);   
            }         
            mostrarIntegrantes(calcularPaginaparaRegistros());
            cancelarDato();
        }
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.top = "-120vh";          
                if(guardarArea == 5){
                    crearPaginacion("../CNegocio/controladoraVoluntario.php");
                    verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",false);   
                }
                mostrarIntegrantes(calcularPaginaparaRegistros());
                cancelarDato();      
            }
        }
    }
    else if(opcion == "actualizarError"){
        document.getElementById('modalConfirmacion').style.top= "0px";
        document.getElementById('confirmacion').style.backgroundColor = "#f44336";
        document.getElementById('botonConfirmacion').style.background = "#f44336";
        document.getElementById('confirmacion').innerHTML = "¡Problema de Actualización!";
        var modal = document.getElementById('modalConfirmacion');

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("closeConfirmacion")[0];
        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.top = "-120vh"; 
            cancelarDato();
        }
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.top = "-120vh"; 
                cancelarDato();
            }
        }
    }
    else if(opcion == "actualizarEspacios"){
        document.getElementById('modalConfirmacion').style.top= "0px";
        document.getElementById('confirmacion').style.backgroundColor = "#f44336";
        document.getElementById('botonConfirmacion').style.background = "#f44336";
        document.getElementById('confirmacion').innerHTML = "¡NO deje espacios en blanco!";
        var modal = document.getElementById('modalConfirmacion');

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("closeConfirmacion")[0];
        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.top = "-120vh"; 
            cancelarDato();
        }
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.top = "-120vh"; 
                cancelarDato();
            }
        }       
    }
    else if(opcion == "eliminar"){
        document.getElementById('modalConfirmacion').style.top= "0px";
        document.getElementById('confirmacion').innerHTML = "¡Registro Eliminado!";
        var modal = document.getElementById('modalConfirmacion');

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("closeConfirmacion")[0];
        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            if(guardarArea == 5){
                crearPaginacion("../CNegocio/controladoraVoluntario.php");
                verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",false);   
            }
            mostrarIntegrantes(calcularPaginaparaRegistros());
            history.pushState(null, "", "../CPresentacion/ventanaAdminPlanInfor.php?pagina= 1");
            cancelarDato();            
        }
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.top = "-120vh"; 
                mostrarIntegrantes(calcularPaginaparaRegistros());
                if(guardarArea == 5){
                    crearPaginacion("../CNegocio/controladoraVoluntario.php");
                    verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",false);   
                }
                cancelarDato();
            }
        }       
    }
}

function botonConfirmacion(){

    document.getElementById('modalConfirmacion').style.top = "-100vh";

    if(guardarArea == 5){
        crearPaginacion("../CNegocio/controladoraVoluntario.php");
        mostrarIntegrantes(calcularPaginaparaRegistros());
        verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",false);
    }
    cancelarDato();
}

function detectarTeclaEnter_enBusqueda(e){

    var evt = e ? e : event;
    var key = window.Event ? evt.which : evt.keyCode;

    if(key == 13){

        busquedaEspecifica();
    }

}

function busquedaEspecifica(){
    var buscar = document.getElementById('buscar').value;
    var tabla = document.getElementById('tabla');
    var td = "";
    var tr = "";
    var temp = "";
    var dato = "Sin resultado";
    var i = 1; 
    var puerta = false;
    var area = "";
    var busquedaEspecifica = "";

    if(guardarArea == 7){
        area = "../CNegocio/controladoraDirectorio.php";
        busquedaEspecifica = "buscarIntegranteEspecifico";
    }
    if(guardarArea == 5){
    	area = "../CNegocio/controladoraVoluntario.php";
    	busquedaEspecifica = "buscarIntegranteEspecifico";
    }
    if(buscar != ""){
        while(i < tabla.rows.length){
            tr = tabla.rows[i];
            for(var j = 1; j < tr.cells.length-1; j++){
                td = tr.cells[j].innerHTML;
                if(buscar.toUpperCase().includes(td.toUpperCase()) && td != ""){
                    if(tr.cells[0].innerHTML.includes(0))
                        temp = tr.cells[1].innerHTML + ",";
                    if(!tr.cells[0].innerHTML.includes(0))
                        temp = tr.cells[0].innerHTML + ",";

                    if(!puerta){
                        puerta = true;
                        dato = "";
                    }
                    if(!dato.includes(temp)){
                        dato += temp;
                    }                    
                }
            }
            i++;
        }
        if(dato != "Sin resultado"){
            for(var i = 1; i < tabla.rows.length; i++){                
                if(dato.includes(tabla.rows[i].cells[0].innerHTML) || dato.includes(tabla.rows[i].cells[1].innerHTML)){
                    tabla.rows[i].style.display = "";
                }
                else{
                    tabla.rows[i].style.display = "none";
                }
            }
        }
        else{
            if(dato == "Sin resultado"){

                buscar_en_baseD(buscar, area, busquedaEspecifica);

            }
            else
                resultadoBusqueda(dato);
        }
        
    }
    if(buscar == ""){
        resultadoBusqueda(buscar);
    }

}

function buscar_en_baseD(variable, area, busquedaEspecifica){
    var xhr = new XMLHttpRequest();
    xhr.open("POST", area);
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send(busquedaEspecifica+"=" + variable);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;
            if (resultado != 0) {
            	switch(guardarArea){
            		case guardarArea = 7:
		                var datos = resultado.split(",");
		                $("tr").remove(".cla");
		                $("#tablaDirectorio").append(""+
		                	"<tr class = 'cla'>"+
			                	"<td hidden>" +  datos[1]  + " </td>" +
			                	"<td>" + datos[2] + "</td>" +
			                	"<td>" + datos[3] + "</td>" +
			                	"<td>" + datos[4] + "</td>" +
			                	"<td>" + datos[5] + "</td>" +
			                	"<td>" + datos[9] + "</td>" +
			                	"<td>" + datos[6] + "</td>" +
			                	"<td>" + datos[8] + "</td>" +
			                	"<td>" + "<BUTTON ID = 'actualizar' NAME = 'actualizar' ONCLICK = 'verDetalle("+datos[0]+")'><i class='far fa-edit'></i></BUTTON>"+ 
				                	"<BUTTON ID = 'eliminar' NAME = 'eliminar' ONCLICK = 'verDetalleEliminar("+ datos[0] + ")'><i class='fas fa-trash-alt'></i></BUTTON>"+
				                	"<INPUT TYPE ='TEXT' NAME = 'idAjax' ID = 'idAjax' VALUE = "+ datos[0] +" MAXLENGTH = '1' SIZE = '1' DISABLED HIDDEN/>" + "</td>" +
		                	"</tr>"
		                	);
            		break;

            		case guardarArea = 5:
                        $("tr").remove(".cla"); 
                        $("#tablaDirectorio").append(resultado);
            		break;
            	}
            }
            else{
                resultadoBusqueda("Sin resultado");
            }
        }

    }

}

//VERIFICA DEL CAMPO DE BUSQUEDA SI ESTA VACIO EN DEJARLO LIMPIO
function campoVacio(){
    if(document.getElementById('buscar').value == ""){
        resultadoBusqueda("");
    }   
}

//SI OBTIENEN RESULTADOS NEGATIVOS DE LA BUSQUEDA LO REMARCA DE ROJO EL CAMPO, DANDO
//A ENTENDER QUE NO EXISTE
function resultadoBusqueda(texto){  
    if(texto == "Sin resultado"){
        document.getElementById('buscar').value = "Sin resultados";
        document.getElementById('buscar').style.color = "red";
    }
    else if(document.getElementById('buscar').value == ""){
        document.getElementById('buscar').style.color = "black";
        mostrarIntegrantes();
    }
}

//VERIFICA SI YA SE ENCUENTRA UN LIMITE DE PAGINA PARA INHABILITAR EL SIGUIENTE O ANTERIOR
function verificarPagina(paginaLimite,avanza){
    if(window.location.search == ""){
        document.getElementById('primeraPagina').style.pointerEvents = "none";
		history.pushState(null, "", "../CPresentacion/ventanaAdminPrepResp.php?pagina= 1");
    }
    if(window.location.search.includes(paginaLimite)){
        document.getElementById('siguientePagina').style.pointerEvents = "none";
    }
    if(window.location.search.includes(1)){
        document.getElementById('primeraPagina').style.pointerEvents = "none";
    }
    if(avanza)
    	document.getElementById('primeraPagina').style.pointerEvents = "auto";
    if(!avanza)
        document.getElementById('siguientePagina').style.pointerEvents = "auto";
}   

function verificarLimitePaginacion(area,avanza){
	
    var xhr=new XMLHttpRequest();
    xhr.open("POST", area);
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("contarFilas=true");

    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4 && xhr.status == 200){
            resultado = xhr.responseText;
            resultado = resultado/4;
            resultado = Math.ceil(resultado)-1;
            verificarPagina(resultado,avanza);

        }
    }
}

function crearPaginacion(area){
    $("#pagination").empty();

    var resultado = 0;
    var xhr=new XMLHttpRequest();
    xhr.open("POST", area);
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("contarFilas=true");

    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4 && xhr.status == 200){
            resultado = xhr.responseText;
            resultado = resultado/4;
            resultado = Math.ceil(resultado)-1;
            for(var i = 0; i < resultado; i++){
                var listNode = document.getElementById('pagination'),
                    liNode = document.createElement("LI"),
                    txtNode = document.createTextNode(i);
                var href = document.createElement("a");
                href.textContent = i+1;
                href.setAttribute('href',"../CPresentacion/ventanaAdminPrepResp.php?pagina= "+(i+1));
                liNode.appendChild(href);
                listNode.appendChild(liNode);

            }
            verificarPagina(resultado);
        }
    }

}

function calcularPaginaparaRegistros(){
    var url = window.location.search;
    var puertaRegistro = false;
    var numUrl = 0;
    if(url.length == 12){
        for(var i = 0; i < url.length; i++){
            if(puertaRegistro){
                if(url[i] == "%"){

                }
                else if(i >= 11){
                    numUrl += url[i];           
                }
            }
            if(url[i] == "="){
                puertaRegistro = true;
            }
        }
    }
    else if(url.length == 9){
        for(var i = 0; i < url.length; i++){
            if(puertaRegistro){
                numUrl += url[i];           
            }
            if(url[i] == "="){
                puertaRegistro = true;
            }
        }
    }
    return  numUrl;

}




//