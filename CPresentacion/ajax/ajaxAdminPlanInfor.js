window.onload = function(event){

	document.getElementById('tituloCabecera').innerHTML += "Administraci&oacute;n de Planificaci&oacute;n e Informaci&oacute;n";
	mostrarTituloAdministrador();
    document.getElementById('admins2').style.backgroundColor = "#ddd";
    document.getElementById('admins2').style.color = "black";
    document.getElementById('admins').style.display = "block";
    document.getElementById('admins2').style.display = "block";
    document.getElementById('admins3').style.display = "block";
}

$(window).resize(function(){
    medirResolucion()
    if($(window).width() <= 580){
        $("#tabla THEAD").remove();
        $("#tabla").append(""+
            "<THEAD>"+
                    "<TR>" +
                        "<TH>Integrante</TH>"+
                    "</TR>" +
            "</THEAD><TR>"
            );
    }
    else{
        recargarTitulosTabla();
    }
});

function medirResolucion(){
    if($(window).width() <= 939){
        document.getElementById('label_sign-in').style.width = "30px";
    }
    if($(window).width() >959){
        document.getElementById('label_sign-in').style.width = "118px";
    }
    if($(window).width() > 907){
        document.getElementById('label_sign-in').style.width = "118px";
    }
    if($(window).width() <= 814){
        document.getElementById('label_sign-in').style.fontSize = "10px";
    }
    if($(window).width() > 814){
        document.getElementById('label_sign-in').style.fontSize = "medium";
    }
    if($(window).width() <= 516){
        document.getElementById('label_sign-in').innerHTML = "";
    }
    else{
        document.getElementById("label_sign-in").innerHTML = "Iniciar Sesión";
    }
}

function mostrarTituloAdministrador(){

	document.getElementById('encargado').innerHTML += "Dennis Muñoz";
}

var guardarArea = 0;
function desplegarInformacion(valor){
    
    limpiarColores();
	
    document.getElementById('buscar').value = "";
    
    history.pushState(null, "", "../CPresentacion/ventanaAdminPlanInfor.php?pagina= 1");

    if(valor == '1'){
    	informacionAlbergue();
        document.getElementById('albergue').style.color = "black";
        document.getElementById('albergue').style.backgroundColor = "#ddd";
    }
	   	
    if(valor == '2')    		
    	informacionAnuncios();
	   	
    if(valor == '3'){
    	informacionDirectorio();
        document.getElementById('directorio').style.color = "black";
        document.getElementById('directorio').style.backgroundColor = "#ddd";
    }
        	
    if(valor == '4'){
    	informacionVoluntarios();
        document.getElementById('voluntariado').style.backgroundColor = "#ddd";
        document.getElementById('voluntariado').style.color = "black";
    }

    if(valor == "7"){
        informacionBodega();
        document.getElementById('bodega').style.backgroundColor = "#ddd";
        document.getElementById('bodega').style.color = "black";
    }

    if(valor == "9"){
        informacionZonasRiesgo();
        document.getElementById('zonasRiesgo').style.backgroundColor = "#ddd";
        document.getElementById('zonasRiesgo').style.color = "black";        
    }
    
    
}

function limpiarColores(){
    document.getElementById('directorio').style.color = "";    
    document.getElementById('voluntariado').style.color = "";
    document.getElementById('bodega').style.color = "";
    document.getElementById('albergue').style.color = "";

    document.getElementById('albergue').style.backgroundColor = "";
    document.getElementById('bodega').style.backgroundColor = "";
    document.getElementById('directorio').style.backgroundColor = "";
    document.getElementById('voluntariado').style.backgroundColor = "";
}
function recargarTitulosTabla(){
    if(guardarArea == 1){
        $("#tabla THEAD").remove();
        $("#tabla").append(""+
            "<THEAD>"+
                    "<TR>" +
                        "<TH>Fila</TH>"+
                        "<TH>Nombre:</TH>"+
                        "<TH>Capacidad:</TH>"+
                        "<TH>Encargado:</TH>"+
                        "<TH>Localidad:</TH>"+
                        "<TH>Tel&eacute;fono:</TH>"+
                        "<TH>Luz:</TH>"+
                        "<TH>Agua:</TH>"+
                        "<TH>Cuartos:</TH>"+
                        "<TH>Cocina:</TH>"+
                        "<TH>Baños:</TH>"+
                        "<TH>Opciones:</TH>"+
                    "</TR>" +
            "</THEAD><TR>"
        );        
    }
    if(guardarArea == 3){
        $("#tabla THEAD").remove();
        $("#tabla").append(""+
        "<THEAD>"+
                "<TR>" +
                    "<TH>Nombre</TH>"+
                    "<TH>Primer Apellido:</TH>"+
                    "<TH>Segundo Apellido:</TH>"+
                    "<TH>Tel&eacute;fono:</TH>"+
                    "<TH>Correo electr&oacute;nico:</TH>"+
                    "<TH>Instituci&oacute;n Representada:</TH>"+
                    "<TH>Comit&eacute;</TH>" +
                    "<TH>Opciones</TH>"+
                "</TR>" +
        "</THEAD><TR>"
        );
    }
    if(guardarArea == 4){
        $("#tabla THEAD").remove();
        $("#tabla").append(""+
            "<THEAD>"+
                    "<TR>" +
                        "<TH>Fila</TH>"+
                        "<TH>Nombre:</TH>"+
                        "<TH>Tel&eacute;fono:</TH>"+
                        "<TH>Detalle:</TH>"+
                        "<TH>Opciones</TH>"+
                    "</TR>" +
            "</THEAD><TR>"
        );
    }
    if(guardarArea == 7){
        $("#tabla THEAD").remove();
        $("#tabla").append(""+
            "<THEAD>"+
                    "<TR>" +
                        "<TH>Fila</TH>"+
                        "<TH>Nombre:</TH>"+
                        "<TH>Descripción:</TH>"+
                        "<TH>Dirección:</TH>"+
                        "<TH>Responsable:</TH>"+
                    "</TR>" +
            "</THEAD><TR>"
        );
    }

    if(guardarArea == 9){
        $("#tabla THEAD").remove();
        $("#tabla").append(""+
            "<THEAD>"+
                    "<TR>" +
                        "<TH>Fila</TH>"+
                        "<TH>Lugar:</TH>"+
                        "<TH>Tipo de Riesgo:</TH>"+
                        "<TH>Albergue más cercano:</TH>"+
                        "<TH>Opción:</TH>"+
                    "</TR>" +
            "</THEAD><TR>"
        );
    }
}

//METODOS DE CARGA DE INFORMACION

function informacionAlbergue(){
    //VARIABLE QUE PERMITE LE INGRESO Y LA CARGA DE INFORMACION DE ACUERDO AL AREA
    guardarArea = 1;
    //MODIFICACION DE CAMPOS GENERALES Y APARICION
    document.getElementById('tituloCabecera').innerHTML = "Secci&oacute;n de Albergues";
    document.getElementById('buscar').placeholder = "Buscar Albergue";
    document.getElementById('buscar').title = "Busque por: nombre, encargado, capacidad";
    document.getElementById('buscar').style.display = "block";
    document.getElementById('opcionesMenu').style.display = "block";
    document.getElementById('btn_registrar').style.display = "none";
    document.getElementById('btn_registrar').disabled = true;
    document.getElementById('tabla').style.display = "block";
    document.getElementById('paginacion').style.display = "block";
    //LLAMADA DE METODOS
    crearPaginacion("../CNegocio/controladoraAlbergues.php");
    mostrarResultadosdeTablas(calcularPaginaparaRegistros());
    //ELIMINA EL CONTENIDO DE LA TABLA PARA HACER LA CARGA RESPECTIVA
    $("#tabla THEAD").remove();
    $("#tablaDirectorio tr").remove();
    $("#tabla").append(""+
        "<THEAD>"+
            "<TR>" +
                "<TH>Fila</TH>"+
                "<TH>Nombre:</TH>"+
                "<TH>Capacidad:</TH>"+
                "<TH>Encargado:</TH>"+
                "<TH>Localidad:</TH>"+
                "<TH>Tel&eacute;fono:</TH>"+
                "<TH>Luz:</TH>"+
                "<TH>Agua:</TH>"+
                "<TH>Cuartos:</TH>"+
                "<TH>Cocina:</TH>"+
                "<TH>Baños:</TH>"+
            "</TR>" +
        "</THEAD><TR>"
    );    
    verificarLimitePaginacion("../CNegocio/controladoraAlbergues.php",false);
    document.getElementById('pagination').style.backgroundColor = "white";

}
function informacionAlertas(){
	document.getElementById('tituloCabecera').innerHTML = "Secci&oacute;n de Alertas";
	document.getElementById('opcionesMenu').style.display = "block";
}
function informacionAnuncios(){
    document.getElementById('tituloCabecera').innerHTML = "Secci&oacute;n de Anuncios";
    document.getElementById('opcionesMenu').style.display = "block";
}

function informacionDirectorio(){
    //VARIABLE QUE PERMITE LE INGRESO Y LA CARGA DE INFORMACION DE ACUERDO AL AREA
    guardarArea = 3;
    //MODIFICACION DE CAMPOS GENERALES Y APARICION
    document.getElementById('tituloCabecera').innerHTML = "Secci&oacute;n de Directorio";
    document.getElementById('buscar').placeholder = "Buscar integrante";
    document.getElementById('buscar').title = "Busque por: Nombre, Apellido, Instituci&oacute;n es lo que busca";
    document.getElementById('buscar').style.display = "block";
    document.getElementById('opcionesMenu').style.display = "block";
    document.getElementById('btn_registrar').style.display = "block";
    document.getElementById('btn_registrar').value = "Registrar Integrante";
    document.getElementById('tabla').style.display = "block";
    document.getElementById('paginacion').style.display = "block";
    //LLAMADA DE METODOS
    crearPaginacion("../CNegocio/controladoraDirectorio.php");
    mostrarResultadosdeTablas(calcularPaginaparaRegistros());
    //ELIMINA EL CONTENIDO DE LA TABLA PARA HACER LA CARGA RESPECTIVA
    $("#tabla THEAD").remove();
    $("#tablaDirectorio tr").remove();
    $("#tabla").append(""+
        "<THEAD>"+
                "<TR>" +
                    "<TH>Nombre</TH>"+
                    "<TH>Primer Apellido:</TH>"+
                    "<TH>Segundo Apellido:</TH>"+
                    "<TH>Tel&eacute;fono:</TH>"+
                    "<TH>Correo electr&oacute;nico:</TH>"+
                    "<TH>Instituci&oacute;n Representada:</TH>"+
                    "<TH>Comit&eacute;</TH>" +
                    "<TH>Opciones</TH>"+
                "</TR>" +
        "</THEAD><TR>"
        );
    verificarLimitePaginacion("../CNegocio/controladoraDirectorio.php",false);
    document.getElementById('pagination').style.backgroundColor = "white";

}

function informacionVoluntarios(){
    //VARIABLE QUE PERMITE LE INGRESO Y LA CARGA DE INFORMACION DE ACUERDO AL AREA
    guardarArea = 4;
    //MODIFICACION DE CAMPOS GENERALES Y APARICION
    document.getElementById('tituloCabecera').innerHTML = "Secci&oacute;n de Voluntarios";
    document.getElementById('opcionesMenu').style.display = "block";
    document.getElementById('buscar').title = "Busque por: Nombre, Apellido es lo que busca";
    document.getElementById('buscar').style.display = "block";
    document.getElementById('buscar').placeholder = "Buscar voluntario";
    document.getElementById('opcionesMenu').style.display = "block";
    document.getElementById('btn_registrar').style.display = "block";
    document.getElementById('btn_registrar').value = "Registrar Voluntario";
    document.getElementById('tabla').style.display = "block";
    document.getElementById('paginacion').style.display = "block";
    crearPaginacion("../CNegocio/controladoraVoluntario.php");
    //LLAMADA DE METODOS
    mostrarResultadosdeTablas(calcularPaginaparaRegistros());
    //ELIMINA EL CONTENIDO DE LA TABLA PARA HACER LA CARGA RESPECTIVA
    $("#tabla THEAD").remove();
    $("#tablaDirectorio tr").remove();
    $("#tabla").append(""+
        "<THEAD>"+
                "<TR>" +
                    "<TH>Fila</TH>"+
                    "<TH>Nombre:</TH>"+
                    "<TH>Tel&eacute;fono:</TH>"+
                    "<TH>Detalle:</TH>"+
                    "<TH>Opciones</TH>"+
                "</TR>" +
        "</THEAD><TR>"
    );
    verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",false);
}

function informacionBodega(){
    //VARIABLE QUE PERMITE LE INGRESO Y LA CARGA DE INFORMACION DE ACUERDO AL AREA
    guardarArea = 7;
    //MODIFICACION DE CAMPOS GENERALES Y APARICION
    document.getElementById('tituloCabecera').innerHTML = "Secci&oacute;n de Bodega";
    document.getElementById('buscar').placeholder = "Buscar bodega";
    document.getElementById('buscar').title = "Busque por: Nombre, Dirección o Responsable";
    document.getElementById('buscar').style.display = "block";
    document.getElementById('opcionesMenu').style.display = "block";
    document.getElementById('btn_registrar').style.display = "none";
    document.getElementById('btn_registrar').disabled = true;
    document.getElementById('tabla').style.display = "block";
    document.getElementById('paginacion').style.display = "block";
    //LLAMADA DE METODOS
    crearPaginacion("../CNegocio/controladoraBodega.php");
    mostrarResultadosdeTablas(calcularPaginaparaRegistros());
    //ELIMINA EL CONTENIDO DE LA TABLA PARA HACER LA CARGA RESPECTIVA
    $("#tabla THEAD").remove();
    $("#tablaDirectorio tr").remove();
    $("#tabla").append(""+
        "<THEAD>"+
                "<TR>" +
                    "<TH>Fila</TH>"+
                    "<TH>Nombre:</TH>"+
                    "<TH>Descripción:</TH>"+
                    "<TH>Dirección:</TH>"+
                    "<TH>Responsable:</TH>"+
                "</TR>" +
        "</THEAD><TR>"
    );
    verificarLimitePaginacion("../CNegocio/controladoraBodega.php",false);
    document.getElementById('pagination').style.backgroundColor = "white";
}

function informacionZonasRiesgo(){
    //VARIABLE QUE PERMITE LE INGRESO Y LA CARGA DE INFORMACION DE ACUERDO AL AREA
    guardarArea = 9;
    //MODIFICACION DE CAMPOS GENERALES Y APARICION
    document.getElementById('tituloCabecera').innerHTML = "Secci&oacute;n de Zonas de Riesgo";
    document.getElementById('buscar').placeholder = "Buscar Zona de Riesgo";
    document.getElementById('buscar').title = "Busque por: Lugar, Tipo de riesgo";
    document.getElementById('buscar').style.display = "block";
    document.getElementById('opcionesMenu').style.display = "block";
    document.getElementById('btn_registrar').style.display = "none";
    document.getElementById('btn_registrar').disabled = true;
    document.getElementById('tabla').style.display = "block";
    document.getElementById('paginacion').style.display = "block";
    //LLAMADA DE METODOS
    crearPaginacion("../CNegocio/controladoraZonaRiesgo.php");
    mostrarResultadosdeTablas(calcularPaginaparaRegistros());
    //ELIMINA EL CONTENIDO DE LA TABLA PARA HACER LA CARGA RESPECTIVA
    $("#tabla THEAD").remove();
    $("#tablaDirectorio tr").remove();
    $("#tabla").append(""+
        "<THEAD>"+
                "<TR>" +
                    "<TH>Fila</TH>"+
                    "<TH>Lugar:</TH>"+
                    "<TH>Tipo de Riesgo:</TH>"+
                    "<TH>Albergue más cercano:</TH>"+
                    "<TH>Opción:</TH>"+
                "</TR>" +
        "</THEAD><TR>"
    );
    verificarLimitePaginacion("../CNegocio/controladoraZonaRiesgo.php",false);
    document.getElementById('pagination').style.backgroundColor = "white";

}

//METODOS DE ZONAS DE RIESGO

function Mostrar(id) {

    $("#tabla").append("",
        "<div id='div_mapa'>" +
            "<table id='tabla_mapa'>" +

            "</table>" +
            "<div id='mapa'></div>" +
        "</div>" 
    );
    $("#tablaDirectorio").hide();

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraZonaRiesgo.php");
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("seleccionar=" + id);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {

                //alert(resultado);
                var cadena = resultado.split("_");
                $("tr").remove(".cla");
                $("#tabla_mapa").append("<tr class='cla'>" +
                    "<td></td>" +
                    "<td>" + cadena[1] + "</td>" +
                    "<td>" + cadena[2] + "</td>" +
                    "<td>" + cadena[3] + "</td>" +
                    "<td><button id='btn_regresar' onclick='recargarTablaZonadeRiesgo()'><i class='fas fa-undo'></i></button></td>" +
                    "</tr>"
                );
                var imgURL = "https://maps.googleapis.com/maps/api/staticmap?center="+cadena[4]+","+cadena[5]+"&size=900x900&markers=color:red%7C"+cadena[4]+","+cadena[5]+"&key=AIzaSyDNZCK5NCo8SO8f14wMWGAn2W2oT6nSGcE";
                document.getElementById('mapa').innerHTML ="<img src='"+imgURL+"'>";
            }
        }
    }
}

function recargarTablaZonadeRiesgo(){
    $("#tabla THEAD").remove();
    $("#tabla div").remove();
    $("#tabla").append(""+
        "<THEAD>"+
                "<TR>" +
                    "<TH>Fila</TH>"+
                    "<TH>Lugar:</TH>"+
                    "<TH>Tipo de Riesgo:</TH>"+
                    "<TH>Albergue más cercano:</TH>"+
                    "<TH>Opción:</TH>"+
                "</TR>" +
        "</THEAD><TR>"
    );    
    $("#tablaDirectorio").show();
    mostrarResultadosdeTablas(calcularPaginaparaRegistros());    
}
//FIN DE METODOS DE ZONAS DE RIESGO



//METODOS DEL AREA DE VOLUNTARIOS

function Mas(i){
    if (document.getElementById(i).style.display == "none" || document.getElementById(i).style.display == "") {

        document.getElementById(i).style.display = "block";
        document.getElementById("btn" + i).value = "Menos...";
    } else {
        document.getElementById(i).style.display = "none";
        document.getElementById("btn" + i).value = "Mas...";
    }
    
    //alert(i);
}

function registrarVoluntario() { // metodo que registra una nuevo voluntario.

    // Obteniendo los valores de los inputs.
    var nombre = document.getElementById("nombre").value;

    // Se puede continuar si solo si, el nombre se ingreso.
    if (nombre != "") {

        // Creando el STRING para enviar a la controladora.
        var enviar = "registrar=true" + "&nombre=" + nombre + "&telefono=" + document.getElementById("telefono").value + "&detalle=" + document.getElementById("detalle").value + " ";

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(enviar);

        xhr.onreadystatechange = function () {

            if (xhr.readyState == 4 && xhr.status == 200) {

                var resultado = xhr.responseText;

                //alert(resultado);
                if (resultado != 0) {
                    $("#modal").modal("hide");
                    setTimeout("verVentanaConfirmacion('registro')",500);

                } else { // Error !!!.

                    setTimeout("verVentanaConfirmacion('registroError')",500);
                }
            }
        }
    } 
}

function eliminarVoluntario() { // Metodo que elimina un voluntario.

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("eliminar=" + document.getElementById("id_id").value);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {

                //alert(resultado);
                if (resultado == 1) { // Exitoso.
                    $("#modal").modal("hide");
                    setTimeout("verVentanaConfirmacion('eliminar')",300);
                }
            } 
        }
    }
}

function actualizarVoluntario(){

    var nombre = document.getElementById("nombre").value;
    var telefono = document.getElementById('telefono').value.split("-")[0] + "" + document.getElementById('telefono').value.split("-")[1];
    if (nombre != "") {
        var enviar = "actualizar=true" + "&id=" + document.getElementById("id_id").value + "&nombre=" + nombre + "&telefono=" + telefono + "&detalle=" + document.getElementById("detalle").value + " ";

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(enviar);

        xhr.onreadystatechange = function () {

            if (xhr.readyState == 4 && xhr.status == 200) {

                var resultado = xhr.responseText;

                //alert(resultado);
                if (resultado != 0) {
                    $("#modal").modal("hide");
                    setTimeout("verVentanaConfirmacion('actualizar')",300);

                } else {

                    setTimeout("verVentanaConfirmacion('actualizarError')",300);
                }
            }
        }
    } 
}

function AceptarLetrasNumeros(e) {

    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    // tecla = 8 -> es de retroceso.
    // tecla = 0 -> es de tabulador.
    // tecla = 32 -> es del espacio.
    if (tecla == 8 || tecla == 0 || tecla == 32) {

        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros y letras
    patron = /[A-Za-z0-9]/;
    tecla_final = String.fromCharCode(tecla);

    return patron.test(tecla_final);
}

function AceptarLetrasNumerosComa(e) {

    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    // tecla = 8 -> es de retroceso.
    // tecla = 0 -> es de tabulador.
    // tecla = 32 -> es del espacio.
    // tecla = 44 -> es la d ela ',' "coma".
    if (tecla == 8 || tecla == 0 || tecla == 44 || tecla == 32) {

        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros y letras
    patron = /[A-Za-z0-9]/;
    tecla_final = String.fromCharCode(tecla);

    return patron.test(tecla_final);
}

//FIN DE METODOS DEL AREA DE VOLUNTARIOS
//
//
//



/*METODOS DEL DIRECTORIO */

//PERMITE REGISTRAR NUEVOS INTEGRANTES AL DIRECTORIO
function registrarIntegrante(){
    if(validarCampos("comprobarCampo","") >= 6){
        var telefono = "";
        if(!puertaTelefono && document.getElementById('segundoTelefono').value != "" && document.getElementById('telefono').value.length == 9 && document.getElementById('segundoTelefono').value.length == 9){
            telefono = document.getElementById("telefono").value.split("-");
            telefono = telefono[0]+""+telefono[1]+"/";
            var temp = document.getElementById('segundoTelefono').value.split("-");         
            telefono = telefono + temp[0]+""+temp[1];
            var puertaRegistro = true;
        }
        else{
            telefono = document.getElementById("telefono").value.split("-");
            telefono = telefono[0]+""+telefono[1];
            if(document.getElementById('telefono').value.length == 9){
                var puertaRegistro = true;
            }
        }
        if(puertaRegistro){
            var cedula = document.getElementById("cedula").value;
            var nombre = document.getElementById("nombre").value;
            var primer_apellido = document.getElementById("primer_apellido").value;
            var segundo_apellido = document.getElementById("segundo_apellido").value;
            var institucionRepresentada = document.getElementById('institucionRepresentada').value;
            var seleccionarPuesto = document.getElementById('seleccionarPuesto').value;
            var correo = document.getElementById('correo').value;

            //EN CASO QUE NO SE INGRESE CEDULA, EL REGISTRO DE CEDULA SERA 0
            if(cedula == "")
                cedula = "0";
            //

            var enviar ="registrar=true" +
                       "&cedula=" + cedula +
                       "&nombre=" + nombre +
                       "&primer_apellido=" + primer_apellido +
                       "&segundo_apellido=" + segundo_apellido +
                       "&telefono=" + telefono +
                       "&institucionRepresentada=" + institucionRepresentada + 
                       "&seleccionarPuesto=" + seleccionarPuesto +
                       "&correo=" + correo; //envia los datos para registrar el integrante


            var xhr = new XMLHttpRequest();
            xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
            xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
            xhr.send(enviar);

            xhr.onreadystatechange = function () {

                if (xhr.readyState == 4 && xhr.status == 200) {

                    var resultado = xhr.responseText;
                    //alert(resultado);
                    if (resultado == 1) {
                        $("#modal").modal("hide");
                        setTimeout("verVentanaConfirmacion('registro')",500);

                    } 

                    else{
                        setTimeout("verVentanaConfirmacion('registroError')",500);
                       //alert("Error en la consulta" + resultado);

                    } // Fin else.
                } // Fin if.
            }// Fin onreadystatechange
        }
    }
}

//TOMA EL ID DE ACUERDO AL INTEGRANTE SELECCIONADO Y MODIFICA LOS CAMBIOS
function actualizarIntegrante(){
    var telefono = "";
    if(document.getElementById('segundoTelefono').value != "" && document.getElementById('segundoTelefono').value.length == 9){
        telefono = document.getElementById("telefono").value;
        if(telefono.includes("-")){
            telefono = telefono.split("-")[0]+""+telefono.split("-")[1];
        }
        var temp = document.getElementById('segundoTelefono').value;
        if(temp.includes("-")){
            temp = temp.split("-")[0]+""+temp.split("-")[1];
        }
        telefono = telefono+"/"+temp;
        var puertaActualizar = true;            
    }
    else{
        telefono = document.getElementById("telefono").value;
        if(document.getElementById('telefono').value.length == 9){
            telefono = telefono.split("-");
            if(document.getElementById('segundoTelefono').value != ""){
                var temp = document.getElementById('segundoTelefono').value;
                temp = temp.split("-")[0]+""+temp.split("-")[1];
                telefono = telefono[0]+""+"/"+temp.split("u")[0];
            }
            else
                telefono = telefono[0]+""+telefono[1];
            var puertaActualizar = true;
        }
    }
    if(puertaActualizar){
        var id = document.getElementById('id').value
        var cedula = document.getElementById("cedula").value;
        var nombre = document.getElementById("nombre").value;
        var primer_apellido = document.getElementById("primer_apellido").value;
        var segundo_apellido = document.getElementById("segundo_apellido").value;
        var institucionRepresentada = document.getElementById('institucionRepresentada').value;
        var seleccionarPuesto = document.getElementById('seleccionarPuesto').value;
        var comite = document.getElementById('comite').value;
        var correo = document.getElementById('correo').value;
        if(cedula == "")
            cedula = 0;
        var envio ="actualizar=true" + 
                    "&idActualizar=" +id +
                    "&cedulaActualizar=" + cedula +
                    "&nombreActualizar=" + nombre +
                    "&primer_apellidoActualizar=" + primer_apellido +
                    "&segundo_apellidoActualizar=" + segundo_apellido +
                    "&telefonoActualizar=" + telefono +
                    "&institucionRepresentadaActualizar=" + institucionRepresentada + 
                    "&seleccionarPuestoActualizar=" + seleccionarPuesto +
                    "&comite=" + comite +
                    "&correoActualizar=" + correo; //se envian los datos para actualizar el integrante
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
        xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");            
        xhr.send(envio);
        xhr.onreadystatechange = function(){

            if(xhr.readyState == 4 && xhr.status == 200){

                var resultado = xhr.responseText;
                if(resultado == 1){
                    $("#modal").modal("hide");
                    setTimeout("verVentanaConfirmacion('actualizar')",500);
                }
                else if(resultado == 2){
                    setTimeout("verVentanaConfirmacion('actualizarError')",500);
                    //alert("Error en actualizar");
                }
                else if(resultado == 0){
                    setTimeout("verVentanaConfirmacion('actualizarEspacios')",500);
                    //alert("Error de espacios");
                }
            }
        }   
    }   
}

//MISMO CASO DEL ANTERIOR PERO LO ELIMINA
function eliminarIntegrante(){
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("eliminar=" + document.getElementById('id').value);//Envia el id del integrante seleccionar para enviar a eliminar
    
    xhr.onreadystatechange = function(){

        if(xhr.readyState == 4 && xhr.status == 200){

            var resultado = xhr.responseText;
            if(resultado != 0){
                $("#modal").modal("hide");                
                setTimeout("verVentanaConfirmacion('eliminar')",300);
            }
            else{
                alert("Error al eliminar");
            }
        }
    }
}

//AL CARGAR UN MODAL DE ACTUALIZAR O ELIMINAR CARGA LOS COMITES EXISTENTES REGISTRADOS
function llenarOpcionesComite(valor){
	
	var xhr=new XMLHttpRequest();
	xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("buscarComite=true");

	xhr.onreadystatechange = function(){

		if(xhr.readyState == 4 && xhr.status == 200){

			var resultado = xhr.responseText;
			if(resultado != ""){
				var datos = resultado.split(',');
				$("#seleccionarComite").empty()
				$("#seleccionarComite").append(new Option("Seleccione una opción","",true,true));
				document.getElementById('seleccionarComite').options[0].disabled = true;
				for(var i = 0; i <= datos.length-1; i++){
					if(datos[i] != ""){
                        if(datos[i] == valor)
                            $("#seleccionarComite").append(new Option(datos[i],datos[i],true,true));
                        else
                            $("#seleccionarComite").append(new Option(datos[i],datos[i],true,false));
						$("#seleccionarComite OPTION[VALUE = 'Ninguno']").remove()
						$("#seleccionarComite OPTION[VALUE = 'Otro']").remove()
						$("#seleccionarComite").append(new Option("Otro","Otro",true,false));
                        if(valor == "Ninguno")
						  $("#seleccionarComite").append(new Option("Ninguno","Ninguno",true,true));
                        else
                            $("#seleccionarComite").append(new Option("Ninguno","Ninguno",true,false)); 
					}
				}			
			}	
		}
	}
}

//EN CASO DE USAR LA FUNCION DE OTRO AGREGAR EL NUEVO COMITE PARA SU REGISTRO
function agregarNuevoComite(){

	if(document.getElementById('seleccionarComite').value == "Otro"){
		var nuevoComite = prompt("Ingrese el nombre del comite","");
        if(nuevoComite != null){
            $("#seleccionarComite").append(new Option(nuevoComite,nuevoComite,true,true));
            $("#seleccionarComite OPTION[VALUE = 'Ninguno']").remove()
            $("#seleccionarComite OPTION[VALUE = 'Otro']").remove()
            $("#seleccionarComite").append(new Option("Otro","Otro",true,false));
            $("#seleccionarComite").append(new Option("Ninguno","Ninguno",true,false));
        }
        if(nuevoComite == null){
            document.getElementById('seleccionarComite').selectedIndex = 0;
        }
	}

}

//insertar un campo de texto mas para el telefono
var puertaTelefono = true;
function insertarTelefonoExtra(opcion){
	if(opcion == "registrar"){
		document.getElementById('telefonoExtraRegistrar').style.display = "block";
		puertaTelefono = false;
	}
	else{
		document.getElementById('telefonoExtra').style.display = "block";
		puertaTelefono = false;
	}

}
//validacion de números
function validacionNumerico(e){
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
        return true;
    }
        
    // Patron de entrada, en este caso solo acepta numeros
    patron =/[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    
    return patron.test(tecla_final);
}

//CUANDO SE CARGA LA INFORMACIÓN SI EL SUJETO SELECCIONADO CUENTA CON DOS NUMEROS
//ESTAN SEPARADOS POR / Y HABILITA EL OTRO CAMPO DE TELEFONO
function habilitarbotonSegundoTelefono(valor){
    if(valor.includes("/")){
        document.getElementById('botonTelefonoExtra').disabled = true;
        document.getElementById('telefonoExtra').style.display = "block";
        document.getElementById('telefono').value = valor.split("/")[0];
        document.getElementById('segundoTelefono').value = valor.split("/")[1];
    }   

}


function validarCedula(opcion){
    var contador = 0;
    var filtro = "123456789";
    var respuesta = false;
    if(opcion == "comprobarCampo" && document.getElementById("cedula").value != ""){
        var cedula = document.getElementById("cedula").value;
        for(var i = 0; i < cedula.length; i++){
            if(!filtro.includes(cedula[i])){
                contador++;
            }
        }

        if(opcion == "comprobarCampo"){
            if(contador >= 3 || cedula.length < 9){
                document.getElementById('cedula').style.borderColor = "red";
                $("#lCedula").show();
            }
            else if(contador < 3 || document.getElementById('cedula').value == ""){
                document.getElementById('cedula').style.borderColor = "";
                $("#lCedula").hide();
                respuesta = true;   
            }
        }   
    }

}

function validarCorreo(opcion){

    var respuesta = false;
    if(opcion == "comprobarCampo"){

        if (/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{2,4})+$/.test(document.getElementById('correo').value)){
            document.getElementById('correo').style.borderColor = "";
            $("#lCorreo").hide();
            respuesta = true;
        } 
        else{
            document.getElementById('correo').style.borderColor = "red";
            $("#lCorreo").show();   
        }
        
    }

    return respuesta;
}

var errores = [0,0,0,0];
function validarCampos(opcion, campoTexto){
    var estado = 0;
    if(opcion == "comprobarCampo"){
        var nombre = document.getElementById("nombre").value;
        var primer_apellido = document.getElementById("primer_apellido").value;
        var segundo_apellido = document.getElementById("segundo_apellido").value;
        var telefono = document.getElementById("telefono").value;
        var segundoTelefono = document.getElementById('segundoTelefono').value;
        var institucionRepresentada = document.getElementById('institucionRepresentada').value;
        var seleccionarPuesto = document.getElementById('seleccionarPuesto').value;
        if(nombre == "" && campoTexto == "nombre" || errores[0] == 1){
            errores[0] = 1;
            document.getElementById('nombre').style.borderColor = "red";
            $("#lNombre").show();
        }
        if(nombre != ""){
            document.getElementById('nombre').style.borderColor = "";   
            $("#lNombre").hide();   
            errores[0] = 0;
            estado++;
        }       
        if(primer_apellido == "" && campoTexto == "apellido1" || errores[1] == 1){
            document.getElementById('primer_apellido').style.borderColor = "red";
            $("#lApellido1").show();
            errores[1] = 1;
        }
        if(primer_apellido != ""){
            document.getElementById('primer_apellido').style.borderColor = "";  
            $("#lApellido1").hide();
            estado++;   
            errores[1] = 0;
        }       
        if(segundo_apellido == "" && campoTexto == "apellido2" || errores[2] == 1){
            document.getElementById('segundo_apellido').style.borderColor = "red";
            errores[2] = 1;
            $("#lApellido2").show();            
        }
        if(segundo_apellido != ""){
            errores[2] = 0;
            document.getElementById('segundo_apellido').style.borderColor = ""; 
            $("#lApellido2").hide();
            estado++;               
        }       
        if(campoTexto == "telefono" && document.getElementById('telefono').value.length < 8){
            document.getElementById('telefono').style.borderColor = "red";
            $("#lTelefono").show();             
        }
        else{
            document.getElementById('telefono').style.borderColor = "";
            $("#lTelefono").hide();
            estado++;   
        }       
        if(campoTexto == "segundoTelefono" && document.getElementById('segundoTelefono').value.length < 8){
            document.getElementById('segundoTelefono').style.borderColor = "red";
            $("#lSegundoTelefono").show();              
        }
        else{
            document.getElementById('segundoTelefono').style.borderColor = "";
            $("#lSegundoTelefono").hide();
        }
        if(institucionRepresentada == "" && campoTexto == "institucion" || errores[3] == 1){
            document.getElementById('institucionRepresentada').style.borderColor = "red";
            errores[3] = 1;
            $("#lInstitucion").show();  
        }
        if(institucionRepresentada != ""){
            errores[3] = 0;
            document.getElementById('institucionRepresentada').style.borderColor = "";  
            $("#lInstitucion").hide();
            estado++;   
        }       
        if(seleccionarPuesto == "" && campoTexto == "puesto"){
            document.getElementById('seleccionarPuesto').style.borderColor = "red";
            $("#lPuesto").show();   
        }
        else{
            document.getElementById('seleccionarPuesto').style.borderColor = "";    
            $("#lPuesto").hide();   
            estado++;
        }         
        if(campoTexto == "correo"){
            if(validarCorreo("comprobarCampo") == true){
                estado++;
            }
        }
        return estado;
    }
}

//insertar un campo de texto mas para el telefono
var puertaTelefono = true;
function insertarTelefonoExtra(opcion){
    document.getElementById('telefonoExtra').style.display = "block";
    puertaTelefono = false;

}

function verVentanaConfirmacion(opcion){
    var modal = document.getElementById('modalConfirmacion');

        // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("closeConfirmacion")[0];
        // When the user clicks on <span> (x), close the modal
    $("#modalConfirmacion").modal("show");
    if(opcion == "registro"){
        document.getElementById('confirmacion').innerHTML = "¡Registro Exitoso!";
        span.onclick = function() {

            if(guardarArea == 3){
                crearPaginacion("../CNegocio/controladoraDirectorio.php");
                verificarLimitePaginacion("../CNegocio/controladoraDirectorio.php",false);
            }
            if(guardarArea == 4){
                crearPaginacion("../CNegocio/controladoraVoluntario.php");
                verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",false);   
            }
            mostrarResultadosdeTablas(calcularPaginaparaRegistros());
            cancelarDato();
        }
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                if(guardarArea == 3){
                    crearPaginacion("../CNegocio/controladoraDirectorio.php");
                    verificarLimitePaginacion("../CNegocio/controladoraDirectorio.php",false);
                }
                if(guardarArea == 4){
                    crearPaginacion("../CNegocio/controladoraVoluntario.php");
                    verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",false);   
                }
                mostrarResultadosdeTablas(calcularPaginaparaRegistros());
                cancelarDato();
            }
        }
    }
    else if(opcion == "registroError"){
        document.getElementById('confirmacion').style.backgroundColor = "#f44336";
        document.getElementById('botonConfirmacion').style.background = "#f44336";
        document.getElementById('confirmacion').innerHTML = "¡Problema de Registro!";

        span.onclick = function() {
            cancelarDato();
        }
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                cancelarDato();
            }
        }
    }
    else if(opcion == "actualizar"){
        document.getElementById('confirmacion').innerHTML = "¡Actualización Exitosa!";

        span.onclick = function() { 
            if(guardarArea == 3){
                crearPaginacion("../CNegocio/controladoraDirectorio.php");
                verificarLimitePaginacion("../CNegocio/controladoraDirectorio.php",false);
            }
            if(guardarArea == 4){
                crearPaginacion("../CNegocio/controladoraVoluntario.php");
                verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",false);   
            }         
            mostrarResultadosdeTablas(calcularPaginaparaRegistros());
            cancelarDato();
        }
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {        
                if(guardarArea == 3){
                    crearPaginacion("../CNegocio/controladoraDirectorio.php");
                    verificarLimitePaginacion("../CNegocio/controladoraDirectorio.php",false);
                }
                if(guardarArea == 4){
                    crearPaginacion("../CNegocio/controladoraVoluntario.php");
                    verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",false);   
                }
                mostrarResultadosdeTablas(calcularPaginaparaRegistros());
                cancelarDato();      
            }
        }
    }
    else if(opcion == "actualizarError"){
        document.getElementById('confirmacion').style.backgroundColor = "#f44336";
        document.getElementById('botonConfirmacion').style.background = "#f44336";
        document.getElementById('confirmacion').innerHTML = "¡Problema de Actualización!";

        span.onclick = function() {
            cancelarDato();
        }
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                cancelarDato();
            }
        }
    }
    else if(opcion == "actualizarEspacios"){
        document.getElementById('confirmacion').style.backgroundColor = "#f44336";
        document.getElementById('botonConfirmacion').style.background = "#f44336";
        document.getElementById('confirmacion').innerHTML = "¡NO deje espacios en blanco!";
        var modal = document.getElementById('modalConfirmacion');

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("closeConfirmacion")[0];
        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            cancelarDato();
        }
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                cancelarDato();
            }
        }       
    }
    else if(opcion == "eliminar"){
        document.getElementById('confirmacion').innerHTML = "¡Registro Eliminado!";

        span.onclick = function() {
            if(guardarArea == 3){
                crearPaginacion("../CNegocio/controladoraDirectorio.php");
                verificarLimitePaginacion("../CNegocio/controladoraDirectorio.php",false);
            }
            if(guardarArea == 4){
                crearPaginacion("../CNegocio/controladoraVoluntario.php");
                verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",false);   
            }
            mostrarResultadosdeTablas(calcularPaginaparaRegistros());
            history.pushState(null, "", "../CPresentacion/ventanaAdminPlanInfor.php?pagina= 1");
            cancelarDato();            
        }
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                mostrarResultadosdeTablas(calcularPaginaparaRegistros());
                if(guardarArea == 3){
                    crearPaginacion("../CNegocio/controladoraDirectorio.php");
                    verificarLimitePaginacion("../CNegocio/controladoraDirectorio.php",false);
                }
                if(guardarArea == 4){
                    crearPaginacion("../CNegocio/controladoraVoluntario.php");
                    verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",false);   
                }
                cancelarDato();
            }
        }       
    }
}

function botonConfirmacion(){

    $("#modalConfirmacion").modal("hide");
    if(guardarArea == 3){
        crearPaginacion("../CNegocio/controladoraDirectorio.php");
        mostrarResultadosdeTablas(calcularPaginaparaRegistros());
        verificarLimitePaginacion("../CNegocio/controladoraDirectorio.php",false);

    }
    if(guardarArea == 4){
        crearPaginacion("../CNegocio/controladoraVoluntario.php");
        mostrarResultadosdeTablas(calcularPaginaparaRegistros());
        verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",false);
    }
    cancelarDato();
}

/*FIN DE LOS METODOS DEL DIRECTORIO*/
//
//
//

/*METODOS FIJOS */

//CARGA LA INFORMACION GENERAL DE LA TABLA
function mostrarResultadosdeTablas(valor){

    var pagina = 0;
    if(valor == 0)
        pagina = calcularPaginaparaRegistros();
    if(valor > 0)
        pagina = valor;

    if(pagina == 1)
        pagina = parseInt(pagina)-1;
    else
        pagina = parseInt(pagina)*3;

    switch(guardarArea){

        case guardarArea = 1:
            var xhr = new XMLHttpRequest();
            xhr.open("POST", "../CNegocio/controladoraAlbergues.php");
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.send("consultarAlberguesVisita=true" + "&pagina=" + pagina);

            xhr.onreadystatechange = function () {

                if (xhr.readyState == 4 && xhr.status == 200) {

                    var resultado = xhr.responseText;

                    //alert(resultado);
                    if (resultado != "null") {

                        $("tr").remove(".cla");
                        $("#tablaDirectorio").append(resultado);
                    }
                }
            }
        break;

        case guardarArea = 3:
            var xhr=new XMLHttpRequest();
            xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
            xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
            xhr.send("buscarIntegrantes=true" + "&pagina=" + pagina);
            xhr.onreadystatechange = function () {

                if (xhr.readyState == 4 && xhr.status == 200) {

                    var resultado = xhr.responseText;
                    if (resultado != "null") {
                        $("tr").remove(".cla"); 
                        //Se aplica un split, para separar cada integrante.
                        var cadena = resultado.split("+");
                        for (var i = 0; i < cadena.length -1; i++) { //Ciclo para mostrar cada integrante.

                            var datos = cadena[i].split(","); //Se aplica split, para dividir cada atributo del integrante.
                            if(datos[2] != "Default"){
                                $("#tablaDirectorio").append(""+
                                    "<tr class = 'cla'>"+
                                        "<td hidden>" +  datos[1]  + " </td>" +
                                        "<td>" + datos[2] + "</td>" +
                                        "<td>" + datos[3] + "</td>" +
                                        "<td>" + datos[4] + "</td>" +
                                        "<td>" + datos[5] + "</td>" +
                                        "<td>" + datos[9] + "</td>" +
                                        "<td>" + datos[6] + "</td>" +
                                        "<td>" + datos[8] + "</td>" +
                                        "<td>" + "<BUTTON ID = 'actualizar' NAME = 'actualizar' ONCLICK = 'Seleccionar("+datos[0]+")'><i class='far fa-edit'></i></BUTTON>"+ 
                                        "<BUTTON ID = 'eliminar' NAME = 'eliminar' ONCLICK = 'verDetalleEliminar("+ datos[0] + ")'><i class='fas fa-trash-alt'></i></BUTTON>"+
                                        "<INPUT TYPE ='TEXT' NAME = 'idAjax' ID = 'idAjax' VALUE = "+ datos[0] +" MAXLENGTH = '1' SIZE = '1' DISABLED HIDDEN/>" + "</td>" +
                                    "</tr>"
                                );
                            }
                        } 
                    } 
                }
            }
        break;

        case guardarArea = 4:
            var xhr = new XMLHttpRequest();
            xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.send("consultar=true" + "&pagina=" + pagina);

            xhr.onreadystatechange = function () {

                if (xhr.readyState == 4 && xhr.status == 200) {

                    var resultado = xhr.responseText;
                        
                    if (resultado != 0) {
                        $("tr").remove(".cla"); 
                        $("#tablaDirectorio").append(resultado);
                    }
                }
            }
        break;

        case guardarArea = 7:
            var xhr = new XMLHttpRequest();
            xhr.open("POST", "../CNegocio/controladoraBodega.php");
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.send("consultarBodegasVisita=true" + "&pagina=" + pagina);

            xhr.onreadystatechange = function () {

                if (xhr.readyState == 4 && xhr.status == 200) {

                    var resultado = xhr.responseText;

                    //alert(resultado);
                    if (resultado != "null") {

                        $("tr").remove(".cla");
                        $("#tablaDirectorio").append(resultado);
                    }
                }
            }
        break;

        case guardarArea = 9:
            var xhr = new XMLHttpRequest();
            xhr.open("POST", "../CNegocio/controladoraZonaRiesgo.php");
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.send("consultarZonadeRiesgoLimitada=true" + "&pagina=" + pagina);

            xhr.onreadystatechange = function () {

                if (xhr.readyState == 4 && xhr.status == 200) {

                    var resultado = xhr.responseText;

                    //alert(resultado);
                    if (resultado != 0) {

                        $("tr").remove(".cla");
                        $("#tablaDirectorio").append(resultado);
                    } else {

                        $("tr").remove(".cla");
                        $("#tablaDirectorio").show();
                    }
                }
            }
        break;
    }
        
}

//CARGA LOS 3 ESTADOS DEL MODAL: REGISTRAR, ACTUALIZAR Y ELIMINAR
//PUERTA 1 ES PARA ACTUALIZAR, PUERTA 2 ES PARA REGISTRAR, PUERTA 3 ES PARA ELIMINAR
function cargaModalGlobal(puerta,variableId){

    switch(guardarArea){

        case guardarArea = 3:
            if(puerta == 1){

                var xhr=new XMLHttpRequest();
                xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
                xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                xhr.send("verDetalle=true" + "&id=" + variableId);
                xhr.onreadystatechange = function () {

                    if (xhr.readyState == 4 && xhr.status == 200) {

                        var resultado = xhr.responseText;
                        if (resultado != "null") {
                            if(resultado.split(",")[4] == "")
                                document.getElementById("encabezadoModal").innerHTML = resultado.split(",")[2] + " " + resultado.split(",")[3];    
                            else
                                document.getElementById("encabezadoModal").innerHTML = resultado.split(",")[2] + " " + resultado.split(",")[3] + " " + resultado.split(",")[4];    
                            var cadena = resultado.split(",");
                            $("#tablaxinformacionDetallada").append("" +
                                "<TBODY>" +
                                    "<TR hidden>" +
                                        "<br><TD>" + "<INPUT TYPE='text' NAME='id' ID='id' VALUE = '"+cadena[0]+"'/>" +
                                    "</TR>" +
                                    "<TR>" +
                                        "<TD>C&eacute;dula:" +
                                            "<br><INPUT TYPE='text' NAME='cedula' ID='cedula' SIZE = '18' MAXLENGTH = '15' ONBLUR = validarCedula('comprobarCampo') PLACEHOLDER = 'Campo no obligatorio' VALUE = '"+cadena[1]+"' />" +
                                                "<FONT color=red /><LABEL style='display:none;' id='lCedula' class='asterisco'> *</LABEL></TH></TD>" +
                                    "</TR>" +
                                    "<TR>" +
                                        "<TD>Nombre:" +
                                            "<br><INPUT TYPE='text' NAME='nombre' ID='nombre' SIZE = '18' MAXLENGTH = '30' ONBLUR = validarCampos('comprobarCampo','nombre') PLACEHOLDER = 'Escriba su nombre'VALUE = '"+cadena[2]+"' />" +
                                                "<FONT color=red /><LABEL style='display:none;' id='lNombre' class='asterisco'> *</LABEL></TH></TD>" +
                                    "</TR>" + 
                                    "<TR>" +
                                        "<TD>Apellidos:"+
                                            "<br><INPUT TYPE='text' NAME='primer_apellido' ID='primer_apellido' SIZE = '18' MAXLENGTH = '30' ONBLUR = validarCampos('comprobarCampo','apellido1') PLACEHOLDER = 'Escriba su primer apellido' VALUE = '"+cadena[3]+"'/>" +
                                                "<FONT color=red /><LABEL style='display:none;' id='lApellido1' class='asterisco'> *</LABEL></TH>" +
                                            "<br><INPUT TYPE='text' NAME='segundo_apellido' ID='segundo_apellido' SIZE = '18' MAXLENGTH = '30' ONBLUR = validarCampos('comprobarCampo','apellido2') PLACEHOLDER = 'Escriba su segundo apellido' VALUE = '"+cadena[4]+"'/>" +
                                                "<FONT color=red /><LABEL style='display:none;' id='lApellido2' class='asterisco'> *</LABEL></TH>" +
                                        "</TD>" +
                                    "</TR>" +
                                    "<TR>" +
                                        "<TD>Tel&eacute;fono:" +
                                            "<br><INPUT TYPE='text' NAME='telefono' ID='telefono' SIZE = '18' MAXLENGTH = '18' ONBLUR = validarCampos('comprobarCampo','telefono') PLACEHOLDER = '####-####' VALUE = '"+cadena[5]+"'/>" +
                                                "<FONT color=red /><LABEL style='display:none;' id='lTelefono' class='asterisco'> *</LABEL></TH>" +
                                            "<br><INPUT TYPE ='button' NAME='botonTelefonoExtra' ID='botonTelefonoExtra' VALUE = '+' ONCLICK = insertarTelefonoExtra('registrar')></TD>" +
                                    "</TR>" +
                                        "<TR ID = 'telefonoExtra' style = 'display: none;' ALIGN = 'center'>" +
                                        "<TD>Tel&eacute;fono Extra:" +
                                            "<br><INPUT TYPE='text' NAME='segundoTelefono' ID='segundoTelefono' SIZE = '18' MAXLENGTH = '18' ONBLUR = validarCampos('comprobarCampo','segundoTelefono' PLACEHOLDER = '####-####') />" +
                                                "<FONT color=red /><LABEL style='display:none;' id='lSegundoTelefono' class='asterisco'> *</LABEL></TH>" +
                                        "</TD>" +
                                    "</TR>" +
                                    "<TR>" +
                                        "<TD>Correo electr&oacute;nico:"+
                                            "<br><INPUT TYPE='text' NAME='correo' ID='correo' SIZE = '18' MAXLENGTH = '150' ONBLUR = validarCampos('comprobarCampo','correo') PLACEHOLDER = 'Escriba su correo electrónico' VALUE = '"+cadena[9].split("+")[0]+"' />" +
                                                "<FONT color=red /><LABEL style='display:none;' id='lCorreo' class='asterisco'> *</LABEL></TH>" +
                                        "</TD>" +
                                    "</TR>" +
                                    "<TR>" +
                                        "<TD>Instituci&oacute;n: <br><INPUT TYPE='text' NAME='institucionRepresentada' ID='institucionRepresentada' MAXLENGTH = '40' SIZE = '18' ONBLUR = validarCampos('comprobarCampo','institucion') PLACEHOLDER = 'Escriba su institución o empresa' VALUE = '"+cadena[6]+"'/>" +
                                            "<FONT color=red /><LABEL style='display:none;' id='lInstitucion' class='asterisco'> *</LABEL></TH></TD>" +
                                    "</TR>" +
         
                                    "<TR>" +
                                        "<TD>Puesto:" +
                                            "<br><INPUT TYPE = 'TEXT' ID = 'seleccionarPuesto' NAME = 'seleccionarPuesto' MAXLENGTH = '30' SIZE = '18' ONBLUR = validarCampos('comprobarCampo','puesto') VALUE = '"+cadena[7]+"'>" +
                                            "<FONT color=red /><LABEL style='display:none;' id='lPuesto' class='asterisco'> *</LABEL></TH></TD>" +
                                    "</TR>" +
                                          
                                    "<TR>" +
                                        "<TD>Comit&eacute;:"+
                                            "<br><INPUT TYPE = 'TEXT' ID = 'comite' NAME = 'comite' MAXLENGTH = '30' SIZE = '18' VALUE = '"+cadena[8]+"' ONBLUR = validarCampos('comprobarCampo','comite')>" +
                                            "<FONT color=red /><LABEL style='display:none;' id='lComite' class='asterisco'> *</LABEL></TH>" +
                                        "</TD>" +
                                    "</TR>"+
                            "</TBODY>"
                            );

                        }
                        //LLAMA EL METODO QUE DESHABILITA EL BOTON DE AGREGAR UN SEGUNDO TELEFONO Y SEPARA LOS TELEFONOS EN LOS CAMPOS
                        //RESPECTIVOS, EVITANDO COLOCAR LOS DOS TELEFONOS CON EL / EN UN MISMO CAMPO
                        habilitarbotonSegundoTelefono(cadena[5]); 
                        $("#telefono").mask("9999-9999");
                        $("#segundoTelefono").mask("9999-9999"); 
                        $('#telefono').on('input', function () { 
                            this.value = this.value.replace(/[^0-9]/g,'');
                        });
                        $('#segundoTelefono').on('input', function () { 
                            this.value = this.value.replace(/[^0-9]/g,'');
                        });
                        $(".asterisco").hide();
                    }
                }

            }

            if(puerta == 2){
                $("#tablaxinformacionDetallada").append("" +
                    "<TBODY>" +
                        "<TR>" +
                            "<TD>C&eacute;dula:" +
                                "<br><INPUT TYPE='text' NAME='cedula' ID='cedula' SIZE = '18' MAXLENGTH = '15' ONBLUR = validarCedula('comprobarCampo') PLACEHOLDER = 'Campo no obligatorio' />" +
                                "<FONT color=red /><LABEL style='display:none;' id='lCedula' class='asterisco'> *</LABEL></TH>"+
                            "</TD>" +
                        "</TR>" +
                        "<TR>" +
                            "<TD>Nombre:" +
                                "<br><INPUT TYPE='text' NAME='nombre' ID='nombre' SIZE = '18' MAXLENGTH = '30' ONBLUR = validarCampos('comprobarCampo','nombre') PLACEHOLDER = 'Escriba su nombre' />" +
                                "<FONT color=red /><LABEL style='display:none;' id='lNombre' class='asterisco'> *</LABEL></TH>" +
                            "</TD>" +
                        "</TR>" + 
                        "<TR>" +
                            "<TD>Apellidos:"+
                                "<br><INPUT TYPE='text' NAME='primer_apellido' ID='primer_apellido' SIZE = '18' MAXLENGTH = '30' ONBLUR = validarCampos('comprobarCampo','apellido1') PLACEHOLDER = 'Escriba su primer apellido'/>" +
                                "<FONT color=red /><LABEL style='display:none;' id='lApellido1' class='asterisco'> *</LABEL></TH>" +
                                "<br><INPUT TYPE='text' NAME='segundo_apellido' ID='segundo_apellido' SIZE = '18' MAXLENGTH = '30' ONBLUR = validarCampos('comprobarCampo','apellido2') PLACEHOLDER = 'Escriba su segundo apellido'/>" +
                                "<FONT color=red /><LABEL style='display:none;' id='lApellido2' class='asterisco'> *</LABEL></TH>" +
                            "</TD>" +
                        "</TR>" +
                        "<TR>" +
                            "<TD>Tel&eacute;fono:" +
                                "<br><INPUT TYPE='text' NAME='telefono' ID='telefono' SIZE = '18' MAXLENGTH = '8'  ONBLUR = validarCampos('comprobarCampo','telefono') PLACEHOLDER = '####-####'/>" +
                                "<FONT color=red /><LABEL style='display:none;' id='lTelefono' class='asterisco'> *</LABEL></TH>" +
                                "<br><INPUT TYPE ='button' NAME='botonTelefonoExtra' ID='botonTelefonoExtra' VALUE = '+' ONCLICK = insertarTelefonoExtra('registrar')></TD>" +
                        "</TR>" +
                        "<TR ID = 'telefonoExtra' style = 'display: none;' ALIGN = 'center'>" +
                            "<TD>Tel&eacute;fono Extra:" +
                                "<br><INPUT TYPE='text' NAME='segundoTelefono' ID='segundoTelefono' SIZE = '18' MAXLENGTH = '8' ONBLUR = validarCampos('comprobarCampo','segundoTelefono') PLACEHOLDER = '####-####' />" +
                                "<FONT color=red /><LABEL style='display:none;' id='lSegundoTelefono' class='asterisco'> *</LABEL></TH>" +
                            "</TD>" +
                        "</TR>" +
                        "<TR>" +
                            "<TD>Correo electr&oacute;nico:"+
                                "<br><INPUT TYPE='text' NAME='correo' ID='correo' SIZE = '18' MAXLENGTH = '18' ONBLUR = validarCampos('comprobarCampo','correo') PLACEHOLDER = 'Escriba su correo electrónico' />" +
                                "<FONT color=red /><LABEL style='display:none;' id='lCorreo' class='asterisco'> *</LABEL></TH>" +
                            "</TD>" +
                        "</TR>" +
                        "<TR>" +
                            "<TD>Instituci&oacute;n:"+
                                "<br><INPUT TYPE='text' NAME='institucionRepresentada' ID='institucionRepresentada' MAXLENGTH = '40' SIZE = '18' ONBLUR = validarCampos('comprobarCampo','institucion') PLACEHOLDER = 'Escriba su institución o empresa'/>" +
                                "<FONT color=red /><LABEL style='display:none;' id='lInstitucion' class='asterisco'> *</LABEL></TH></TD>" +
                        "</TR>" +
                        "<TR>" +
                            "<TD>Puesto:" +
                                "<br><INPUT TYPE = 'TEXT' ID = 'seleccionarPuesto' NAME = 'seleccionarPuesto' MAXLENGTH = '40' SIZE = '18' ONBLUR = validarCampos('comprobarCampo','puesto') PLACEHOLDER = 'Escriba su puesto institucional'>" +
                                "<FONT color=red /><LABEL style='display:none;' id='lPuesto' class='asterisco'> *</LABEL></TH></TD>" +
                            "</TD>" +
                        "</TR>"  +              
                    "</TBODY>"
                ); 
                $("#telefono").mask("9999-9999");
                $("#segundoTelefono").mask("9999-9999");
                $('#telefono').on('input', function () { 
                    this.value = this.value.replace(/[^0-9]/g,'');
                });
                $('#segundoTelefono').on('input', function () { 
                    this.value = this.value.replace(/[^0-9]/g,'');
                });
                $(".asterisco").hide();
            } 

            if(puerta == 3){
                var xhr=new XMLHttpRequest();
                xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
                xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                xhr.send("verDetalle=true" + "&id=" + variableId);
                xhr.onreadystatechange = function () {

                    if (xhr.readyState == 4 && xhr.status == 200) {

                        var resultado = xhr.responseText;
                        if (resultado != "null") {
                            document.getElementById("encabezadoModal").innerHTML = "Registro por Eliminar";    
                            var cadena = resultado.split(",");
                            $("#tablaxinformacionDetallada").append("" +
                                "<TBODY>" +
                                    "<TR hidden>" +
                                        "<TD>" + "<INPUT TYPE='text' NAME='id' ID='id' VALUE = '"+cadena[0]+"'/>" +
                                    "</TR>" +
                                    "<TR>" +
                                        "<TD>Integrante:" +
                                            "<br>"+cadena[2]+" "+cadena[3]+" "+cadena[4]+
                                        "</TD>" +
                                    "</TR>" +
                                    "</TR>"+
                            "</TBODY>"
                            );

                        }
                        //LLAMA EL METODO QUE DESHABILITA EL BOTON DE AGREGAR UN SEGUNDO TELEFONO Y SEPARA LOS TELEFONOS EN LOS CAMPOS
                        //RESPECTIVOS, EVITANDO COLOCAR LOS DOS TELEFONOS CON EL / EN UN MISMO CAMPO
                        habilitarbotonSegundoTelefono(cadena[5]); 
                        $(".asterisco").hide();
                    }
                }

            }

        break;

        case guardarArea = 4:
            if(puerta == 1){
                var xhr=new XMLHttpRequest();
                xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
                xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                xhr.send("verDetalle=true" + "&id=" + variableId);
                xhr.onreadystatechange = function () {

                    if (xhr.readyState == 4 && xhr.status == 200) {

                        var resultado = xhr.responseText;
                        if (resultado != "null") {
                            var cadena = resultado.split("_");
                            document.getElementById('encabezadoModal').innerHTML = cadena[1];
                            $("#tablaxinformacionDetallada").append("" +
                                "<tbody>" +
                                    "<tr hidden><td><input type = 'text' id = 'id_id' value = '"+cadena[0]+"' /></td></tr>" +
                                    "<tr>" +
                                        "<td>Nombre Completo:" +
                                            "<br><input type='text' name='nombre' id='nombre' size='18' maxlength='30' value = '"+cadena[1]+"' placeholder='Michael Andres Salas Granados' onkeypress=return AceptarLetrasNumeros(event) />" +
                                            "<font color=red ><label style='display:none;' id='lNombre' class='asterisco'> *</label></font>" +
                                        "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td>Tel&eacute;fono:" +
                                            "<br><input type='text' name='telefono' id='telefono' size='18' maxlength='11' value= '"+cadena[2]+"' placeholder='88-77-66-55' />" +
                                        "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td>Detalle:" +
                                            "<br><textarea id='detalle' rows = '5' cols = '30' maxlength='300' onkeypress=return AceptarLetrasNumerosComa(event) placeholder='Escribe aqu&iacute; porqu&eacute; quieres ser voluntario' >"+cadena[3]+"</textarea>" +
                                        "</td>" +
                                    "</tr>" +
                                "</tbody>"
                            );
                            $("#telefono").mask("9999-9999");                        
                            $('#telefono').on('input', function () { 
                                this.value = this.value.replace(/[^0-9]/g,'');
                            });                                                
                        }
                    }
                }
            }
            if(puerta == 2){
                $("#tablaxinformacionDetallada").append("" +
                    "<tbody>" +
                        "<tr hidden><td><input type = 'text' id = 'id_id'/></td></tr>" +
                        "<tr>" +
                            "<td>Nombre Completo:" +
                                "<br><input type='text' name='nombre' id='nombre' size='18' maxlength='30' placeholder='Michael Andres Salas Granados' onkeypress=return AceptarLetrasNumeros(event) />" +
                                "<font color=red ><label id='lNombre' style='display:none;' class='asterisco'> *</label></font>" +
                            "</td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Tel&eacute;fono:" +
                                "<br><input type='text' name='telefono' id='telefono' size='18' maxlength='11' onkeypress=return validacionNumerico(event) placeholder='88-77-66-55' />" +
                            "</td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>Detalle:" +
                                "<br><textarea id='detalle' rows = '5' cols = '30' maxlength='300' onkeypress=return AceptarLetrasNumerosComa(event) placeholder='Escribe aqu&iacute; porqu&eacute; quieres ser voluntario' ></textarea>" +
                            "</td>" +
                        "</tr>" +
                    "</tbody>"
                );
                $("#telefono").mask("9999-9999");                        
                    $('#telefono').on('input', function () { 
                    this.value = this.value.replace(/[^0-9]/g,'');
                });  


            }
            if(puerta == 3){
                var xhr=new XMLHttpRequest();
                xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
                xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                xhr.send("verDetalle=true" + "&id=" + variableId);
                xhr.onreadystatechange = function () {

                    if (xhr.readyState == 4 && xhr.status == 200) {

                        var resultado = xhr.responseText;
                        if (resultado != "null") {
                            var cadena = resultado.split("_");
                            document.getElementById('encabezadoModal').innerHTML = cadena[1];
                            $("#tablaxinformacionDetallada").append("" +
                                "<tbody>" +
                                    "<tr hidden><td><input type = 'text' id = 'id_id' value = '"+cadena[0]+"' /></td></tr>" +
                                    "<tr>" +
                                        "<td>Nombre Completo:" +
                                            "<br><input type='text' name='nombre' id='nombre' size='18' maxlength='30' value = '"+cadena[1]+"' placeholder='Michael Andres Salas Granados' onkeypress=return AceptarLetrasNumeros(event) disabled/>" +
                                            "<font color=red ><label style='display:none;' id='lNombre' class='asterisco'> *</label></font>" +
                                        "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td>Tel&eacute;fono:" +
                                            "<br><input type='text' name='telefono' id='telefono' size='18' maxlength='11' value= '"+cadena[2]+"' onkeypress=return validacionNumerico(event) placeholder='88-77-66-55' disabled/>" +
                                        "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td>Detalle:" +
                                            "<br><textarea id='detalle' rows = '5' cols = '30' maxlength='300' onkeypress=return AceptarLetrasNumerosComa(event) placeholder='Escribe aqu&iacute; porqu&eacute; quieres ser voluntario' disabled>"+cadena[3]+"</textarea>" +
                                        "</td>" +
                                    "</tr>" +
                                "</tbody>"
                            );

                        }
                    }
                }                
            }            
        break;
    }
    
}

//CARGA EL MODAL PARA ACTUALIZAR
function Seleccionar(txt){
    $("#tablaxinformacionDetallada TBODY").remove();
    $("#modal").modal("show");
    if(guardarArea == 3){
        document.getElementById('aceptarDatos').value = "Actualizar";
        cargaModalGlobal(1,txt);        
    }
    if(guardarArea == 4){
        document.getElementById('aceptarDatos').value = "Actualizar";
        cargaModalGlobal(1,txt);               
    }
}

//ACCIONA EL BOTON DE REGISTRO ACORDE AL AREA SERA EL MODAL
function verModalRegistrar(){

    $("#tablaxinformacionDetallada TBODY").remove();
    $("#modal").modal("show");
    if(guardarArea == 3){

        document.getElementById('encabezadoModal').innerHTML = "Registrar Integrante";
        document.getElementById('aceptarDatos').value = "Registrar";
   
        cargaModalGlobal(2,"");         
    }
    if(guardarArea == 4){
        document.getElementById('encabezadoModal').innerHTML = "Registrar Voluntario";
        document.getElementById('aceptarDatos').value = "Registrar";
   
        cargaModalGlobal(2,"");        
    }
}

//CARGAR EL MODAL PARA ELIMINAR
function verDetalleEliminar(txt){
    $("#tablaxinformacionDetallada TBODY").remove();
    $("#modal").modal("show");
    if(guardarArea == 3){
        document.getElementById('aceptarDatos').value = "Eliminar";
        cargaModalGlobal(3,txt);        
    }
    if(guardarArea == 4){
        ConfirmarEliminacion(txt);          
    }
}

function ConfirmarEliminacion(id) {
    $("#tablaxinformacionDetallada TBODY").remove();
    $("#modal").modal("show");
    if(guardarArea == 4){
        document.getElementById('aceptarDatos').value = "Eliminar";
        cargaModalGlobal(3,id);            
    }
}

function ocultarError() { // Metodo que oculta (visiblemente), los mensajes (DIV).

    $(".asterisco").hide();
    document.getElementById('cedula').style.borderColor = "";
    document.getElementById('nombre').style.borderColor = "";
    document.getElementById('primer_apellido').style.borderColor = "";
    document.getElementById('segundo_apellido').style.borderColor = "";
    document.getElementById('telefono').style.borderColor = "";
    document.getElementById('institucionRepresentada').style.borderColor = "";
    document.getElementById('seleccionarPuesto').style.borderColor = "";
    document.getElementById('seleccionarComite').style.borderColor = "";
    document.getElementById('correo').style.borderColor = "";
}


//LOS CAMBIOS GENERADOS SON ACEPTADOS ACORDE A LA SECCION ELEGIDA
//LOS CAMBIOS DE ELIMINAR, ACTUALIZAR Y REGISTRAR
function aceptarDatos(){
    if(guardarArea == 3){
        switch(document.getElementById('aceptarDatos').value){
            case document.getElementById('aceptarDatos').value = "Actualizar":
                actualizarIntegrante();
                break;
            case document.getElementById('aceptarDatos').value = "Eliminar":
                eliminarIntegrante();
                break;
            case document.getElementById('aceptarDatos').value = "Registrar":
                registrarIntegrante();
                break;
        }
    }
    if(guardarArea == 4){
        switch(document.getElementById('aceptarDatos').value){
            case document.getElementById('aceptarDatos').value = "Actualizar":
                actualizarVoluntario();
                break;
            case document.getElementById('aceptarDatos').value = "Eliminar":
                eliminarVoluntario();
                break;
            case document.getElementById('aceptarDatos').value = "Registrar":
                registrarVoluntario();
                break;
        }
    }
}

//DESTRUYE EL TBODY PARA LIMPIAR TODO EL MODAL, INDISTINTAMENTE SEA DE REGISTRAR, ACTUALIZAR O ELIMINAR
function cancelarDato(){
    $("#tablaxinformacionDetallada TBODY").remove();
    document.getElementById('telefonoExtra').style.display = 'none';
}


function cargarSiguienteInformacion(){
    var temp = calcularPaginaparaRegistros();
    temp = parseInt(temp)+1;
    history.pushState(null, "", "../CPresentacion/ventanaAdminPlanInfor.php?pagina= " + temp);
    segundaSolicitudInformacion(temp,true);


}
function cargarPreviaInformacion(){
    var temp = calcularPaginaparaRegistros();
    temp = parseInt(temp)-1;
    history.pushState(null, "", "../CPresentacion/ventanaAdminPlanInfor.php?pagina= " + temp);
    segundaSolicitudInformacion(temp,false);
}

function segundaSolicitudInformacion(pagina,avanzo,retrocedio){

    if(guardarArea == 1){
        verificarLimitePaginacion("../CNegocio/controladoraAlbergues.php",avanzo,retrocedio);
        mostrarResultadosdeTablas(pagina);
    }
    if(guardarArea == 3){
        verificarLimitePaginacion("../CNegocio/controladoraDirectorio.php",avanzo,retrocedio);
        mostrarResultadosdeTablas(pagina);
    }

    if(guardarArea == 4){
        verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",avanzo,retrocedio);
        mostrarResultadosdeTablas(pagina);        
    }

    if(guardarArea == 7){
        verificarLimitePaginacion("../CNegocio/controladoraBodega.php",avanzo,retrocedio);
        mostrarResultadosdeTablas(pagina);         
    }

    if(guardarArea == 9){
        verificarLimitePaginacion("../CNegocio/controladoraZonaRiesgo.php",avanzo,retrocedio);
        mostrarResultadosdeTablas(pagina);        
    }
}


function detectarTeclaEnter_enBusqueda(e){

    var evt = e ? e : event;
    var key = window.Event ? evt.which : evt.keyCode;

    if(key == 13){

        busquedaEspecifica();
    }

}


function busquedaEspecifica(){
    var buscar = document.getElementById('buscar').value;
    var tabla = document.getElementById('tabla');
    var td = "";
    var tr = "";
    var temp = "";
    var dato = "Sin resultado";
    var i = 1; 
    var puerta = false;
    var area = "";
    var busquedaEspecifica = "";

    if(guardarArea == 1){
        area = "../CNegocio/controladoraAlbergues.php";
        busquedaEspecifica = "buscarAlbergueEspecifico";
    }
    if(guardarArea == 3){
        area = "../CNegocio/controladoraDirectorio.php";
        busquedaEspecifica = "buscarIntegranteEspecifico";
    }
    if(guardarArea == 4){
        area = "../CNegocio/controladoraVoluntario.php";
        busquedaEspecifica = "buscarIntegranteEspecifico";
    }
    if(guardarArea == 7){
        area = "../CNegocio/controladoraBodega.php";
        busquedaEspecifica = "buscarBodegaEspecifica";   
    }
    if(guardarArea == 9){
        area = "../CNegocio/controladoraZonaRiesgo.php";
        busquedaEspecifica = "buscarPuntoRiesgoEspecifico";   
    }

    if(buscar != ""){
        while(i < tabla.rows.length){
            tr = tabla.rows[i];
            for(var j = 1; j < tr.cells.length-1; j++){
                td = tr.cells[j].innerHTML;
                if(td.toUpperCase().includes(buscar.toUpperCase())){
                    temp = td;
                    if(!puerta){
                        puerta = true;
                        dato = "";
                    }
                    if(!dato.includes(temp)){
                        dato += temp;
                        alert(dato);
                    }                    
                }
            }
            i++;
        }
        if(dato != "Sin resultado"){
            for(var i = 1; i < tabla.rows.length; i++){                
                if(tabla.rows[i].innerText.includes(dato)){
                    tabla.rows[i].style.display = "";
                }
                else{
                    tabla.rows[i].style.display = "none";
                }
            }
        }
        else{
            if(dato == "Sin resultado"){

                buscar_en_baseD(buscar, area, busquedaEspecifica);

            }
            else
                resultadoBusqueda(dato);
        }
        
    }
    if(buscar == ""){
        resultadoBusqueda(buscar);
    }

}

function buscar_en_baseD(variable, area, busquedaEspecifica){
    var xhr = new XMLHttpRequest();
    xhr.open("POST", area);
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send(busquedaEspecifica+"=" + variable);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            if (resultado != 0) {

                switch(guardarArea){
                    case guardarArea = 3:
                        $("tr").remove(".cla");
                        var cadena = resultado.split("+");
                        for(var i = 0; i < resultado.length; i++){
                            var datos = cadena[i].split(",");
                            if(!datos[2].includes("undefined")){
                                $("#tablaDirectorio").append(""+
                                    "<tr class = 'cla'>"+
                                        "<td hidden>" +  datos[1]  + " </td>" +
                                        "<td>" + datos[2] + "</td>" +
                                        "<td>" + datos[3] + "</td>" +
                                        "<td>" + datos[4] + "</td>" +
                                        "<td>" + datos[5] + "</td>" +
                                        "<td>" + datos[9] + "</td>" +
                                        "<td>" + datos[6] + "</td>" +
                                        "<td>" + datos[8] + "</td>" +
                                        "<td>" + "<BUTTON ID = 'actualizar' NAME = 'actualizar' ONCLICK = 'verDetalle("+datos[0]+")'><i class='far fa-edit'></i></BUTTON>"+ 
                                            "<BUTTON ID = 'eliminar' NAME = 'eliminar' ONCLICK = 'verDetalleEliminar("+ datos[0] + ")'><i class='fas fa-trash-alt'></i></BUTTON>"+
                                            "<INPUT TYPE ='TEXT' NAME = 'idAjax' ID = 'idAjax' VALUE = "+ datos[0] +" MAXLENGTH = '1' SIZE = '1' DISABLED HIDDEN/>" + "</td>" +
                                    "</tr>"
                                );

                            }
                        }

                    break;

                    case guardarArea = 1:
                        $("tr").remove(".cla"); 
                        $("#tablaDirectorio").append(resultado);
                    break;

                    case guardarArea = 4:
                        $("tr").remove(".cla"); 
                        $("#tablaDirectorio").append(resultado);
                    break;

                    case guardarArea = 7:
                        $("tr").remove(".cla"); 
                        $("#tablaDirectorio").append(resultado);
                    break;

                    case guardarArea = 9:
                        $("tr").remove(".cla"); 
                        $("#tablaDirectorio").append(resultado);
                    break;
                }
            }
            else{
                resultadoBusqueda("Sin resultado");
            }
        }

    }

}

function campoVacio(){
    if(document.getElementById('buscar').value != ""){
        if(guardarArea == 1)
            document.getElementById('buscar').placeholder = "Buscar Albergue";
        if(guardarArea == 3 || guardarArea == 4)
            document.getElementById('buscar').placeholder = "Buscar Integrante";
        if(guardarArea == 7)
            document.getElementById('buscar').placeholder = "Buscar Bodega";
        if(guardarArea == 9)
            document.getElementById('buscar').placeholder = "Buscar Zona de Riesgo";
        document.getElementById('buscar').style.color = "black";
        resultadoBusqueda("");
    }
    if(document.getElementById('buscar').value == ""){
        resultadoBusqueda("");
    }      
}
function resultadoBusqueda(texto){  
    if(document.getElementById('buscar').value == ""){
        mostrarResultadosdeTablas(calcularPaginaparaRegistros());
    }
    if(texto == "Sin resultado"){
        document.getElementById('buscar').value = "";
        document.getElementById('buscar').placeholder = "Sin resultados";
        document.getElementById('buscar').style.color = "red";
    }
}


function verificarPagina(paginaLimite,avanza){
    if(window.location.search == ""){
        document.getElementById('primeraPagina').style.pointerEvents = "none";
		history.pushState(null, "", "../CPresentacion/ventanaAdminPlanInfor.php?pagina= 1");
    }
    if(window.location.search.includes(paginaLimite)){
        document.getElementById('siguientePagina').style.pointerEvents = "none";
    }
    if(window.location.search.includes(1)){
        document.getElementById('primeraPagina').style.pointerEvents = "none";
    }
    if(avanza)
    	document.getElementById('primeraPagina').style.pointerEvents = "auto";
    if(!avanza)
        document.getElementById('siguientePagina').style.pointerEvents = "auto";
    if(avanza == null){
        if(window.location.search.includes(paginaLimite))
            document.getElementById('siguientePagina').style.pointerEvents = "none";    
        else
            document.getElementById('siguientePagina').style.pointerEvents = "auto";
        if(window.location.search.includes(1))
            document.getElementById('primeraPagina').style.pointerEvents = "none";    
        else
            document.getElementById('primeraPagina').style.pointerEvents = "auto";
    }
}   

function verificarLimitePaginacion(area,avanza){
    var xhr=new XMLHttpRequest();
    xhr.open("POST", area);
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("contarFilas=true");

    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4 && xhr.status == 200){
            resultado = xhr.responseText;
            resultado = resultado/4;
            resultado = Math.ceil(resultado)-1;
            verificarPagina(resultado,avanza);

        }
    }
}

function crearPaginacion(area){
    $("#pagination").empty();

    var resultado = 0;
    var xhr=new XMLHttpRequest();
    xhr.open("POST", area);
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("contarFilas=true");

    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4 && xhr.status == 200){
            resultado = xhr.responseText;
            resultado = resultado/4;
            resultado = Math.ceil(resultado)-1;
            for(var i = 0; i < resultado; i++){
                var listNode = document.getElementById('pagination'),
                    liNode = document.createElement("LI"),
                    txtNode = document.createTextNode(i);
                var href = document.createElement("a");
                href.textContent = i+1;
                href.setAttribute('onclick',"cargarTablaPaginacionNumerica('"+(parseInt(i)+1)+"')");
                liNode.appendChild(href);
                listNode.appendChild(liNode);

            }
            verificarPagina(resultado);
        }
    }

}

function cargarTablaPaginacionNumerica(valor){
    history.pushState(null, "", "../CPresentacion/ventanaAdminPlanInfor.php?pagina= " + valor);
        
    switch(guardarArea){
        case guardarArea = 1:
            verificarLimitePaginacion("../CNegocio/controladoraAlbergues.php",null);
            break;
        case guardarArea = 3:
            verificarLimitePaginacion("../CNegocio/controladoraDirectorio.php",null);
            break;
        case guardarArea = 4:
            verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",null);
            break;
        case guardarArea = 7:
            verificarLimitePaginacion("../CNegocio/controladoraBodega.php",null);
            break;
        case guardarArea = 9:
            verificarLimitePaginacion("../CNegocio/controladoraZonaRiesgo.php",null);
            break;            
    }


    mostrarResultadosdeTablas(valor);

}

function calcularPaginaparaRegistros(){
    var url = window.location.search;
    var puertaRegistro = false;
    var numUrl = 0;
    if(url.length == 12){
        for(var i = 0; i < url.length; i++){
            if(puertaRegistro){
                if(url[i] == "%"){

                }
                else if(i >= 11){
                    numUrl += url[i];           
                }
            }
            if(url[i] == "="){
                puertaRegistro = true;
            }
        }
    }
    else if(url.length == 9){
        for(var i = 0; i < url.length; i++){
            if(puertaRegistro){
                numUrl += url[i];           
            }
            if(url[i] == "="){
                puertaRegistro = true;
            }
        }
    }

    return  numUrl;

}

