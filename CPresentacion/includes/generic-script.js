/* Alternar entre colocar y quitar la clase "responsive" al navbar cuando el usuario hace click en ella */


function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";  /*  Dejar siempre ese espacio en blanco, así: " responsive"   */
  } else {
    x.className = "topnav";
  }
}


function myFunction2() {
  var x = document.getElementById("myTopnav2");
  if (x.className === "topnav2") {
    document.getElementById('icono').style.color = "black";
    document.getElementById('icono').style.backgroundColor = "#ddd";
    x.className += " responsive2";  /*  Dejar siempre ese espacio en blanco, así: " responsive"   */
  } else {
    document.getElementById('icono').style.color = "";
    document.getElementById('icono').style.backgroundColor = "";
    x.className = "topnav2";
  }
}



//Esta función repinta el menú elegido del topvar

$(function(){  
  var url = window.location.href;   // con esto agarramos la dirección url
   
  $("#myTopnav a").each(function() {  // se la aplicamos a cada etiqueta <a>          
      if(url.includes(this.href)) {   // revisa si la url contiene una parte de lo que se encuentra en el menu, tener cuidado con la paginación
          $(this).addClass("seleccionado");
      }
  });
});
