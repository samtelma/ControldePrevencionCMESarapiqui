<?php

	include 'conexion.php';

	class consultasZonasRiesgo{

		public function consultasZonasRiesgo() {}

		function contarFilas(){
			$conexion = new conexion();
			$respuesta = "";

			$contarFilas = "SELECT COUNT(idzonariesgo) FROM tbzonariesgo";
			$resultado = mysqli_query($conexion->abrirConexion(),$contarFilas);
			if(mysqli_num_rows($resultado) > 0)
				while($row = mysqli_fetch_row($resultado))
					$respuesta = $row[0];
			$conexion->cerrarConexion();

			return $respuesta;
		}

		function buscarPuntoRiesgoEspecifico($variable){

			$resultado = $this->Query("SELECT * FROM tbzonariesgo WHERE lugarzonariesgo = '".$variable."'
																		OR tipozonariesgo = '".$variable."'
																		OR alberguezonariesgo = '".$variable."'");

			if (mysqli_num_rows($resultado) > 0) {

				$i = 0;
				$array = array();

				while ($row = mysqli_fetch_row($resultado)) {

					$array[$i] = new zonaRiesgo($row[0], $row[1], $row[2], $row[3], $row[4], $row[4]);
					$i++;
				}

				//return $datos;
				return $array;
			}

			return 0;
		}
		function Query($query) {

			$conexion = new conexion(); // Instancia de la clase conexion, donde esta la estructura de conexion con la DB.
			$resultado = mysqli_query($conexion->abrirConexion(), $query); // Se abre la conexion y se envia la consulta.
			$conexion->cerrarConexion(); // Se cierra la conexion.
			
			return $resultado; // Se retorna la informacion pedida.
		}

		// Obtiene datos de los albergues.
		function Cargar() {
			
			// STRING de la consulta.
			$resultado = $this->Query("SELECT nombrealbergue FROM tbalbergues");

			if (mysqli_num_rows($resultado) > 0) { // Se verifica si retorna informacion.

				$datos = "";

				while ($row = mysqli_fetch_row($resultado)) {

					$datos.=$row[0]."+";
				}

				return $datos;
			}

			return 0;
		}

		// Registra una nueva Zona de Riesgo.
		function Registrar($zonaRiesgo) {

			$query="INSERT INTO tbzonariesgo VALUES ('".$zonaRiesgo->getId()."','".
														$zonaRiesgo->getLugar()."','".
														$zonaRiesgo->getTipo()."','".
														$zonaRiesgo->getAlbergue()."','".
														$zonaRiesgo->getLatitud()."','".
														$zonaRiesgo->getLongitud()."')";

			$resultado = $this->Query($query);

			return $resultado;
		}

		// Obtiene datos de la Zonas de Riesgo, para mostrar en la tabla de ZdR.
		function Consultar($pagina) {
			
			$resultado = $this->Query("SELECT * FROM tbzonariesgo ORDER BY idzonariesgo DESC LIMIT $pagina,6");

			if (mysqli_num_rows($resultado) > 0) {

				$i = 0;
				$array = array();

				while ($row = mysqli_fetch_row($resultado)) {

					$array[$i] = new zonaRiesgo($row[0], $row[1], $row[2], $row[3], $row[4], $row[4]);
					$i++;
				}

				//return $datos;
				return $array;
			}

			return 0;
		}

		// Elimina una Zona de Risgo de la base de datos.
		function Eliminar($id) {

			return $resultado = $this->Query("DELETE FROM tbzonariesgo WHERE idzonariesgo='".$id."'");
		}

		// Selecciona una Zona de Riesgo para su editacion.
		function Seleccionar($id) {

			$query = "SELECT * FROM tbzonariesgo WHERE idzonariesgo = '".$id."'";
			$resultado = $this->Query($query);

			if (mysqli_num_rows($resultado) > 0) {

				$row = mysqli_fetch_row($resultado);

				$zonaRiesgo = new zonaRiesgo($row[0], $row[1], $row[2], $row[3], $row[4], $row[5]);

				return $zonaRiesgo;
			}

			return 0;
		}

		// Acualiza la informacion de una Zonz de Riesgo ya en la BD.
		function Actualizar($zonaRiesgo) {

			// STRING de consulta a la base de datos.
			$query ="UPDATE tbzonariesgo SET lugarzonariesgo='".$zonaRiesgo->getLugar()."',tipozonariesgo='".
																$zonaRiesgo->getTipo()."',alberguezonariesgo='".
																$zonaRiesgo->getAlbergue()."'WHERE idzonariesgo='".
																$zonaRiesgo->getId()."'";

			$resultado = $this->Query($query);

			return $resultado;
		}
	}
?>