<?php


	class responsable {

		private $id;
		private $cedula;
		private $nombre;
		private $apellido1;
		private $apellido2;
		private $telefono;

		public function responsable($id, $cedula, $nombre, $apellido1, $apellido2, $telefono) {

			$this->id = $id;
			$this->cedula = $cedula;
			$this->nombre = $nombre;
			$this->apellido1 = $apellido1;
			$this->apellido2 = $apellido2;
			$this->telefono = $telefono;
		}

		// Sets...
		public function setId($id) {

			$this->id = $id;
		}
		public function setCedula($cedula) {

			$this->cedula = $cedula;
		}
		public function setNombre($nombre) {

			$this->nombre = $nombre;
		}
		public function setApellido1($apellido1) {

			$this->apellido1 = $apellido1;
		}
		public function setApellido2($apellido2) {

			$this->apellido2 = $apellido2;
		}
		public function setTelefono($telefono) {

			$this->telefono = $telefono;
		}

		// Gets...
		public function getId() {

			return $this->id;
		}
		public function getCedula() {

			return $this->cedula;
		}
		public function getNombre() {

			return $this->nombre;
		}
		public function getApellido1() {

			return $this->apellido1;
		}
		public function getApellido2() {

			return $this->apellido2;
		}
		public function getTelefono() {

			return $this->telefono;
		}
	}
?>