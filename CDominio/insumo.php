<?php


	class insumo {

		private $id;
		private $articulo;
		private $institucion;
		private $cantidad;
		private $observacion;
		private $bodega;

		public function insumo($id, $articulo, $institucion, $cantidad, $observacion, $bodega) {

			$this->id    = $id;
			$this->articulo    = $articulo;
			$this->institucion = $institucion;
			$this->cantidad    = $cantidad;
			$this->observacion = $observacion;
			$this->bodega      = $bodega;
		}

		// Sets...
		public function setId($id) {

			$this->id = $id;
		}

		public function setArticulo($articulo) {

			$this->articulo = $articulo;
		}

		public function setInstitucion($institucion) {

			$this->institucion = $institucion;
		}

		public function setCantidad($cantidad) {

			$this->cantidad = $cantidad;
		}

		public function setObservacion($observacion) {

			$this->observacion = $observacion;
		}

		public function setBodega($bodega) {

			$this->bodega = $bodega;
		}

		// Gets...
		public function getId() {

			return $this->id;
		}

		public function getArticulo() {

			return $this->articulo;
		}

		public function getInstitucion() {

			return $this->institucion;
		}

		public function getCantidad() {

			return $this->cantidad;
		}

		public function getObservacion() {

			return $this->observacion;
		}

		public function getBodega() {

			return $this->bodega;
		}

		// toString()...
		public function toString() {

			return $this->id."-".$this->articulo."-".$this->institucion."-".$this->cantidad."-".$this->observacion."-".$this->bodega;
		}
	}
?>