<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="../CPresentacion/imagenes/crc.png" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <title>Administrador(a) P.I</title>
    <LINK REL="stylesheet" type="text/css" href="../CPresentacion/css/fonts.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <link href="../CPresentacion/css/adminPlanInforCss.css" rel="stylesheet" type="text/css"/>    
    <script src="../CPresentacion/ajax/jquery.maskedinput-master/src/jquery.maskedinput.js" type="text/javascript"></script>
    <script src="../CPresentacion/ajax/ajaxAdminPlanInfor.js" type="text/javascript"></script>
</head>
<body>

    <?php include("includes/generic-header-login.html"); ?>
   
    <div id="page" class="container">

        <!-- Titulo de pagina -->
        <div id = "cabeceraPrincipal">
            <p id = "tituloCabecera">Secci&oacute;n de </p> 
            <p id = "encargado">Administrado(a) por: </p>
            <!-- Linea divisora -->
            <table id="opcionesMenu">
                <thead>
                    <tr>
                        <td>
                            <!-- Boton Registrar nuevo Valor -->
                            <INPUT TYPE = 'button' id="btn_registrar" ONCLICK = "verModalRegistrar()"></INPUT>
                        </td>
                    </tr>
                </thead>
            </table>
        </div>

        <INPUT TYPE = 'TEXT' ID = 'buscar' MAXLENGTH = '40' SIZE = '40' TITLE = "" ONKEYUP = "campoVacio()" ONKEYDOWN = "detectarTeclaEnter_enBusqueda(event)" PLACEHOLDER = "" style="display: none;"></INPUT>

        <div style = "position: relative;overflow: auto;">
            <table id="tabla">
                <tbody id = 'tablaDirectorio'></tbody>
            </table>
        </div>

        <NAV id = "main">
            <UL ID= "paginacion" CLASS= "paginacion" style='list-style-type: none;'>
                <LI CLASS = "page-item" ID = 'paginaAnterior'>
                    <A CLASS = "page-link" ID = 'primeraPagina' ONCLICK = "cargarPreviaInformacion()">
                        <i id = "iconos" class="fas fa-caret-square-left"></i>
                    </A>
                </LI>
                <LI ID = 'pagination' CLASS = "page-item">
                </LI>
                <LI CLASS = "page-item" ID = 'siguientePagina'>
                    <A CLASS = "page-link " ONCLICK = "cargarSiguienteInformacion()">
                        <i id = "iconos" class="fas fa-caret-square-right"></i>
                    </A>
                </LI>
            </UL>
        </NAV>

        <DIV class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <DIV class="modal-content">
                    <DIV class="modal-header text-center" style="background: #333333 url(images/overlay.png);">
                        <h4 ID="encabezadoModal" class="modal-title w-100 font-weight-bold" style="color: #FFF"></H4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </DIV>
                    <DIV class="modal-body mx-3">
                        <TABLE id = 'tablaxinformacionDetallada'>
                                   
                        </TABLE>
                    </DIV>
                    <DIV class="modal-footer d-flex justify-content-center">

                            <INPUT TYPE = 'BUTTON' id = 'aceptarDatos' class="btn btn-outline-info" VALUE = 'Actualizar' ONCLICK = "aceptarDatos()">
                            <INPUT TYPE = 'BUTTON' id = 'cancelarDatos' class="btn btn-outline-danger waves-effect ml-auto" data-dismiss="modal" VALUE = 'Cancelar' ONCLICK = 'cancelarDato()'>
                     
                    </DIV>
                </DIV>
            </div>
        </DIV>


    </div>

    <DIV class="modal fade" id="modalConfirmacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <DIV class="modal-content">
                <DIV class="modal-header text-center" style="background: #333333 url(images/overlay.png);">
                    <h4 ID="confirmacion" class="modal-title w-100 font-weight-bold" style="color: #FFF"></H4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </DIV>
 
                <DIV class="modal-footer d-flex justify-content-center">
                    <INPUT TYPE = 'BUTTON' id = 'botonConfirmacion' class="btn btn-outline-info" VALUE = 'Cerrar' ONCLICK = "botonConfirmacion()">      
                </DIV>
            </DIV>
        </div>
    </DIV>

    <?php include("includes/generic-footer.html"); ?>   

</body>
</html>